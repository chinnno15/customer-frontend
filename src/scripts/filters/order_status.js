export default (ORDER_STATUSES) => {
  const STATUS_TO_STRING = {
    [ORDER_STATUSES.PAID]: 'Placed & paid',
    [ORDER_STATUSES.ACKNOWLEDGED]: 'In Progress',
    [ORDER_STATUSES.READY]: 'Ready',
    [ORDER_STATUSES.SERVED]: 'Served',
    [ORDER_STATUSES.CANCELED]: 'Canceled',
  };

  return (input) => {
    const status = parseInt(input || 0, 10);
    return STATUS_TO_STRING[status];
  };
};
