import 'angular';

import orderStatus from './order_status.js';
import orderItemStatus from './order_item_status.js';
import htmlify from './htmlify.js';
import PriceFilter from './price.js';


/**
 * Module filters
 */
export default angular.module('customerApp.filters', ['customerApp.constants'])
  .filter('orderItemStatus', orderItemStatus)
  .filter('orderStatus', orderStatus)
  .filter('htmlify', htmlify)
  .filter('price', PriceFilter);
