/**
 * PriceFilter is responsible for price formatting and displaying currency symbol.
 * @example {{ '12' | price }}
 */
export default function PriceFilter($filter, Currency) {
  function filter(input) {
    const currency = Currency.getCurrency();
    const value = parseFloat(input);

    if (isNaN(value) || !currency) {
      return '';
    }

    const formatted = $filter('number')(value, currency.decimal_fields);

    return `${currency.sign}${formatted}`;
  }

  filter.$stateful = true;

  return filter;
};
