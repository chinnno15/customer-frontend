const STATUS_STRINGS = {
  IN_QUEUE: 'In queue',
  PREPARING: 'Preparing',
  READY: 'Ready',
  SERVED: 'Served',
  CANCELED: 'Canceled',
};

function toCamelCase(sentence) {
  const almostCamel = sentence.split(' ').reduce((camel, word) => {
    return camel + word[0].toUpperCase() + word.slice(1).toLowerCase();
  });
  return almostCamel[0].toLowerCase() + almostCamel.slice(1);
}

export default (ORDER_ITEM_STATUSES) => {
  const STATUS_TO_STRING = {
    [ORDER_ITEM_STATUSES.IN_CART]: STATUS_STRINGS.IN_QUEUE,
    [ORDER_ITEM_STATUSES.NEW]: STATUS_STRINGS.IN_QUEUE,
    [ORDER_ITEM_STATUSES.ORDERED]: STATUS_STRINGS.IN_QUEUE,
    [ORDER_ITEM_STATUSES.PREPARING]: STATUS_STRINGS.PREPARING,
    [ORDER_ITEM_STATUSES.READY]: STATUS_STRINGS.READY,
    [ORDER_ITEM_STATUSES.SERVED]: STATUS_STRINGS.SERVED,
    [ORDER_ITEM_STATUSES.CANCELED]: STATUS_STRINGS.CANCELED,
  };

  return (input, asCssClass=false) => {
    const status = parseInt(input || 0, 10);
    const statusString = STATUS_TO_STRING[status];
    return asCssClass ? toCamelCase(statusString) : statusString;
  };
};
