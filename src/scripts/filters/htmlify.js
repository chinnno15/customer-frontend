export default ($sce) => {
  return (html) => {
    // TODO decorate the html if needed (linkify phone number, url ..)

    return $sce.trustAsHtml(html);
  };
};
