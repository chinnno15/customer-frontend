import 'angular';
import '../styles/application.scss';
import './app';

/* Bootstrap AngularJS */
angular.element(document).ready(() => {
  angular.bootstrap(document, ['customerApp']);
});
