import 'angular';
import Raven from '../vendor/raven/raven';

export default function ravenConfig() {
  /* eslint-disable */

  // __process__ is injected by webpack, see `webpack/common.js`
  // @link https://webpack.github.io/docs/list-of-plugins.html#defineplugin
  //
  // Also, we should definitely skip Raven report in development,
  // mostly due to out-of-money issue with Sentry higher plan,
  // and kinda annoying, indeed ..
  if (__process__.DEVELOPMENT) {
    return null;
  }

  Raven
    .config('https://4e61bc750d9d4c8199886bc6fabc0d33@app.getsentry.com/63493')
    .setTagsContext({ environment: __process__.ENVIRONMENT })
    .install();

  /* eslint-enable */
};
