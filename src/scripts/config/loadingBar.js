export default function(cfpLoadingBarProvider) {
  // Disable spinner
  cfpLoadingBarProvider.includeSpinner = false;
};
