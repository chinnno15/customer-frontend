export default function($httpProvider) {
  // Make Angular to send cookies with each request
  $httpProvider.defaults.withCredentials = true;
  // Make sure Django knows we are sending XHR
  $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
  // Add SwishApiInterceptor
  $httpProvider.interceptors.push('SwishApiInterceptor');
};
