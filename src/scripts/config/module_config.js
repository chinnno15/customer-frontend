import 'angular';

import httpProvider from './httpProvider';
import lang from './lang';
import loadingBar from './loadingBar';
import main from './main';
import spinner from './spinner';
import states from './states';
import raven from './raven';

export default angular.module('customerApp.config', ['customerApp.constants'])
  .config(httpProvider)
  .config(lang)
  .config(loadingBar)
  .config(main)
  .config(spinner)
  .config(raven)
  .config(states);
