export default function($urlRouterProvider, $urlMatcherFactoryProvider) {
  // Make states URLs match trailing slashes
  $urlMatcherFactoryProvider.strictMode(false);

  // Default url
  $urlRouterProvider.otherwise('/no-order-point/');
};
