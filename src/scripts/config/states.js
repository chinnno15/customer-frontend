export default function($stateProvider, StripeCheckoutProvider, STATES) {
  // Customer app states
  $stateProvider

  /**
   * Backward compatibility for old url structure
   */
  .state(STATES.ORDER_POINT, {
    url: '/orderpoint/:orderPointId',
  })
  .state(STATES.NO_ORDERPOINT, {
    url: '/no-order-point',
    template: require('templates/landing_page.jade'),
  })
  .state(STATES.OPENBANK, {
    url: '/obso?orderId',
    controller: 'OpenBankController',
    controllerAs: 'OpenBankCtrl',
  })

  /**
   * rewrite url to http://swish.to/#/company_name/orderpoint_name
   * all child state such as: menu, orders will be:
   * http://swish.to/#/company_name/orderpoint_name/menu/...
   * http://swish.to/#/company_name/orderpoint_name/orders
   * ...
   */
  .state(STATES.MAIN, {
    url: '/:storeName/:orderPointName',
    controller: 'MainController',
    controllerAs: 'MainCtrl',
    template: require('templates/main.jade'),
  })

  .state(STATES.MENU, {
    url: '/menu/:categoryId',
    controller: 'MenuController',
    controllerAs: 'MenuCtrl',
    template: require('templates/menu/index.jade'),
  })

  .state(STATES.ORDERS, {
    url: '/orders',
    controller: 'OrdersController',
    controllerAs: 'OrdersCtrl',
    template: require('templates/orders.jade'),
  })

  .state(STATES.ORDER, {
    url: '/order/:orderId',
    controller: 'OrderController',
    controllerAs: 'OrderCtrl',
    template: require('templates/order.jade'),
  })

  .state(STATES.CART, {
    url: '/cart',
    controller: 'CartController',
    controllerAs: 'CartCtrl',
    template: require('templates/cart/index.jade'),
    resolve: {
      stripe: StripeCheckoutProvider.load,
    },
  })

  .state(STATES.TRANSLATE, {
    url: '/translate',
    controller: 'TranslateController',
    controllerAs: 'TranslateCtrl',
    template: require('templates/translate.jade'),
  })

  .state(STATES.RECEIPT, {
    url: '/receipt/:orderId',
    controller: 'ReceiptController',
    controllerAs: 'ReceiptCtrl',
    template: require('templates/receipt.jade'),
  })

  .state(STATES.PAYMENT_ERROR, {
    url: '/payment-error',
    template: require('templates/payment_error.jade'),
  })

  .state(STATES.ABOUT, {
    url: '/about',
    controller: 'CompanyController',
    controllerAs: 'CompanyCtrl',
    template: require('templates/about.jade'),
  })

  .state(STATES.TERMS_OF_USE, {
    url: '/terms-of-use',
    template: require('templates/terms_of_use.jade'),
  });
};
