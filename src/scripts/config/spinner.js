export default function(usSpinnerConfigProvider) {
  // Default spinner
  usSpinnerConfigProvider.setDefaults({ color: '#fff' });

  // Button spinner
  usSpinnerConfigProvider.setTheme('button', { radius: 7, width: 2, length: 6 });
};
