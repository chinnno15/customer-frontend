import en from '../../../lang/en.json';
import ru from '../../../lang/ru.json';
import de from '../../../lang/de.json';
import es from '../../../lang/es.json';
import fr from '../../../lang/fr.json';

// Locale for messageFormat
// TODO refactor this
// import MessageFormat from 'messageformat';
// MessageFormat.locale.en = n => {
//  // The default messageFormat locale doesn't support zero
//  //   so this is an extended version of it
//  if (0 === n) {
//    return 'zero';
//  } else if (1 === n) {
//    return 'one';
//  } else {
//    return 'other';
//  }
// };

export default function($translateProvider) {
  $translateProvider
    .translations('en', en)
    .translations('ru', ru)
    .translations('de', de)
    .translations('es', es)
    .translations('fr', fr)
    .addInterpolation('$translateMessageFormatInterpolation');
};
