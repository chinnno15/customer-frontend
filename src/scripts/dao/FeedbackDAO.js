export default class FeedbackDAO {
  constructor($http) {
    this.http = $http;
    this.rootUrl = 'feedback';
  }

  createFeedback(orderId, feedback) {
    return this.http
      .post(`${this.rootUrl}`, {
        order_id: orderId,
        feedback,
      })
      .then((res) => res.data);
  }
}
