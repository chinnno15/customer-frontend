/**
 * CategoryDAO is responsible for querying category API endpoint.
 */
export default function CategoryDAO($http, Translator, DAOS) {
  const rootUrl = 'category';

  function listCategories(parentCategoryId) {
    const parentCategoryIdParam = parentCategoryId || DAOS.EMPTY_FILTER;

    return $http
      .get(
        rootUrl,
        { params: {
          parent: parentCategoryIdParam,
          page_size: DAOS.NO_PAGINATION,
        }}
      )
      .then(res => {
        return Translator.translate(res.data.results, ['name', 'description']);
      });
  }

  function getCategory(categoryId) {
    return $http
      .get(`${rootUrl}/${categoryId}`)
      .then(res => res.data);
  }

  // public API
  return {
    listCategories,
    getCategory,
  };
}
