/**
 * CompanyDAO is responsible for querying company API endpoint.
 */
export default function CompanyDAO($http) {
  function getCompany() {
    return $http.get('company')
      .then(res => res.data);
  }

  return {
    getCompany
  };
}
