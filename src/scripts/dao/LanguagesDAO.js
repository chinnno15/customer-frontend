/**
 * LanguagesDAO is responsible for querying languages API endpoint.
 */
export default function LanguagesDAO($http) {
  function listLanguages() {
    return $http
      .get('languages', { cache: true })
      .then(res => res.data.sort((a, b) => {
        return a.localized_title.toLowerCase().localeCompare(
          b.localized_title.toLowerCase()
        );
      }));
  }

  function listGoogleTranslateLanguages() {
    return $http
      .get('location/google-translate', { cache: true })
      .then(res => res.data);
  }

  return {
    listLanguages,
    listGoogleTranslateLanguages,
  };
}
