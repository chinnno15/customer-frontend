/**
 * OrderDAO is responsible for querying order API endpoints.
 */
export default function OrderDAO($http, ORDER_STATUSES) {
  const PENDING_STATUSES = [
    ORDER_STATUSES.ACKNOWLEDGED,
    ORDER_STATUSES.PAID,
    ORDER_STATUSES.READY,
  ].join(',');

  const VISIBLE_STATUSES = [
    ORDER_STATUSES.ACKNOWLEDGED,
    ORDER_STATUSES.PAID,
    ORDER_STATUSES.READY,
    ORDER_STATUSES.SERVED,
    ORDER_STATUSES.CANCELED,
  ].join(',');

  const rootUrl = 'order';

  function listOrders() {
    // TODO: implement pagination support and sort orders by status
    return $http
      .get(rootUrl, { params: { state__in: VISIBLE_STATUSES }})
      .then(res => res.data.results);
  }

  function getPendingOrdersCount() {
    return $http
      .get(rootUrl, { params: { state__in: PENDING_STATUSES }})
      .then(res => res.data.count);
  }

  function getOrder(orderId) {
    return $http
      .get(`${rootUrl}/${orderId}`)
      .then(res => res.data);
  }

  function getReceiptUrl(orderId) {
    return $http
      .post(`${rootUrl}/${orderId}/receipt`)
      .then(res => res.data.url);
  }

  function sendReceiptByEmail(orderId, email) {
    return $http
      .post(`${rootUrl}/${orderId}/receipt-email`, { email });
  }

  // public API
  return {
    listOrders,
    getOrder,
    getPendingOrdersCount,
    getReceiptUrl,
    sendReceiptByEmail,
  };
}
