/**
 * OrderItemDAO is responsible for querying orderitem API endpoints.
 */
export default class OrderItemDAO {
  constructor($q, $http, Translator, DAOS, ORDER_ITEM_STATUSES) {
    this.$q = $q;
    this.http = $http;
    this.ORDER_ITEM_STATUSES = ORDER_ITEM_STATUSES;
    this.DAOS = DAOS;
    this.Translator = Translator;

    this.rootUrl = 'orderitem';
  }

  createOrderItem(productId, quantity) {
    return this.http
      .post(this.rootUrl, {
        product: productId,
        status: this.ORDER_ITEM_STATUSES.IN_CART,
        quantity,
      }, { ignoreLoadingBar: true })
      .then((res) => res.data);
  }

  createOrderItemWithOptions(productId, quantity, options) {
    return this.http
      .post(this.rootUrl, {
        product: productId,
        status: this.ORDER_ITEM_STATUSES.IN_CART,
        quantity,
        options: options
      }, { ignoreLoadingBar: true })
      .then((res) => res.data);
  }

  listOrderItems() {
    return this.http
      .get(
        this.rootUrl,
        { params: {
          status: this.ORDER_ITEM_STATUSES.IN_CART,
          page_size: this.DAOS.NO_PAGINATION,
        }}
      )
      .then((res) => {
        return this.Translator.translate(res.data.results, ['product_name']);
      });
  }

  updateOrderItem(orderItem) {
    const { id } = orderItem;

    delete orderItem.id;

    return this.http
      .put(`${this.rootUrl}/${id}`, {
        ...orderItem
      }, { ignoreLoadingBar: true })
      .then((res) => res.data);
  }

  prioritizeOrderItem(orderItemId, productId, priority) {
    return this.http
      .put(`${this.rootUrl}/${orderItemId}`, {
        product: productId,
        priority,
      }, { ignoreLoadingBar: true })
      .then((res) => res.data);
  }

  deleteOrderItem(orderItemId) {
    return this.http
      .delete(`${this.rootUrl}/${orderItemId}`, { ignoreLoadingBar: true })
      .then((res) => res.data);
  }
}
