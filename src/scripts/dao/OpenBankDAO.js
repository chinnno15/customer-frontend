/**
 * OpenBankDAO is responsible for querying company API endpoint.
 */
export default function OpenBankDAO($http) {
  function getOrder(orderid) {
    return $http.get('openbankorder', {params: {orderId: orderid}})
      .then(res => res.data);
  }

  return {
    getOrder
  };
}
