/**
 * OrderPointDAO is responsible for querying orderpoint API endpoint.
 */
export default class OrderPointDAO {
  constructor($http) {
    this.http = $http;
  }

  register(storeName, orderPointName) {
    return this.http
      .post('orderpoint/register', {
        store_name: storeName,
        order_point_name: orderPointName
      }, {
        withCredentials: false
      })
      .then((res) => res.data);
  }

  /**
   * get order point by UUID
   * @param {String} uuid - uuid of orderpoint
   */
  getOrderPoint(uuid) {
    return this.http
      .get(`orderpoint/uuid`, {
        params: {
          uuid: uuid
        }
      })
      .then(res => res.data);
  }
}
