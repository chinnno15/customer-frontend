/**
 * CurrencyDAO is responsible for querying currency API endpoints.
 */
export default function CurrencyDAO($http) {
  function getCurrency(currencyId) {
    return $http.get(`currency/${currencyId}`)
      .then(res => res.data);
  }

  return {
    getCurrency
  };
}
