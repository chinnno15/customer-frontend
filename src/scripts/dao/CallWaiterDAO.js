/**
 * CallWaiterDAO is responsible for querying callwaiter API endpoint.
 */
export default function CallWaiterDAO($http) {
  const rootUrl = 'callwaiter';

  function formatCallIdList(ids) {
    return ids.reduce((str, id) => {
      if (str.length === 0) {
        str += `${id}`; // eslint-disable-line no-param-reassign
      } else {
        str += `,${id}`; // eslint-disable-line no-param-reassign
      }
      return str;
    }, '');
  }

  function getCalls(callIds) {
    return $http
      .get(rootUrl, { ignoreLoadingBar: true, params: {id: formatCallIdList(callIds)}})
      .then(res => res.data.results);
  }

  /**
   * create a call for waiter regarding respective orderId
   * @param  {Integer} orderId
   * @param  {String} message
   * @return {Promise} Resolves if server is able to create call to waiter.
   */
  function createCall(orderId, message) {
    let data = {};

    if (message) {
      Object.assign(data, {message});
    }

    if (orderId) {
      Object.assign(data, {order_id: orderId});
    }

    return $http
      .post(rootUrl, data)
      .then(res => res.data);
  }

  // public API
  return {
    getCalls,
    createCall,
  };
}
