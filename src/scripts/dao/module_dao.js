import 'angular';

import CompanyDAO from './CompanyDAO';
import CategoryDAO from './CategoryDAO';
import LanguagesDAO from './LanguagesDAO';
import OrderPointDAO from './OrderPointDAO';
import ProductDAO from './ProductDAO';
import PaymentDAO from './PaymentDAO';
import OrderDAO from './OrderDAO';
import OrderItemDAO from './OrderItemDAO';
import FeedbackDAO from './FeedbackDAO';
import CallWaiterDAO from './CallWaiterDAO';
import CurrencyDAO from './CurrencyDAO';
import OpenBankDAO from './OpenBankDAO';

export default angular.module('customerApp.dao', ['customerApp.constants'])
  .factory('CallWaiterDAO', CallWaiterDAO)
  .factory('LanguagesDAO', LanguagesDAO)
  .factory('CurrencyDAO', CurrencyDAO)
  .service('CompanyDAO', CompanyDAO)
  .factory('CategoryDAO', CategoryDAO)
  .service('OrderPointDAO', OrderPointDAO)
  .factory('ProductDAO', ProductDAO)
  .service('PaymentDAO', PaymentDAO)
  .service('FeedbackDAO', FeedbackDAO)
  .factory('OrderDAO', OrderDAO)
  .service('OrderItemDAO', OrderItemDAO)
  .service('OpenBankDAO', OpenBankDAO);
