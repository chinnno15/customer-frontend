/**
 * PaymentDAO is responsible for querying payment API endpoints.
 */
export default class PaymentDAO {
  constructor($http) {
    this.http = $http;
    this.rootUrl = 'payment';
  }

  getStripeKey() {
    return this.http
      .get(`${this.rootUrl}/stripe`)
      .then((res) => res.data.public_key);
  }

  checkoutWithGear(comment) {
    return this.http
      .post(`${this.rootUrl}/gear`, { comment })
      .then((res) => res.data);
  }

  checkoutWithOpenBank(comment) {
    return this.http
      .post(`${this.rootUrl}/openbank`, { comment })
      .then((res) => res.data);
  }

  checkoutWithCash(comment) {
    return this.http
      .post(`${this.rootUrl}/cash`, { comment })
      .then((res) => res.data);
  }

  checkoutWithStripe(token, comment) {
    return this.http
      .post(`${this.rootUrl}/stripe`, { token, comment })
      .then((res) => res.data);
  }

  checkoutWithBilderlingspay(comment) {
    return this.http
      .post(`${this.rootUrl}/bilderlingspay`, { comment })
      .then((res) => res.data);
  }
}
