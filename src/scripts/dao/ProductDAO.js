/**
 * ProductDAO is responsible for querying product API endpoint.
 */
export default function ProductDAO($http, DAOS, Translator) {
  const rootUrl = 'product';

  function listProducts(categoryId) {
    let categoryIdParam = categoryId;

    if (!categoryId) {
      categoryIdParam = DAOS.EMPTY_FILTER;
    }

    return $http
      .get(
        rootUrl,
        { params: {
          category: categoryIdParam,
          page_size: DAOS.NO_PAGINATION
        }})
      .then(res => {
        return Translator.translate(res.data.results, ['name', 'description']);
      });
  }

  // public API
  return {
    listProducts
  };
}
