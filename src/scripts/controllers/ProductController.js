/**
 * ProductController is responsible for managing product's quantity.
 */
export default function ProductController($log, $scope, $rootScope, Cart, Alert, EVENTS, $filter) {
  const labels = [null, 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'];
  function initialize() {
    this.product = $scope.item;
    this.quantity = Cart.getQuantity(this.product.id);
    this.quantityCounter = this.quantity > 0;

    // Mapping preferences and extras from Cart
    if (this.product.is_option_enabled) {
      const option = Cart.getOption(this.product.id) || {};
      this.option = {
        preferences: [],
        extras: []
      };

      // Filter in active item first
      this.product.preferences = this.product.preferences.map(preferenceItem => {
        return preferenceItem.filter(ele => ele.is_active);
      });
      this.product.extras = this.product.extras.filter(ele => ele.is_active);

      this.product.preferences.forEach((preference, index) => {
        let defaultIndex = -1;
        let defaultPreference = preference[0];

        preference.forEach((preferenceItem, idx) => {
          const optionPreference = option.preferences
            ? option.preferences.find(p => p.id === preferenceItem.id)
            : null;

          // Select from Cart
          if (optionPreference) {
            defaultIndex = idx;
          }

          // Get the default selected
          if (defaultIndex === -1 && preferenceItem.is_selected) {
            defaultIndex = idx;
          }
        });

        if (preference[defaultIndex]) {
          defaultPreference = preference[defaultIndex];
        }

        this.option.preferences[index] = defaultPreference;
      });

      this.product.extras.forEach((extra, index) => {
        const optionExtra = option.extras
          ? option.extras.find(ex => ex.id === extra.id)
          : null;
        const quantity = optionExtra ? optionExtra.quantity : 0;

        this.option.extras[index] = {
          id: extra.id,
          name: extra.name,
          quantity: quantity,
          price: extra.price || 0,
          show_counter: quantity > 0
        };
      });
    }
  }

  function updateQuantity(quantity) {
    const oldQuantityValue = this.quantity;

    this.quantity = quantity;

    let promise;
    if (this.product.is_option_enabled) {
      promise = Cart.setQuantity(this.product, this.quantity, this.option);
    } else {
      promise = Cart.setQuantity(this.product, this.quantity);
    }

    promise.catch((err) => {
      this.quantity = oldQuantityValue;
      $log.error('Failed to modify cart', err);
      Alert.error('CART.UPDATE_QUANTITY_FAILED');
    });
  }

  // bind private functions to controller
  const initializeBound = initialize.bind(this);
  const updateQuantityBound = updateQuantity.bind(this);

  // initialize controller and refresh on first cart update (happens when cart gets initialized)
  initializeBound();
  const deregister = $rootScope.$on(EVENTS.CART_UPDATED, () => {
    initializeBound();
    deregister();
  });

  // expose controller API
  this.incQuantity = () => {
    updateQuantityBound(this.quantity + 1);
  };

  this.decQuantity = () => {
    if (this.quantity > 0) {
      updateQuantityBound(this.quantity - 1);
    }
  };

  this.setQuantity = n => {
    updateQuantityBound(n);
  };

  this.showQuantityCounter = (item) => {
    if (!item.is_sellable) {
      // append labels along with weekday
      // to easily translate, and human readable
      item.schedule.forEach(day => {
        day.label = labels[day.weekday];
      });

      $scope.MenuCtrl.showModal = true;
      $scope.MenuCtrl.unavailableItem = item;
    } else {
      this.quantityCounter = true;
    }
  };

  this.priceCounter = item => {
    if (!item.is_option_enabled) {
      return item.price;
    } else {
      let price = 0;

      this.option.preferences.forEach(preference => {
        if (preference) {
          price+= parseFloat(preference.price);
        }
      });

      this.option.extras.forEach(extra => {
        price+= extra.quantity * parseFloat(extra.price);
      });

      return price;
    }
  };

  this.getDefaultPreferenceItem = preference => {
    for (let i in preference) {
      if (preference[i].is_selected) {
        return preference[i];
      }
    }
  };

  this.getPriceFormat = (price, isOption) => {
    const priceFormat = $filter('price')(price);

    if (price !== 0) {
      if (isOption) {
        return ': ' + priceFormat;
      } else {
        return ': <strong>' + priceFormat + '</strong>';
      }
    }

    return '';
  };

  this.incExtra = priceExtra => {
    priceExtra.quantity++;
    this.updateCartOptions();
  };

  this.decExtra = priceExtra => {
    if (priceExtra.quantity > 0) {
      priceExtra.quantity--;
      this.updateCartOptions();
    }
  };

  this.updateCartOptions = () => {
    if (this.product.is_option_enabled && this.quantity > 0) {
      Cart.setOption(this.product, this.option);
    }
  };
}
