const SOURCE_LANGAUGE = 'en';
const TEXT_TO_TRANSLATE = 'translate';

/**
 * TranslateLinkController exposes translatedLink text to the UI.
 */
export default function TranslateLinkController($log, Lang, GoogleTranslator, Alert) {
  const targetLanguage = Lang.getLangShortCode(Lang.getBrowserLanguage());

  if (SOURCE_LANGAUGE !== targetLanguage) {
    GoogleTranslator.translate(TEXT_TO_TRANSLATE, SOURCE_LANGAUGE, targetLanguage)
      .then(translations => {
        this.translatedLink = translations[0].translatedText;
      })
      .catch(err => {
        $log.error(
          'Error translating translate link text ' +
          `from ${SOURCE_LANGAUGE} to ${targetLanguage}`, err
        );
        Alert.error('TRANSLATE.TRANSLATE_LINK_FAILED');
      });
  }
};
