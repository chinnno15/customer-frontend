/**
 * OpenBankController is responsible for handling replies from Open Bank
 */
export default class OpenBankController {
  constructor($log, Alert, STATES, $state, OpenBankDAO, $stateParams, OrdersStore, Cart, SwishAppInit, localStorageService, STORAGE) {
    const orderid = $stateParams.orderId;
    const storedOrderPointName = localStorageService.get(STORAGE.ORDER_POINT_NAME);
    const storedStoreName = localStorageService.get(STORAGE.STORE_NAME);

    OpenBankDAO.getOrder(orderid)
      .then((res) => {
        Cart.clean(true);
        $state.go(STATES.RECEIPT, {orderId: res.id.toString(), storeName: storedStoreName, orderPointName: storedOrderPointName});
      })
      .catch((err) => {
        $log.error('Failed to fetch openbank order', err);
        Alert.error('ORDER.DATA_FAILED');
        $state.go(STATES.MENU);
      });
  }
}
