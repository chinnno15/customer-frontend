/**
 * ToggleController can be used to show and hide a UI component.
 */
export default class ToggleController {
  constructor($scope) {
    this.isOpen = false;

    // makes sure a UI component is hidden after location change
    $scope.$on('$stateChangeStart', () => {
      this.isOpen = false;
    });
  }

  /**
   * Toggles a UI component visibility on and off.
   */
  toggle() {
    this.isOpen = !this.isOpen;
  }

  setOpen(isOpen) {
    this.isOpen = isOpen;
  }
}
