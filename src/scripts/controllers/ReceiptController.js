/**
 * ReceiptController is responsible for exposing receipt order to the UI.
 */
export default class ReceiptController {
  constructor($q, $log, $state, $stateParams, OrdersStore, OrderPointStore, OrderDAO, FeedbackDAO, CompanyDAO, Alert, STATES, I18n) {
    Object.assign(this, { $log, Alert, OrderDAO, FeedbackDAO, I18n });

    const orderId = $stateParams.orderId;
    const getOrderPromise = orderId ? OrderDAO.getOrder(orderId) : OrdersStore.getLatestOrder();

    OrderPointStore.getOrderPoint().then((orderPoint) => {
      this.orderPoint = orderPoint;
    });

    CompanyDAO.getCompany().then(company => {
      this.company = company;
    });

    getOrderPromise
      .then((order) => {
        if (order) {
          this._setOrder(order);

          if (!order.paid && order.payment_method === null) {
            Alert.error('RECEIPT.UNPAID_MESSAGE');
          }

          return OrderDAO.getReceiptUrl(order.id);
        }
        // go to the menu state if no orderId and no latest order
        $state.go(STATES.MENU);
        return $q.resolve();
      })
      .then((url) => {
        if (url) {
          this.email = '';
          this.downloadUrl = url;
        }
      })
      .catch((err) => {
        if (err.status === 402) {
          // some notifications should be here..
        } else {
          $log.error('Failed to fetch order', err);
          Alert.error('RECEIPT.DATA_FAILED');
        }
      });
  }

  _setOrder(order) {
    this.order = order;
    this.itemsTotal = order.items.reduce((total, item) => total + item.quantity, 0);
  }

  sendFeedback() {
    const { $log, Alert, FeedbackDAO, order } = this;
    this.feedbackProcessing = true;

    FeedbackDAO.createFeedback(order.id, this.feedback)
      .then((res) => {
        order.feedback = res.feedback;
      })
      .catch((err) => {
        $log.error('Failed to create feedback', err);
        Alert.error((err.data && err.data.detail) || 'RECEIPT.FEEDBACK.CREATE_FAILED');
      })
      .finally(() => {
        this.feedbackProcessing = false;
      });
  }

  sendByEmail() {
    const { $log, Alert, OrderDAO, order, email } = this;

    if (email && email.length) {
      OrderDAO.sendReceiptByEmail(order.id, email)
        .then(() => {
          this.email = '';

          this.I18n.translate('RECEIPT.SAVE.EMAIL_SUCCESS', { email })
            .then(str => Alert.info(str));
        })
        .catch((err) => {
          $log.error('Failed sending receipt by e-mail', err);
          Alert.error('RECEIPT.SAVE.EMAIL_FAILED');
        });
    }
  }
}
