/**
 * ImageController is responsible for preparing product and category image for display.
 */
export default function ImageController($scope, $attrs) {
  // image sizes
  this.IMAGE_SIZES = {
    SMALL: '67x67',
    BIG: '540x220',
  };

  const size = $attrs.imageSize || this.IMAGE_SIZES.BIG;
  const item = $scope.item;
  const thumbs = item.product_thumbs || item.thumbs;
  const image = thumbs ? thumbs[size] : null;
  const color = item.product_default_color || item.default_color;

  this.noImage = !image;
  this.defaultImage = !!color || this.noImage;

  this.backgroundStyles = {};

  if (image) {
    this.backgroundStyles['background-image'] = `url(${image})`;
  }

  if (color) {
    this.backgroundStyles['background-color'] = color;
  }
}
