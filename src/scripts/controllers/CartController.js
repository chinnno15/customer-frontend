/**
 * CartController is responsible for displaying products in the cart and checkout.
 */
export default function CartController($scope, $rootScope, $log, Cart, CartItemsStore, Company, Alert, Checkout, PAYMENT_PROVIDERS, EVENTS) {
  const PAYMENT_METHODS = {
    BITCOIN: 'BITCOIN',
    CREDIT_CART: 'CREDIT_CART',
    CASH: 'CASH',
  };

  const listeners = [];


  const isProviderAvailable = (providerName) => {
    const company = Company.getCompany();

    if (!company) {
      return false;
    }

    const provider = company.payment_providers[providerName];

    return provider.available && provider.active;
  };


  const areWaitersAvailable = () => {
    const company = Company.getCompany() || { waiters_working: true };

    return company.waiters_working;
  };


  // TODO: refactor PAYMENT_PROVIDERS constant to be string keys
  const getProvider = (paymentMethod) => {
    if (paymentMethod === PAYMENT_METHODS.BITCOIN && isProviderAvailable('gear')) {
      return PAYMENT_PROVIDERS.GEAR;
    }

    if (paymentMethod === PAYMENT_METHODS.CREDIT_CART && isProviderAvailable('stripe')) {
      return PAYMENT_PROVIDERS.STRIPE;
    }

    if (paymentMethod === PAYMENT_METHODS.CREDIT_CART && isProviderAvailable('bilderlingspay')) {
      return PAYMENT_PROVIDERS.BILDERLINGSPAY;
    }

    if (paymentMethod === PAYMENT_METHODS.CREDIT_CART && this.isOpenBankAvailable()) {
      return PAYMENT_PROVIDERS.OPENBANK;
    }

    if (paymentMethod === PAYMENT_METHODS.CASH && this.isCashAvailable()) {
      return PAYMENT_PROVIDERS.CASH;
    }

    return null;
  };

  // IMPORTANT! Due to the browser popup blocking feature and Stripe using `window.open()`,
  // `Checkout.checkout()` must be called directly in this function, not in the callback.
  // Mobile browsers will generally block the popup if it's shown as a result
  // of a callback (e.g. ajax request) and not a direct user action (e.g. click event).

  this.isOpenBankAvailable = () => {
    const company = Company.getCompany();
    if (!company) {
      return false;
    }
    return company.openbank_enabled;
  };

  const checkout = (providerId) => {
    // checkout only if cart is not empty,
    // another payment is not currently being processed
    // and order point is available (active and open)
    if (!this.isCartEmpty() && !this.paymentLoading && this.isOrderPointAvailable) {
      this.paymentLoading = true;

      const loadedCallback = () => {
        this.paymentLoading = false;
      };

      Checkout.checkout(providerId, this.comment, loadedCallback)
        .catch(err => {
          this.paymentLoading = false;

          // rejected promise is not always an error
          // for example Stripe API rejects returned promise when user
          // cancells the payment
          if (err) {
            const data = err.data || {};
            const alertArgs = [ 'CART.CHECKOUT_FAILED' ];

            $log.error('Checkout failed', err);

            if (data.detail === 'StripeInvalidRequest') {
              alertArgs.push(data.stripe);
            }

            if (data.detail === 'OrderPointInactive') {
              alertArgs.push('ORDER_POINT.UNAVAILABLE.INACTIVE');
            }

            if (data.detail === 'OrderPointCurrentlyClosed') {
              alertArgs.push('ORDER_POINT.UNAVAILABLE.CLOSED');
            }

            if (data.detail === 'WaiterNotAvailable') {
              alertArgs.push('ORDER_POINT.UNAVAILABLE.NO_WAITERS');
            }

            Alert.error(...alertArgs);
          }
        });
    }
  };

  const refreshProducts = () => {
    this.products = Cart.getProducts();

    // As said, the list should be in sorting mode only if
    //   there's at least one item with truthy `priority`
    this.productSortable.sorting = this.products.some(product => product.priority);
  };

  this.isOrderPointAvailable = true;

  this.METHODS = PAYMENT_METHODS;

  // Should the products sortable or not?
  // If there's at least one prioritized item, then
  //   it means user wanna sort the list by himself
  this.productSortable = {
    delay: 150, // prevent drag when we wanna scroll on touch devices
    disabled: false,
    onUpdate: () => {
      this.updateProductSorting();
    },
    // Please note we don't wanna disable the sorting
    //   as it's the only way to activate the sort mode
    //   so let's use another attr to update the ui
    sorting: false,
  };

  this.isCashAvailable = () => {
    const company = Company.getCompany();

    if (!company) {
      return false;
    }

    return company.cash_enabled === true;
  };

  this.isCartEmpty = () => {
    return Cart.getTotals().quantity === 0;
  };

  this.getPrice = () => {
    return Cart.getTotals().price;
  };

  this.clearCart = () => {
    Cart.deleteAllItems();
    this.showClearCartModal = false;
  };

  /**
   * Checkout the cart.
   *
   * IMPORTANT! Due to the browser popup blocking feature and Stripe using `window.open()`,
   * `Checkout.checkout()` must be called directly in this function, not in the callback.
   * Mobile browsers will generally block the popup if it's shown as a result
   * of a callback (e.g. ajax request) and not a direct user action (e.g. click event).
   *
   * @param  {String} paymentMethod - 'BITCOIN' or 'CREDIT_CART'
   */
  this.checkout = (paymentMethod) => {
    const providerId = getProvider(paymentMethod);

    if (!providerId) {
      $log.info('Payment method not supported');
      this.showPaymentUnavailableModal = true;

      return;
    }

    if (!areWaitersAvailable()) {
      $log.info('Waiters not available');
      this.showWaitersUnavailableModal = true;

      return;
    }

    checkout(providerId);
  };

  this.updateProductSorting = () => {
    const totalItems = this.products.length;

    this.products.forEach((product, idx) => {
      // higher priority bubble up
      const priority = totalItems - idx;

      product.priority = priority;
      CartItemsStore.prioritizeItem(product.id, priority);
    });

    this.productSortable.sorting = true;

    return this;
  };

  this.cancelProductSorting = () => {
    this.products.forEach((product) => {
      product.priority = 0;
      CartItemsStore.prioritizeItem(product.id, 0);
    });

    this.productSortable.sorting = false;

    return this;
  };

  // listen for order point availability changes
  listeners.push($rootScope.$on(EVENTS.ORDERPOINT_AVAILABILITY_CHANGED, (event, isAvailable) => {
    this.isOrderPointAvailable = isAvailable;
  }));

  // wait for cart to be initialized to grab products
  Cart.waitForInitializedCart().then(refreshProducts);
  listeners.push($rootScope.$on(EVENTS.CART_UPDATED, refreshProducts));

  // clear global listeners on destroy
  $scope.$on('$destroy', () => {
    listeners.forEach(deregister => deregister());
  });

  // display modal if waiters not available
  if (!areWaitersAvailable()) {
    $log.info('Waiters not available');
    this.showWaitersUnavailableModal = true;
  }
}
