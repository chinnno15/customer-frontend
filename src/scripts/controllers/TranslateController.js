import { keys } from 'lodash';

/* Translate page controller */
export default class TranslateController {
  constructor($q, $log, $state, Lang, LanguagesDAO, Alert, STATES) {
    Object.assign(this, { $log, $state, Lang, Alert, STATES });
    const ctx = this;

    $q.all([
      LanguagesDAO.listGoogleTranslateLanguages(),
      Lang.getSelectedLanguage(),
    ])
      .then(results => {
        let selectedLanguage;
        [ ctx.languages, selectedLanguage ] = results;
        ctx.setSelectedLanguage(selectedLanguage);
      })
      .catch((err) => {
        $log.error('Failed to load languages list or selected language', err);
        Alert.error('TRANSLATE.DATA_FAILED');
      });
  }

  setSelectedLanguage(language) {
    if (this.languages[language]) {
      this.selectedLanguage = language;
    } else {
      const languageKeys = keys(this.languages);
      const firstMatch = languageKeys.find((langKey) => langKey.indexOf(language) > -1);
      if (firstMatch) {
        this.selectedLanguage = firstMatch;
      }
    }
  }
  changeLanguage(language=this.selectedLanguage) {
    const { $state, Lang } = this;

    // save selected language
    Lang.setSelectedLanguage(language);

    // go to menu
    $state.go(this.STATES.MENU);
  }

  dontTranslate() {
    const { $log, Alert, Lang } = this;

    // get default language
    Lang.getDefaultLanguage()
      .then(defaultLanguage => {
        // set selected language back to default language
        this.changeLanguage(defaultLanguage);
      })
      .catch(err => {
        $log.error('Failed to load default language', err);
        Alert.error('TRANSLATE.DEFAULT_LANGUAGE_FAILED');
      });
  }
}
