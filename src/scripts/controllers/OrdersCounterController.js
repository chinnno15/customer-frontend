/**
 * OrdersCountersController is responsible for exposing the number of orders to the UI.
 */
export default function OrdersCountersController($scope, $rootScope, OrdersStore, EVENTS) {
  const deregister = $rootScope.$on(EVENTS.NUMBER_OF_ORDERS_UPDATED, () => {
    this.numberOfOrders = OrdersStore.getNumberOfPendingOrders();
  });

  // cleanup global listener on scope destruction
  $scope.$on('$destroy', deregister);
}
