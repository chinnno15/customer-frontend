/**
 * MenuController is responsible for exposing categories and products to the UI.
 */
export default function MenuController($q, $log, $stateParams, CategoryDAO, ProductDAO, Alert, $state, STATES) {
  const categoryId = $stateParams.categoryId;
  const labels = [null, 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'];

  this.showModal = false;

  this.isItemACategory = (item) => {
    return !!item.products;
  };

  this.go = (item) => {
    if (!item.is_sellable && item.schedule) {
      // append labels along with weekday
      // to easily translate, and human readable
      item.schedule.forEach(day => {
        day.label = labels[day.weekday];
      });

      this.showModal = true;
      this.unavailableItem = item;
    } else {
      $state.go(STATES.MENU, { categoryId: item.id });
    }
  };

  $q.all([
    // get parent category (if nested menu level)
    categoryId && CategoryDAO.getCategory(categoryId),

    // get categories and products
    CategoryDAO.listCategories(categoryId),
    ProductDAO.listProducts(categoryId),
  ])
    .then(results => {
      const [ parent, categories, products ] = results;

      // expose number of products and categories
      this.categoriesCount = categories.length;
      this.productsCount = products.length;

      // set parent category as current category
      this.category = parent;

      // merge products and categories into single array
      const items = categories.concat(products);

      // sort products and categories array according to sorting param
      this.items = items.sort((a, b) => a.sorting - b.sorting);
    })
    .catch((err) => {
      $log.error('Failed to fetch categories or products', err);
      Alert.error('MENU.DATA_FAILED');
    });
}
