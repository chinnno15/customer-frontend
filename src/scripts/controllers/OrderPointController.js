/**
 * OrderPointController is responsible for periodically checking orderpoint availability and displaying modal.
 */
export default function OrderPointController($scope, $rootScope, $interval, OrderPointStore, INTERVALS, EVENTS) {
  const updateAvailability = (availabilityValue) => {
    if (availabilityValue !== this.isAvailable) {
      this.isAvailable = availabilityValue;

      // notify about availability change
      $rootScope.$emit(EVENTS.ORDERPOINT_AVAILABILITY_CHANGED, this.isAvailable);
    }
  };

  const checkAvailability = () => {
    OrderPointStore.isAvailable()
      .then(() => {
        updateAvailability(true);

        // hide modal
        this.showModal = false;
      })
      .catch(err => {
        // show modal only when availability state or details change
        if (this.isAvailable || !angular.equals(err, this.details)) {
          updateAvailability(false);

          // show modal and update details
          this.showModal = true;
          this.details = err;
        }
      });
  };

  this.isAvailable = true;

  // initial check
  checkAvailability();

  // init the interval
  const checkAvailabilityInterval = $interval(checkAvailability, INTERVALS.CHECK_ORDERPOINT_AVAILABILITY);

  // clear the interval on scope destruction
  $scope.$on('$destroy', () => {
    $interval.cancel(checkAvailabilityInterval);
  });
};
