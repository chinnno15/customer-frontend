/**
 * OrderController is responsible for exposing order items to the UI.
 */
export default class OrderController {
  constructor($log, $stateParams, OrderDAO, OrderItemDAO, Alert, $interval, $scope, INTERVALS) {
    this.OrderItemDAO = OrderItemDAO;

    const orderId = $stateParams.orderId;

    const fetchOrder = () => {
      // fetch order
      OrderDAO.getOrder(orderId)
        .then((order) => {
          this.items = order.items;
          this.orderId = order.company_order_id;
        })
        .catch((err) => {
          $log.error(`Failed to fetch order ${orderId}`, err);
          Alert.error('ORDER.DATA_FAILED');
        });
    };

    // init the interval
    const fetchOrderInterval = $interval(fetchOrder, INTERVALS.FETCH_ORDER_ITEMS);

    // clear the interval when leave
    const deregister = $scope.$on('$destroy', () => {
      $interval.cancel(fetchOrderInterval);

      // cleanup the listener
      deregister();
    });

    // fetch order
    fetchOrder();
  }
  /**
   * set rating for order item.
   * @param {Integer} rating 1 - 5
   * @param {Object} item
   * - {Integer} order  order number
   * - {Integer} product  product number
   */
  setRating(rating, item) {
    const {id, product} = item;
    const orderItemWRating = {id, product, rating};
    const prevRating = item.rating;

    // set rating on current item
    item.rating = rating;

    return this.OrderItemDAO.updateOrderItem(orderItemWRating)
      .catch(() => {
        // restore rating on failure
        item.rating = prevRating;
      });
  }
}
