/**
 * CallWaiterController is responsible for call waiter functionality
 */
export default function CallWaiterController($log, Alert, CallWaiter) {
  // assign showCallWaiterMessageSheet to controller
  this.showCallWaiterMessageSheet = CallWaiter.showCallWaiterMessageSheet;

  this.callWaiter = (orderId, message) => {
    Alert.success('CALL_WAITER.FINDING');
    CallWaiter.callWaiter(orderId, message);
  };

  // initialize CallWaiter
  CallWaiter.init();
}
