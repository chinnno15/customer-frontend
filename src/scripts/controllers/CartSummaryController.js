/**
 * CartSummaryController is responsible for displaying
 * total number of items in the cart and total price.
 */
export default function CartSummaryController($scope, $rootScope, $state, $stateParams, $timeout, Cart, EVENTS, INTERVALS, STATES) {
  Cart.waitForInitializedCart().then(() => {
    this.totals = Cart.getTotals();
  });

  // listen for cart updates
  const deregister = $rootScope.$on(EVENTS.CART_UPDATED, () => {
    this.totals = Cart.getTotals();
  });


  // cleanup global listener on scope destruction
  $scope.$on('$destroy', deregister);

  this.shouldCartHide = false;
  if ($state.current.name === STATES.MENU && $stateParams.categoryId) {
    this.shouldCartHide = true;
    $timeout(() => {
      this.shouldCartHide = false;
    }, INTERVALS.TMP_HIDE_CART);
  }
}
