import 'angular';

import ToggleController from './ToggleController';
import CompanyController from './CompanyController';
import MenuController from './MenuController';
import ImageController from './ImageController';
import OrderController from './OrderController';
import OrdersController from './OrdersController';
import OrdersCounterController from './OrdersCounterController';
import ProductController from './ProductController';
import CartSummaryController from './CartSummaryController';
import CartController from './CartController';
import ReceiptController from './ReceiptController';
import TranslateController from './TranslateController';
import TranslateLinkController from './TranslateLinkController';
import CallWaiterController from './CallWaiterController';
import OrderPointController from './OrderPointController';
import CartProductController from './CartProductController';
import MainController from './MainController.js';
import OpenBankController from './OpenBankController.js';

/**
 * Module controllers
 */
export default angular.module('customerApp.controllers', ['customerApp.constants'])
  .controller('ToggleController', ToggleController)
  .controller('OrderController', OrderController)
  .controller('OrdersController', OrdersController)
  .controller('OrdersCounterController', OrdersCounterController)
  .controller('ProductController', ProductController)
  .controller('CartSummaryController', CartSummaryController)
  .controller('CartController', CartController)
  .controller('ReceiptController', ReceiptController)
  .controller('CompanyController', CompanyController)
  .controller('MenuController', MenuController)
  .controller('ImageController', ImageController)
  .controller('TranslateLinkController', TranslateLinkController)
  .controller('TranslateController', TranslateController)
  .controller('CallWaiterController', CallWaiterController)
  .controller('OrderPointController', OrderPointController)
  .controller('MainController', MainController)
  .controller('CartProductController', CartProductController)
  .controller('OpenBankController', OpenBankController);
