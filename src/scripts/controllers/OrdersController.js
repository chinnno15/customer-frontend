/**
 * OrdersController is responsible for exposing orders to the UI.
 */
export default function OrdersController($log, $state, $scope, $interval, Alert, OrderDAO, STATES, ORDER_STATUSES, INTERVALS) {
  const fetchOrders = () => {
    OrderDAO.listOrders()
      .then((orders) => {
        this.orders = orders || [];
        this.ordersInPreparation = orders.reduce((sum, order) => {
          return sum + (this.isOrderReady(order) ? 0 : 1);
        }, 0);
      })
      .catch((err) => {
        $log.error('Failed to fetch orders', err);
        Alert.error('ORDERS.DATA_FAILED');
      });
  };

  // init the interval
  const fetchOrdersInterval = $interval(fetchOrders, INTERVALS.FETCH_ORDERS);

  // clear the interval on location change
  const deregister = $scope.$on('$destroy', () => {
    $interval.cancel(fetchOrdersInterval);

    // cleanup the listener
    deregister();
  });

  // fetch orders
  fetchOrders();

  /**
   * Checks if order is ready.
   * @param  {Object} order - order object
   * @return {Boolean}        true if order's status in SERVED, false otherwise
   */
  this.isOrderReady = (order) => {
    return order.state === ORDER_STATUSES.SERVED;
  };

  /**
   * Go to order details URL.
   * @param  {Object} order - order object
   */
  this.goToOrderDetails = (order) => {
    return $state.go(STATES.ORDER, { orderId: order.id });
  };
}
