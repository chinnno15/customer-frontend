/**
 * CompanyController is responsible for exposing company to the UI.
 */
export default class CompanyController {
  constructor($rootScope, $log, Alert, CompanyDAO, EVENTS, OrderPointStore) {
    this.Alert = Alert;
    this.$log = $log;

    // fetch company
    const getCompany = () => {
      CompanyDAO.getCompany()
        .then((company) => {
          this.company = company;
        })
        .catch((err) => {
          $log.error('Failed to fetch company', err);
          Alert.error('COMPANY.DATA_FAILED');
        });
    };

    // XXX fix get company when order point isn't registered
    // Should be revised
    if (OrderPointStore.isOrderPointRegistered()) {
      getCompany();
    } else {
      $rootScope.$on(EVENTS.ORDERPOINT_REGISTERED, () => {
        getCompany();
      });
    }
  }
}
