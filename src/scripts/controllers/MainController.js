/**
 * MenuController can be used to show and hide a UI component.
 *
 * register orderpoint when app init
 *
 */
export default class MenuController {
  constructor($scope, $stateParams, SwishAppInit) {
    SwishAppInit.registerOrderPoint($stateParams.storeName, $stateParams.orderPointName);
  }
}
