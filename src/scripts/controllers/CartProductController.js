/**
 * CartProductController is responsible for managing product in cart
 */
export default function CartProductController($log, $scope, $rootScope, Cart, Alert, EVENTS) {
  function initialize() {
    this.product = $scope.item.product;
    this.quantity = Cart.getQuantity(this.product.id);
    this.quantityCounter = this.quantity > 0;
  }

  function updateQuantity(quantity) {
    const oldQuantityValue = this.quantity;

    this.quantity = quantity;

    Cart.setQuantity(this.product, this.quantity)
      .catch((err) => {
        this.quantity = oldQuantityValue;
        $log.error('Failed to modify cart', err);
        Alert.error('CART.UPDATE_QUANTITY_FAILED');
      });
  }

  // bind private functions to controller
  const initializeBound = initialize.bind(this);
  const updateQuantityBound = updateQuantity.bind(this);

  // initialize controller and refresh on first cart update (happens when cart gets initialized)
  initializeBound();
  const deregister = $rootScope.$on(EVENTS.CART_UPDATED, () => {
    initializeBound();
    deregister();
  });

  // expose controller API
  this.incQuantity = () => {
    updateQuantityBound(this.quantity + 1);
  };

  this.decQuantity = () => {
    if (this.quantity > 0) {
      updateQuantityBound(this.quantity - 1);
    }
  };

  this.getPrice = (item) => {
    return Cart.getItemPrice(item);
  };
}
