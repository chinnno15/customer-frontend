import 'babel-polyfill';
/* import 3rd party modules */
import 'angular';
import 'angular-flash-alert'; // XXX does its job, although not a perfect lib, lack of customization, and value added features ..
import 'angular-loading-bar';
import 'angular-local-storage';
import 'angular-sanitize';
import 'angular-spinner';
import 'angular-stripe-checkout';
import 'angular-touch'; // XXX ideally it should be angular-hammer, but how to embed it into this webpack?
import 'angular-animate';
import 'angular-translate';
import 'angular-translate-interpolation-messageformat';
import 'angular-ui-router';
import 'angulartics';
import 'angulartics-google-analytics';
import 'messageformat';
import 'angular-qrcode';
import 'ng-dialog';
import 'Sortable';
import 'spin.js';
import './vendor/raven/raven-module';

/*
import angular-ui-bootstrap components
for full list of components see: https://github.com/angular-ui/bootstrap/tree/master/src
*/
// import buttons from 'angular-ui-bootstrap/src/buttons';

/* import own modules */
import appConfig from './config/module_config';
import appConstants from './constants/module_constants';
import appControllers from './controllers/module_controllers';
import appDao from './dao/module_dao';
import appDirectives from './directives/module_directives';
import appFilters from './filters/module_filters';
import appServices from './services/module_services';
import '../../public/ngConstants';


/* application definition */
angular.module('customerApp', [
  // third party dependencies
  'angular-loading-bar',
  'angularSpinner',
  'ngDialog',
  'flash',
  'LocalStorageModule',
  'ng-sortable',
  'ngSanitize',
  'ngTouch',
  'pascalprecht.translate',
  'stripe.checkout',
  'ui.router',
  'ngRaven',
  'monospaced.qrcode',
  'ngAnimate',
  'angulartics',
  'angulartics.google.analytics',

  // angular-bootstrap components
  // buttons,

  // application local dependencies
  appConfig.name,
  appConstants.name,
  appControllers.name,
  appDao.name,
  appDirectives.name,
  appFilters.name,
  appServices.name,

  'customerApp.env', // dynamic env, @see config/environments.json
])

// run blocks
.run((SwishAppInit, AppReloader) => {
  SwishAppInit.initialize();
  // initialize with app name
  AppReloader.initialize('customer');
})

// add 'inside-frame' class to body element if page is displayed inside the frame
// used to apply frame specific styling
.run(($window) => {
  if ($window.self !== $window.top) {
    angular.element($window.document.body).addClass('inside-frame');
  }
});

// TODO move this to separated device/Android fix files
// @link http://stackoverflow.com/questions/23757345/x
if (/Android/i.test(navigator.appVersion)) {
  window.addEventListener('resize', () => {
    if (
      document.activeElement
      && /^(input|textarea)$/i.test(document.activeElement.tagName)
    ) {
      let el = document.activeElement;

      // Scroll form instead, if specified
      if (el.form.classList.contains('scrollIntoView')) {
        el = el.form;
      }

      if (el.scrollIntoView) {
        setTimeout(() => {
          el.scrollIntoView(false);
        }, 0);
      }
    }
  });
}
