import 'angular';

export default angular.module('customerApp.constants', [])
  .constant('STATES', {
    NO_ORDERPOINT: 'no_orderpoint',
    OPENBANK: 'openbank',
    ORDER_POINT: 'orderpoint',
    MAIN: 'main',
    MENU: 'main.menu',
    DISHES: 'main.dishes',
    ORDERS: 'main.orders',
    ORDER: 'main.order',
    CART: 'main.cart',
    TRANSLATE: 'main.translate',
    RECEIPT: 'main.receipt',
    ABOUT: 'main.about',
    TERMS_OF_USE: 'main.terms_of_use',
    PAYMENT_ERROR: 'main.payment_error',
  })

  .constant('EVENTS', {
    ORDERPOINT_REGISTERED: 'swish:order-point-registered',
    CART_UPDATED: 'swish:cart-updated',
    COMPANY_UPDATED: 'swish:company-updated',
    NUMBER_OF_ORDERS_UPDATED: 'swish:number-of-orders-updated',
    MENU_LANGUAGE_CHANGED: 'swish:menu-language-changed',
    STRIPE_KEY_CHANGED: 'swish:stripe-key-changed',
    ORDERPOINT_AVAILABILITY_CHANGED: 'swish:order-point-availability-changed',
  })

  // TODO: short polling is probably fine for now, but this needs to be
  // properly implemented as instant notifications using websockets or long-polling
  .constant('INTERVALS', {
    FETCH_WAITER_CALL: 5 * 1000,
    FETCH_STRIPE_KEY: 30 * 1000,
    FETCH_COMPANY: 30 * 1000,
    FETCH_ORDER_ITEMS: 10 * 1000,
    FETCH_ORDERS: 10 * 1000,
    FETCH_ORDERS_COUNT: 10 * 1000,
    CHECK_ORDERPOINT_AVAILABILITY: 10 * 1000,
    TMP_HIDE_CART: 2 * 1000,
    CHECK_APPLICATION_UPDATE: 60 * 1000,
    ALERT_DURATION: 60 * 1000,
  })

  .constant('ORDER_ITEM_STATUSES', {
    IN_CART: 0,
    NEW: 1,
    ORDERED: 2,
    PREPARING: 3,
    READY: 4,
    SERVED: 5,
    CANCELED: 100,
  })

  .constant('ORDER_STATUSES', {
    CREATED: 1,
    PAID: 2,
    ACKNOWLEDGED: 3,
    READY: 4,
    SERVED: 5,
    CANCELED: 100,
  })

  .constant('WAITER_CALL_STATUSES', {
    PENDING: 1,
    TAKEN: 2,
    COMPLETED: 3,
    EXPIRED: 4,
    CANCELLED: 100,
  })

  .constant('PAYMENT_PROVIDERS', {
    STRIPE: 1,
    BILDERLINGSPAY: 2,
    GEAR: 3,
    OPENBANK: 8,
    CASH: 9,
  })

  .constant('DAOS', {
    EMPTY_FILTER: 'null',
    NO_PAGINATION: 1000000000,
  })

  .constant('GOOGLE_API_KEY', 'AIzaSyD5IT8Wt0t-RGEPna0vE3ujaSx0EFyjnig')

  .constant('STORAGE', {
    STORE_NAME: 'storeName',
    ORDER_POINT_NAME: 'orderPointName',
  });
