export default function($timeout) {
  return {
    restrict: 'A',
    link(scope, el) { // eslint-disable-line
      $timeout(() => {
        el[0].focus();
      });
    },
  };
};
