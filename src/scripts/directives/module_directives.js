import 'angular';

import autofocus from './autofocus';
import lengthLimit from './lengthLimit';

export default angular.module('customerApp.directives', [])
  .filter('autofocus', autofocus)
  .directive('lengthLimit', lengthLimit);
