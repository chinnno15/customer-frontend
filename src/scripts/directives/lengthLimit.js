/**
 * A directive for limiting textarea length for instance:
 * <textarea ng-model="text" length-limit="100">
 * @param {String} ng-model required.
 */
export default function() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, elem, attrs, ctrl) {
      attrs.$set('ngTrim', 'false');
      const maxlength = parseInt(attrs.lengthLimit, 10);
      ctrl.$parsers.push(function(value) {
        if (value.length > maxlength) {
          const newValue = value.substr(0, maxlength);
          ctrl.$setViewValue(newValue);
          ctrl.$render();
        }
        return value;
      });
    }
  };
}
