/**
 * Angular.js plugin
 *
 * Provides an $exceptionHandler for Angular.js
 */
import Raven from './raven';

// See https://github.com/angular/angular.js/blob/v1.4.7/src/minErr.js
const angularPattern = /^\[((?:[$a-zA-Z0-9]+:)?(?:[$a-zA-Z0-9]+))\] (.+?)\n(\S+)$/;

function RavenProvider() {
  this.$get = function() {
    return Raven;
  };
}

function exceptionHandler(R, $delegate) {
  return function(ex, cause) {
    R.captureException(ex, {
      extra: { cause: cause }
    });
    $delegate(ex, cause);
  };
}

function ExceptionHandlerProvider($provide) {
  $provide.decorator('$exceptionHandler',
      ['Raven', '$delegate', exceptionHandler]);
}

export default angular.module('ngRaven', [])
  .provider('Raven',  RavenProvider)
  .config(['$provide', ExceptionHandlerProvider]);

Raven.setDataCallback((data) => {
  // We only care about mutating an exception
  let exception = data.exception;
  if (exception) {
    exception = exception.values[0];
    const matches = angularPattern.exec(exception.value);

    if (matches) {
      // This type now becomes something like: $rootScope:inprog
      exception.type = matches[1];
      exception.value = matches[2];
      data.message = exception.type + ': ' + exception.value;
      // auto set a new tag specifically for the angular error url
      data.extra.angularDocs = matches[3].substr(0, 250);
    }
  }
});
