/**
 * Translator is responsible for translating selected properties of multiple
 * objects from App's default language to user's selected language.
 * @see MultiTranslator
 */
export default function Translator($q, $log, Lang, MultiTranslator, GoogleTranslator, Alert) {
  function handleError(err) {
    $log.warn('Translation service is currently unavailable', err);
    Alert.error('TRANSLATE.SERVICE_UNAVAILABLE');
  }

  /**
   * Translates selected properties of multiple objects.
   * @param  {Array} objects             - objects to translate
   * @param  {Array} properties          - properties that will be translated
   * @return {Promise}                     promise with translated objects
   */
  function translate(objects, properties) {
    const deferred = $q.defer();

    $q.all([
      Lang.getDefaultLanguage(),
      Lang.getSelectedLanguage(),
    ])
      .then(results => {
        const [defaultLanguage, selectedLanguage] = results;

        // translate
        return MultiTranslator.translate(objects, properties, defaultLanguage, selectedLanguage);
      })
      .then(translatedObjects => {
        deferred.resolve(translatedObjects);
      })
      .catch(err => {
        // resolve with initial objects when translation fails
        deferred.resolve(objects);
        handleError(err);
      });

    return deferred.promise;
  }

  /**
   * Just calls GoogleTranslator.initialize(), but adds error handling.
   * @see GoogleTranslator
   */
  function initialize() {
    return GoogleTranslator.initialize().catch(err => handleError(err));
  }

  return {
    translate,
    initialize,
  };
}
