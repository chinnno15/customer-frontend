const LATEST_ORDER_ID_KEY = 'latestOrderId';

/**
 * OrdersStore is simple service that keeps track of orders related states.
 */
export default class OrdersStore {
  constructor($q, $log, $rootScope, $interval, localStorageService, OrderDAO, Alert, EVENTS, INTERVALS, OrderPointStore) {
    Object.assign(this, { $q, $rootScope, localStorageService, OrderDAO, EVENTS });

    this.numberOfPendingOrders = 0;

    const getPendingOrdersCount = () => {
      OrderDAO.getPendingOrdersCount()
        .then((count) => {
          this.numberOfPendingOrders = count;
          this._notify();
        })
        .catch((err) => {
          $log.error('Failed to fetch pending orders count', err);
          Alert.error('ORDERS.PENDING_COUNT_FAILED');
        });
    };

    // XXX get pending too soon when order point isn't registered
    // Should be revised
    // refresh number of pending orders periodicaly
    if (OrderPointStore.isOrderPointRegistered()) {
      getPendingOrdersCount();
      $interval(getPendingOrdersCount, INTERVALS.FETCH_ORDERS_COUNT);
    } else {
      $rootScope.$on(EVENTS.ORDERPOINT_REGISTERED, () => {
        getPendingOrdersCount();
        $interval(getPendingOrdersCount, INTERVALS.FETCH_ORDERS_COUNT);
      });
    }
  }

  _notify() {
    const { $rootScope, EVENTS } = this;
    $rootScope.$emit(EVENTS.NUMBER_OF_ORDERS_UPDATED);
  }

  getNumberOfPendingOrders() {
    return this.numberOfPendingOrders;
  }

  setLatestOrder(order) {
    // increase number of pending orders and notify
    this.numberOfPendingOrders++;
    this._notify();

    // set latest order and store it's ID in local storage
    this.latestOrder = order;
    this.localStorageService.set(LATEST_ORDER_ID_KEY, order.id);
  }

  getLatestOrder() {
    const { $q, localStorageService, OrderDAO } = this;

    if (this.latestOrder) {
      return $q.resolve(this.latestOrder);
    }

    const latestOrderId = localStorageService.get(LATEST_ORDER_ID_KEY);
    return latestOrderId ? OrderDAO.getOrder(latestOrderId) : $q.resolve(null);
  }
}
