export default function GoogleApiLoader($document, $q, $window, GOOGLE_API_KEY) {
  const URL = 'https://apis.google.com/js/client.js?onload=googleApiResolve';

  let isApiLoaded = false;

  function loadGoogleApiScript() {
    const deferred = $q.defer();
    const scriptElement = $document[0].createElement('script');

    $window.googleApiResolve = () => {
      deferred.resolve($window.gapi);
      delete $window.googleApiResolve;
    };

    scriptElement.onerror = error => {
      deferred.reject(error);
    };

    scriptElement.src = URL;
    $document[0].body.appendChild(scriptElement);

    return deferred.promise;
  }

  function loadTranslateApi() {
    const deferred = $q.defer();

    $window.gapi.client.setApiKey(GOOGLE_API_KEY);
    $window.gapi.client.load('translate', 'v2', () => {
      isApiLoaded = true;
      deferred.resolve($window.gapi);
    });

    return deferred.promise;
  }

  function load() {
    return loadGoogleApiScript()
      .then(() => {
        return loadTranslateApi();
      });
  }

  function isLoaded() {
    return isApiLoaded;
  }

  return {
    load,
    isLoaded
  };
}
