import appReloaderMessageTmpl from '../../templates/alerts/appReloader.jade';
const serverInfoUrl = 'serverinfo';
/**
 * When initialized, AppReloader checks last commit hash against the last commit hash stored on server.
 */
export default function Update($interval, INTERVALS, $http, TopSheet) {
  function promptUserToReload() {
    if (!TopSheet.isOpen()) {
      TopSheet.close()
      .then(() => {
        TopSheet.show({
          template: appReloaderMessageTmpl(),
          canClose: false
        });
      });
    }
  }

  function checkAppVersion() {
    const ctx = this;
    return $http
    .get(serverInfoUrl)
    .then(res => {
      const version = res.data.versions[ctx._appName];
      if (version !== __VERSION__ && version) { // eslint-disable-line no-undef
        // prompt user to reload
        promptUserToReload();
      }
    });
  }

  function initialize(appName) {
    // disable AppReloader when NODE_ENV is development
    if (NODE_ENV !== 'development') { // eslint-disable-line no-undef
      this._appName = appName;
      this.checkAppVersion();
      this._checkAppVersion = $interval(checkAppVersion.bind(this), INTERVALS.CHECK_APPLICATION_UPDATE);
    }
  }
  return {
    initialize,
    checkAppVersion,
  };
}
