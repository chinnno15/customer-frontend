/**
 * StripePayment service is responsible for Stripe integration.
 */
export default class StripePayment {
  constructor($q, $log, $interval, $rootScope, StripeCheckout, PaymentDAO, EVENTS, INTERVALS) {
    Object.assign(this, { $q, $log, $interval, $rootScope, StripeCheckout, PaymentDAO, EVENTS, INTERVALS });

    this.ERRORS = {
      NO_KEY: 'MISSING_STRIPE_KEY',
      FAIL: 'STRIPE_KEY_FETCH_FAIL'
    };
  }

  _getStripeKey() {
    const { $q, $log, PaymentDAO } = this;
    const deffered = $q.defer();

    PaymentDAO.getStripeKey()
      .then((key) => {
        deffered.resolve(key);
      })
      .catch((err) => {
        // TODO: temporary fix, this need more serious rework
        if (err.data.detail === 'StripeDisabled') {
          $log.info('Stripe disabled', err);
          deffered.resolve(null);
        } else {
          $log.error('Failed to fetch Stripe key.', err);
          deffered.reject(this.ERRORS.FAIL);
        }
      });

    return deffered.promise;
  }

  _configureStripe() {
    if (!this.key) {
      return false;
    }

    if (this.stripe) {
      return true;
    }

    this.stripe = this.StripeCheckout.configure({
      name: 'Checkout',
      key: this.key,
    });

    return !!this.stripe;
  }

  _setStripeKey() {
    const { $rootScope, EVENTS } = this;
    return this._getStripeKey()
      .then((key) => {
        if (this.key && this.key !== key) {
          $rootScope.$emit(EVENTS.STRIPE_KEY_CHANGED);
        }

        this.key = key;
      });
  }

  _checkForKeyChange() {
    if (!this.fetchKeyInterval) {
      const { $interval, INTERVALS } = this;
      this.fetchKeyInterval = $interval(() => this._setStripeKey(), INTERVALS.FETCH_STRIPE_KEY);
    }
  }

  initializeStripe() {
    this._checkForKeyChange();
    return this._setStripeKey();
  }

  /**
   * Checkout with Stripe.
   *
   * IMPORTANT! Due to the browser popup blocking feature and Stripe using `window.open()`,
   * `stripe.open()` must be called directly in this function, not in the callback.
   * Mobile browsers will generally block the popup if it's shown as a result
   * of a callback (e.g. ajax request) and not a direct user action (e.g. click event).
   *
   * @param  {Integer} price     - price as a number of cents
   * @param  {String} currency   - e.g. 'USD', 'CAD', 'EUR'
   * @param  {Object} callbacks  - `opened` and `closed` Stripe callback options
   * @return {Promise}             resolves when payment was successful,
   *                               rejects on error or user cancellation
   */
  checkout(price, currency, callbacks = {}) {
    if (this._configureStripe()) {
      const options = {
        amount: price,
        currency: currency,
      };

      if (callbacks.opened) {
        options.opened = () => this.$rootScope.$apply(callbacks.opened);
      }

      if (callbacks.closed) {
        options.closed = () => this.$rootScope.$apply(callbacks.closed);
      }

      return this.stripe.open(options)
        .then((result) => {
          return result[0].id;
        });
    }
    return this.$q.reject(this.ERRORS.NO_KEY);
  }
};
