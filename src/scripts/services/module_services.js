import 'angular';

import Alert from './Alert';
import Cart from './Cart';
import CartItemsStore from './CartItemsStore';
import Checkout from './Checkout';
import Company from './Company';
import Currency from './Currency';
import GoogleApiLoader from './GoogleApiLoader';
import GoogleTranslator from './GoogleTranslator';
import I18n from './I18n';
import Lang from './Lang';
import MultiTranslator from './MultiTranslator';
import OrderPointStore from './OrderPointStore';
import OrdersStore from './OrdersStore';
import CallWaiter from './CallWaiter';
import StripePayment from './StripePayment';
import SwishApiInterceptor from './SwishApiInterceptor';
import SwishAppInit from './SwishAppInit';
import Translator from './Translator';
import UrlRequestSender from './UrlRequestSender';
import TopSheet from './TopSheet';
import Modal from './Modal';
import AppReloader from './AppReloader';

export default angular.module('customerApp.services', ['customerApp.constants'])
  .factory('GoogleApiLoader', GoogleApiLoader)
  .factory('GoogleTranslator', GoogleTranslator)
  .factory('I18n', I18n)
  .factory('Lang', Lang)
  .factory('MultiTranslator', MultiTranslator)
  .factory('Translator', Translator)
  .factory('Alert', Alert)
  .factory('OrderPointStore', OrderPointStore)
  .factory('CallWaiter', CallWaiter)
  .service('Cart', Cart)
  .service('CartItemsStore', CartItemsStore)
  .service('Checkout', Checkout)
  .factory('Company', Company)
  .factory('Currency', Currency)
  .service('OrdersStore', OrdersStore)
  .service('StripePayment', StripePayment)
  .service('SwishApiInterceptor', SwishApiInterceptor)
  .service('SwishAppInit', SwishAppInit)
  .service('UrlRequestSender', UrlRequestSender)
  .service('TopSheet', TopSheet)
  .service('Modal', Modal)
  .service('AppReloader', AppReloader);
