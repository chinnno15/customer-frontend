/**
 * CartItemsStore is responsible for storing cart items
 * and updating order items in the backend.
 * @see Cart
 */
export default class CartItemsStore {
  constructor($q, OrderItemDAO) {
    this.$q = $q;
    this.OrderItemDAO = OrderItemDAO;
    this._cartItems = {};
    this._orderItemsIds = {};

    this.ERRORS = {
      NOT_FOUND: 'ITEM_NOT_FOUND',
    };
  }

  _queue(actionFn) {
    this._lastPromise = this._lastPromise ? this._lastPromise.then(actionFn) : actionFn();
    return this._lastPromise;
  }

  _modifyItem(productId, modifyFn, syncFn) {
    const cartItem = this.getItem(productId);

    if (cartItem) {
      modifyFn(cartItem);
      return this._queue(() => {
        return syncFn(this._orderItemsIds[productId]);
      });
    }

    return this._queue(() => this.$q.reject(this.ERRORS.NOT_FOUND));
  }

  initialize() {
    // fetch existing order items from the server
    return this.OrderItemDAO.listOrderItems().then((orderItems) => {
      orderItems.forEach((orderItem) => this.setItem(orderItem));
    });
  }

  deleteItem(productId) {
    return this._modifyItem(
      productId,
      () => delete this._cartItems[productId],
      (orderItemId) => this.OrderItemDAO.deleteOrderItem(orderItemId),
    );
  }

  deleteAllItems() {
    const promises = [];

    this.getItems().forEach(item =>
      promises.push(this.deleteItem(item.product.id))
    );

    return this.$q.all(promises);
  }

  prioritizeItem(productId, priority) {
    return this._modifyItem(
      productId,
      (cartItem) => cartItem.priority = priority,
      (orderItemId) => this.OrderItemDAO.prioritizeOrderItem(orderItemId, productId, priority),
    );
  }

  updateQuantity(productId, quantity) {
    const cartItem = this.getItem(productId);

    return this._modifyItem(
      productId,
      (item) => item.quantity = quantity,
      (orderItemId) => {
        let orderItem = {
          id: orderItemId,
          product: productId,
          quantity: quantity
        };

        if (cartItem.product.is_option_enabled) {
          const options = this.convertOptionToArray(cartItem.optionInObject);
          orderItem.options = options;
        }

        return this.OrderItemDAO.updateOrderItem(orderItem);
      }
    );
  }

  createItem(product, quantity, optionInObject) {
    this._cartItems[product.id] = { product, quantity };

    const callback = orderItem => {
      this._orderItemsIds[product.id] = orderItem.id;
    };

    if (product.is_option_enabled) {
      const options = this.convertOptionToArray(optionInObject);
      this._cartItems[product.id] = { product, quantity, optionInObject };

      return this._queue(() => this.OrderItemDAO.createOrderItemWithOptions(product.id, quantity, options).then(callback));
    }

    return this._queue(() => this.OrderItemDAO.createOrderItem(product.id, quantity).then(callback));
  }

  createOrUpdateItem(product, quantity) {
    const cartItem = this.getItem(product.id);

    if (cartItem && cartItem.quantity) {
      return this.updateQuantity(product.id, quantity);
    }

    return this.createItem(product, quantity);
  }

  getItem(productId) {
    return this._cartItems[productId];
  }

  setItem(orderItem) {
    this._orderItemsIds[orderItem.product] = orderItem.id;

    const options = orderItem.options || [];
    let optionInObject = {
      preferences: [],
      extras: []
    };
    options.forEach(optionItem => {
      if (!optionItem.is_extra) {
        optionInObject.preferences.push(optionItem);
      } else {
        optionInObject.extras.push(optionItem);
      }
    });

    this._cartItems[orderItem.product] = {
      product: {
        id: orderItem.product,
        price: orderItem.price,
        name: orderItem.product_name,
        description: orderItem.product_description,
        priority: orderItem.priority,
        image: orderItem.product_image,
        thumbs: orderItem.product_thumbs,
        default_color: orderItem.product_default_color,
        default_icon: orderItem.product_default_icon,
        is_option_enabled: orderItem.product_option_enabled,
      },
      quantity: orderItem.quantity,
      optionInObject: optionInObject,
    };
  }

  getItems() {
    const items = [];

    for (let productId in this._cartItems) {
      items.push(this.getItem(productId));
    }

    return items;
  }

  clean() {
    this._cartItems = {};
  }

  /**
   * Checks if no items in the cart store.
   * @return {Boolean} true if cart store is empty, false otherwise
   */
  isEmpty() {
    return Object.keys(this._cartItems).length === 0;
  }

  setItemOptions(productId, optionInObject) {
    const cartItem = this.getItem(productId);

    if (cartItem && cartItem.quantity) {
      const options = this.convertOptionToArray(optionInObject);
      return this._modifyItem(
        productId,
        (item) => item.optionInObject = optionInObject,
        (orderItemId) => this.OrderItemDAO.updateOrderItem({
          id: orderItemId,
          product: productId,
          options: options,
          quantity: cartItem.quantity
        }),
      );
    }
  }

  /**
   * convert optionInObject to array
   *
   * @param {Object} optionInObject
   * @return {Array}
   */
  convertOptionToArray(optionInObject) {
    let options = [];
    if (!optionInObject) {
      return [];
    }

    if (optionInObject.preferences) {
      optionInObject.preferences.forEach(preference => {
        options.push(preference);
      });
    }

    if (optionInObject.extras) {
      optionInObject.extras.forEach(extra => {
        if (extra.quantity > 0) {
          options.push(extra);
        }
      });
    }

    return options;
  }
}
