/* import 3rd party modules */
import 'angular';


/**
 * UrlRequestSender is responsible for sending non-xhr requests that will
 * cause the browser to refresh.
 */
export default class UrlRequestSender {
  constructor($window) {
    this.$window = $window;
  }

  /**
   * It basically just makes browser to load a url.
   * @param  {String} url    - url to load
   */
  sendGet(url) {
    this.$window.location.href = url;
  }

  /**
   * Makes a browser to send POST request, by dynamically creating form
   * and submitting it. This is a very ugly hack, needed for Bilderlingspay integration.
   * @param  {String} url    - POST url, rendered as form action field
   * @param  {Object} params - params rendered as hidden fields
   */
  sendPost(url, params) {
    const form = angular.element('<form>');

    form.attr('method', 'POST');
    form.attr('action', url);
    form.css('display', 'none');

    for (let key in params) {
      const hiddenField = angular.element('<input>');
      hiddenField.attr('type', 'hidden');
      hiddenField.attr('name', key);
      hiddenField.attr('value', params[key]);
      form.append(hiddenField);
    }

    angular.element(this.$window.document.body).append(form);

    // submit form
    form[0].submit();

    // remove form to avoid potential bfCache problems
    form.remove();
  }
}
