const SELECTED_LANGUAGE_FIELD = 'MenuSelectedLanguage';

/**
 * Lang is responsible for managing app's language for purpose of translation
 * using Google API services. It also provides language specific utilities.
 */
export default function Lang($q, $rootScope, $window, localStorageService, CompanyDAO, EVENTS) {
  let defaultLanguage;

  /**
   * Converts language code to ISO 639-1 code.
   * E.g. 'en-US', 'en_GB', 'EN_NZ', 'en' will all become 'en'
   * @param  {String} languageCode  - language code, e.g. 'en-US'
   * @return {String}                 ISO 639-1 language code, e.g. 'en'
   */
  function getLangShortCode(languageCode) {
    if (!angular.isString(languageCode)) {
      return languageCode;
    }
    return languageCode.toLowerCase().split(/[-_]/)[0];
  }

  /**
   * Gets default language from company profile.
   * @return {Promise}                promise with default language value
   */
  function getDefaultLanguage() {
    if (defaultLanguage) {
      return $q.resolve(defaultLanguage);
    }

    return CompanyDAO.getCompany()
      .then(company => {
        defaultLanguage = company.default_language;
        return defaultLanguage;
      });
  }

  /**
   * Gets selected language from the local storage.
   * @return {Promise}                promise with selected language value
   */
  function getSelectedLanguage() {
    return getDefaultLanguage()
      .then(() => {
        return localStorageService.get(SELECTED_LANGUAGE_FIELD) || defaultLanguage;
      });
  }

  /**
   * Saves selected language into the local storage.
   * @param {String} languageCode   - ISO 639-1 code of language
   */
  function setSelectedLanguage(languageCode) {
    const previousValue = localStorageService.get(SELECTED_LANGUAGE_FIELD);

    // make sure it's an ISO 639-1 code, e.g. 'en-US' will become 'en'
    const lang = getLangShortCode(languageCode);

    // set language in local storage
    localStorageService.set(SELECTED_LANGUAGE_FIELD, lang);

    // notify if changed
    if (previousValue !== lang) {
      $rootScope.$emit(EVENTS.MENU_LANGUAGE_CHANGED);
    }
  }

  /**
   * Return user's language, based on browser settings.
   * @return {String}                 language code, e.g. 'en-Us'
   */
  function getBrowserLanguage() {
    const nav = $window.navigator;
    return nav.languages ? nav.languages[0] : (nav.language || nav.userLanguage);
  }

  return {
    getDefaultLanguage,
    getSelectedLanguage,
    setSelectedLanguage,
    getBrowserLanguage,
    getLangShortCode,
  };
}
