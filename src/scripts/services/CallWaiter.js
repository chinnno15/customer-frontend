import callwaiterTmpl from '../../templates/modals/callwaiter.jade';
import callwaiterResponseTmpl from '../../templates/modals/callWaiterResponse.jade';
import { filter } from 'lodash';

const CALL_ID = 'CALL_WAITER_ID';

/**
 * CallWaiter is responsible for checking messages from gcm
 * (Google Cloud Messages)
 */
export default function CallWaiter($q, $interval, $log, localStorageService, CallWaiterDAO, INTERVALS, WAITER_CALL_STATUSES, $rootScope, $document, TopSheet, Alert, Modal) {
  const CALL_STATUSES_I18N = {
    [WAITER_CALL_STATUSES.PENDING]: 'PENDING',
    [WAITER_CALL_STATUSES.TAKEN]: 'TAKEN',
    [WAITER_CALL_STATUSES.COMPLETED]: 'COMPLETED',
    [WAITER_CALL_STATUSES.EXPIRED]: 'EXPIRED',
    [WAITER_CALL_STATUSES.CANCELLED]: 'CANCELLED',
  };
  // so we cancel interval
  let stop;

  /**
  * generates orderCallid with CALL_ID prefix and orderId suffix.
  * @protected
  * @param  {Integer} orderId
  * @return {String}
  */
  function _generateOrderCallId(orderId) {
    if (!orderId) {
      return CALL_ID;
    }
    return `${CALL_ID}:${orderId}`;
  }

  /**
   * given orderId this returns callId.
   * @protected
   * @param  {Integer} orderId
   * @return {Integer} callId for given orderId.
   */
  function _getCallIdForOrderId(orderId) {
    const callOrderId = _generateOrderCallId(orderId);
    return localStorageService.get(callOrderId);
  }

  /**
   * given callId and orderId params, this saves the key value pair in localStorage
   * @protected
   * @param  {Object} opts
   * - orderId {Integer} Required.
   * - callId {Integer} see WAITER_CALL_STATUSES
   */
  function _saveCallIdForOrder(opts) {
    const {
      orderId,
      callId
    } = opts;

    if (!callId) {
      throw new Error('callId param is required.');
    }

    const callOrderId = _generateOrderCallId(orderId);
    localStorageService.set(callOrderId, callId);
  }

  /**
   * get all callOrderIds in localStorage
   * @protected
   * @return {Array}
   */
  function _getOrderCallIds() {
    const allKeys = localStorageService.keys();
    // filter for keys that begin with callOrderId prefix.
    return filter(allKeys, (key) => key.indexOf(CALL_ID) > -1);
  }

  /**
   * get all callIds in localStorage
   * @return {Array}
   */
  function _getCallIds() {
    const orderCallIds = _getOrderCallIds();
    return orderCallIds.map((key)=>localStorageService.get(key));
  }

  /**
   * lookup callOrderId key in storage by callId and remove it.
   * @protected
   * @throws if callId does not exist in localStorage. Don't want to call this unnecessarily
   * @param  {Integer} callId
   */
  function _clearCallOrder(callId) {
    const callIds = _getOrderCallIds();

    // reduce to object where keys are callIds
    const calls = callIds.reduce((prev, id) => {
      prev[localStorageService.get(id)] = id;
      return prev;
    }, {});

    const callOrderId = calls[callId];

    if (!callOrderId) {
      throw new Error('callOrderId was not found for given callId.');
    }
    // finally remove key from localStorage
    localStorageService.remove(callOrderId);
  }

  /**
   * show a modal that shows the current state of a call.
   * @protected
   * @param  {Object} data
   * - company_order_id {Integer}
   * - state {Integer} see WAITER_CALL_STATUSES
   */
  function _showWaiterResponse(data) {
    const { company_order_id: orderId, state } = data;

    if (!state || !orderId) {
      throw new Error('state and company_order_id are required.');
    }

    data.responseText = CALL_STATUSES_I18N[state];

    Modal.show({
      template: callwaiterResponseTmpl(),
      controller: 'CallWaiterController',
      data
    });
  }
  /**
   * Check call object returned from server and clear it from localStorage if it is no longer pending
   * show modal with new state as well.
   * @protected
   * @param  {Object} call
   */
  function _checkCallState(call) {
    const { company_order_id, state, id: callId } = call;
    // if the call is not pending anymore
    if (state !== WAITER_CALL_STATUSES.PENDING) {
      _showWaiterResponse({state, company_order_id});
      _clearCallOrder(callId);
    }
  }
  /**
   * get state for each callId in localStorage from server
   * @return {Promise} resolves call to server succeeds
   */
  function _checkCalls() {
    const callIds = _getCallIds();
    if (callIds.length > 0) {
      return CallWaiterDAO.getCalls(callIds)
        .then(waiterCalls => { // eslint-disable-line no-unused-vars
          waiterCalls.forEach(call => _checkCallState(call));
        })
        .catch(err => {
          $log.error('Failed to fetch waiter call', err);
        });
    }
    return $q.resolve();
  }

  /**
   * check server for call status intermittently.
   * @protected
   */
  function _startCallChecks() {
    stop = $interval(() => {
      _checkCalls();
    }, INTERVALS.FETCH_WAITER_CALL);
  }

  /**
   * Post a call to the server. If the post is succesful than it is saved to localstorage for future
   * checks for waiters response.
   * @param  {Integer} orderId [description]
   * @param  {String} message [description]
   * @return {Promise} Resolves if post to server is succesful and callId is saved to localstorage
   * succesfully.
   */
  function _createCall(orderId, message) {
    return CallWaiterDAO.createCall(orderId, message)
      .then(waiterCall => {
        if (waiterCall.id) {
          let { id: callId } = waiterCall;
          // save to localstorage
          _saveCallIdForOrder({orderId, callId});

          return $q.resolve(callId);
        }

        return $q.reject('MISSING_CALL_WAITER_ID');
      });
  }

  /**
   * Checks if call was already made, by checking localStorage.
   * @return {Boolean} true if call was already made, false otherwise
   */
  function wasCallMade(orderId) {
    const callOrderId = _generateOrderCallId(orderId);
    return !!localStorageService.get(callOrderId);
  }

  /**
   * Calls for waiter by sending notification and waiting for response.
   * @return {Promise} resolves with new call state once waiter responds to a call or call expires
   */
  function callWaiter(orderId, message) {
    // get callId from localStorageService or create new waiter call
    const callId = _getCallIdForOrderId(orderId);
    if (callId) {
      return $q.reject(new Error('the waiter for this order has already been called.'));
    }

    return _createCall(orderId, message)
      .catch((err = {}) => {
        const {
          data = {}
        } = err;
        $log.error('Failed to call the waiter', err);
        Alert.error(`CALL_WAITER.${data.detail || 'FAILED_TO_CALL'}`);
      });
  }

  /**
   * initializes this service and starts checking for state of existing calls.
   */
  function init() {
    if (!stop) {
      _startCallChecks();
    }
  }

  /**
   * Shows waiter request dialog with message field.
   */
  function showCallWaiterMessageSheet(orderId) {
    if (wasCallMade(orderId)) {
      Alert.error('CALL_WAITER.CallWaiterNotifiedAlready');
      return;
    }
    TopSheet.show({
      template: callwaiterTmpl(),
      controller: 'CallWaiterController',
      controllerAs: 'ctrl',
      parentEl: '.mainContainer',
      data: {orderId}
    });
  }
  // public API
  return {
    callWaiter,
    showCallWaiterMessageSheet,
    wasCallMade,
    init,
    _generateOrderCallId,
    _clearCallOrder,
    _getCallIds,
    _checkCalls,
  };
}
