/**
 * Currency service is responsible for fetching and storing currency.
 */
export default function Currency($log, $rootScope, Alert, Company, CurrencyDAO, EVENTS) {
  let currency = null;

  function loadCurrency(company) {
    CurrencyDAO.getCurrency(company.default_currency)
      .then(fetchedCurrency => {
        currency = fetchedCurrency;
        return fetchedCurrency;
      })
      .catch(err => {
        $log.error('Failed to fetch company or currency', err);
        Alert.error('Failed to initialize currency.');
      });
  }

  function initialize() {
    const company = Company.getCompany();

    // company may not be available just yet
    if (company) {
      loadCurrency(company);
    }
  }

  // initialize for the first time
  initialize();

  // re-initialize if company details changed
  $rootScope.$on(EVENTS.COMPANY_UPDATED, () => {
    initialize();
  });

  /**
   * Gets currency in sync way.
   * @return {Object}  currency or null if currency is not loaded yet
   */
  function getCurrency() {
    return currency;
  }

  return {
    getCurrency,
  };
}
