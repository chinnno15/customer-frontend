/**
 * Cart is responsible for storing products that wait for checkout.
 * @see CartItemsStore
 */
export default class Cart {
  constructor($q, $rootScope, CartItemsStore, EVENTS) {
    Object.assign(this, { $q, $rootScope, CartItemsStore, EVENTS });

    this._totals = { price: 0, quantity: 0 };
    this._products = [];
    this.initializeDeferred = $q.defer();
  }

  // sends cart update event and clears cached data
  _notifyOnChange(afterStateChange=false) {
    const { $rootScope, EVENTS } = this;

    // clear dirty data
    this._totals = null;
    this._products = null;

    // notify about quantity change
    if (afterStateChange) {
      // it sends cart update event after view state change
      const deregister = $rootScope.$on('$stateChangeSuccess', () => {
        $rootScope.$emit(EVENTS.CART_UPDATED);
        deregister();
      });
    } else {
      $rootScope.$emit(EVENTS.CART_UPDATED);
    }
  }

  /**
   * Initializes cart with order items fetched from the server, but only if cart is empty.
   * @param {Boolean} notifyOnChange  - should notification event be send if cart state changes (default: true)
   * @return {Promise}                  resolves when cart gets initialized
   */
  initialize(notifyOnChange=true) {
    if (!this.CartItemsStore.isEmpty()) {
      return this.$q.resolve();
    }

    return this.CartItemsStore.initialize().then(() => {
      this.initializeDeferred.resolve();
      if (notifyOnChange) {
        this._notifyOnChange();
      }
    });
  }

  /**
   * Waits for cart to be fully initialized.
   * @return {Promise}                  resolves if cart is already initialized
   */
  waitForInitializedCart() {
    return this.initializeDeferred.promise;
  }

  /**
   * Cleans cart and reinitializes it with fresh items from the server.
   * @return {Promise}                  resolves when cart gets initialized
   */
  reload() {
    this.CartItemsStore.clean();
    return this.initialize();
  }

  /**
   * Sets the quantity of the product in the cart.
   * @param {Object} product        - product object
   * @param {Integer} quantity      - quantity value
   * @param {Object} optionInObject - product object
   */
  setQuantity(product, quantity, optionInObject) {
    let promise;

    if (quantity < 1) {
      // remove cart item if quantity is 0
      promise = this.CartItemsStore.deleteItem(product.id);
    } else {
      // update quantity or add new cart item
      const cartItem = this.CartItemsStore.getItem(product.id);

      if (cartItem && cartItem.quantity) {
        promise = this.CartItemsStore.updateQuantity(product.id, quantity);
      } else {
        promise = this.CartItemsStore.createItem(product, quantity, optionInObject);
      }
    }

    this._notifyOnChange();
    return promise;
  }

  /**
   * Gets the product's quantity in the cart.
   * @param  {Integer} productId - product id
   * @return {Integer} quantity, 0 if product is not in the cart
   */
  getQuantity(productId) {
    const cartItem = this.CartItemsStore.getItem(productId);
    return cartItem ? cartItem.quantity : 0;
  }

  /**
   * Computes total price and total number of items in the cart.
   * Totals values are cached, an won't be re-computed until the state of the cart
   * changes. State of the cart can change either by calling:
   * setQuantity(), clean(), initialize(), reload() or deleteAllItems().
   *
   * @return {Object} totals object: { price, quantity }
   */
  getTotals() {
    if (!this._totals) {
      this._totals = this.CartItemsStore.getItems().reduce((totals, item) => {
        totals.quantity += item.quantity;
        const price = this.getItemPrice(item);
        totals.price += price * item.quantity;

        return totals;
      }, { price: 0, quantity: 0 });
    }

    return this._totals;
  }

  /**
   * Creates an array of products in the cart traversing the internal map
   * representation of the cart state. Products array is cached, an won't be
   * re-created until the state of the cart changes.
   * State of the cart can change either by calling:
   * setQuantity(), clean(), initialize(), reload() or deleteAllItems().
   *
   * @return {Array} list of products in the cart
   */
  getProducts() {
    if (!this._products) {
      this._products = this.CartItemsStore.getItems();
    }

    return this._products;
  }

  /**
   * Clears the cart internal state (all products quantities). It does not remove
   * the order items from the backend.
   * @param {Boolean} notifyAfterStateChange - if true it sends cart changed event
   * only after view $state change
   */
  clean(notifyAfterStateChange=false) {
    this.CartItemsStore.clean();
    this._notifyOnChange(notifyAfterStateChange);
  }

  /**
   * Clears the cart internal state (all products quantities) and removes
   * all order items from the backend by sending DELETE requests.
   *
   * @return {Promise} promise for order items DELETE requests
   */
  deleteAllItems() {
    const promise = this.CartItemsStore.deleteAllItems();
    this._notifyOnChange();
    return promise;
  }

  /**
  * Set preferences for cart
  * An menu item has a default price but when this menu enable is_option_enabled
  * It allow end user to custom their menu with preference and extras (OPTIONS)
  *
  * @param {Object} product product need to set option
  * @param {Object} option in object - which will be converted to array
  *
  * @return promise
  */
  setOption(product, optionInObject) {
    if (!product.is_option_enabled) {
      return this.$q.resolve();
    }

    const promise = this.CartItemsStore.setItemOptions(product.id, optionInObject);
    this._notifyOnChange();

    return promise;
  }

  /**
   * Get current option of item
   * @param  {Integer} productId - product id
   * @return {Object} option - current option of this item
   * contains: preferences and extras in attribute
   */
  getOption(productId) {
    const cartItem = this.CartItemsStore.getItem(productId);
    return cartItem ? cartItem.optionInObject : {};
  }

  /**
   * Get exactly price of an item
   * Price will be calculate via is_option_enabled
   *
   * @param {Object} orderItem
   *
   * @return {Float} price
   */
  getItemPrice(item) {
    if (item.product.is_option_enabled && item.optionInObject) {
      let price = 0;

      item.optionInObject.preferences.forEach(preference => {
        if (preference) {
          price+= parseFloat(preference.price);
        }
      });

      item.optionInObject.extras.forEach(extra => {
        if (extra.quantity) {
          price+= extra.quantity * parseFloat(extra.price);
        }
      });

      return price;
    }

    return item.product.price;
  }
}
