/**
 * Company service is responsible for fetching and storing company.
 */
export default function Company($q, $log, $interval, $rootScope, CompanyDAO, Alert, INTERVALS, EVENTS) {
  let company = null;

  function loadCompany() {
    CompanyDAO.getCompany()
      .then(fetchedCompany => {
        company = fetchedCompany;

        // notify about company change
        $rootScope.$emit(EVENTS.COMPANY_UPDATED);
      })
      .catch(err => {
        $log.error('Failed to fetch pending orders count', err);
        Alert.error('COMPANY.DATA_FAILED');
      });
  }

  /**
   * Gets company in sync way.
   * @return {Object}  company or null if company is not loaded yet
   */
  function getCompany() {
    return company;
  }

  function initialize() {
    // load company for the first time
    loadCompany();

    // start the reloading interval
    $interval(() => {
      // fetch fresh copy of company
      loadCompany();
    }, INTERVALS.FETCH_COMPANY);
  }

  return {
    getCompany,
    initialize,
  };
}
