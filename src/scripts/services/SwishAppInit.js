/**
 * SwishAppInit is responsible for querying orderpoint API endpoint.
 */
export default function SwishAppInit($rootScope, $log, $state, $window, $timeout, $q, OrderPointStore, StripePayment, Alert, Translator, Cart, Company, I18n, EVENTS, STATES, localStorageService, STORAGE, OrderPointDAO) {
  this._initializeCart = (cleanCart) => {
    const notifyOnChange = !cleanCart;

    return Cart.initialize(notifyOnChange)
      .then(() => {
        // we need to make sure cart is empty when user changes the orderpoint
        // this is a quick fix to remove all order items from the backend,
        // but utlimately customer's order items should be unique per order point.
        return cleanCart ? Cart.deleteAllItems() : $q.resolve();
      });
  };

  this._registerOrderPoint = (storeName, orderPointName) => {
    const storedOrderPointName = localStorageService.get(STORAGE.ORDER_POINT_NAME);
    const storedStoreName = localStorageService.get(STORAGE.STORE_NAME);

    let cleanCart = false;

    if (storeName !== storedStoreName || orderPointName !== storedOrderPointName) {
      cleanCart = true;
    }

    OrderPointStore.register(storeName, orderPointName)
      .catch(err => {
        $state.go(STATES.NO_ORDERPOINT);
        return $q.reject(err);
      })
      .then(() => {
        $q.all([
          this._initializeCart(cleanCart),
          Company.initialize(),
          StripePayment.initializeStripe(),
        ])
        .catch(err => {
          $log.error('Swish failed to initialize correctly.', err);
          Alert.error('GENERAL.INIT_FAILED');
        })
        .finally(() => {
          if ($state.current.name === STATES.MAIN) {
            $state.go(STATES.MENU);
          }
        });
      });
  };

  this._windowListeners = () => {
    // add online / offline listeners
    $window.addEventListener('offline', () => {
      Alert.error('GENERAL.OFFLINE_NOTIFICATION');
    }, false);

    $window.addEventListener('online', () => {
      Alert.info('GENERAL.ONLINE_NOTIFICATION');
    }, false);
  };

  this._rootScopeListeners = () => {
     // re-init Cart
    $rootScope.$on(EVENTS.MENU_LANGUAGE_CHANGED, () => {
      Cart.reload();
    });
    // reload whole app
    $rootScope.$on(EVENTS.STRIPE_KEY_CHANGED, () => {
      $log.info('Stripe key changed, app will be reloaded');
      Alert.info('GENERAL.STRIPE_KEY_CHANGED');
      $timeout(() => $window.location.reload(), 3 * 1000);
    });
  };

  this._initializeTranslateService = () => {
    Translator.initialize();
  };

  this._initializeI18nService = () => {
    // Load default language
    I18n.load('en');
  };

  /**
   * Public register order point
   *
   * @param {String} storeName
   * @param {String} orderPointName
   */
  this.registerOrderPoint = (storeName, orderPointName) => {
    this._registerOrderPoint(storeName, orderPointName);
  };


  /**
   * Backward compatibility for old url structure
   */
  this._handleOrderPoinntURL = () => {
    $rootScope.$on('$stateChangeStart', (event, toState, toParams) => {
      if (STATES.ORDER_POINT === toState.name) {
        const orderPointId = toParams.orderPointId;
        const uuid = orderPointId.substr(orderPointId.length - 36);

        OrderPointDAO.getOrderPoint(uuid)
          .then(orderPoint => {
            $state.go(STATES.MAIN, {
              storeName: orderPoint.store_name,
              orderPointName: orderPoint.orderpoint_name,
            });
          })
          .catch(() => {
            $state.go(STATES.NO_ORDERPOINT);
          });
      }
    });
  };

  this.initialize = () => {
    this._handleOrderPoinntURL();
    this._windowListeners();
    this._rootScopeListeners();
    this._initializeTranslateService();
    this._initializeI18nService();
  };
};
