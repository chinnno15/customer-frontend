import modalTmpl from '../../templates/modals/modal.jade';

const CONTAINER_CLASS = '.overlay.active';
/**
 * Modal service is responsible for displaying modal modal on the top of the screen.
 */
export default function Modal($rootScope, $controller, $compile, $animate) {
  /**
  * simplify finding elements w/ jqlite
  * @protected
  * @param  {String} selector
  * @return {Element}
  */
  function _findEl(selector) {
    return angular.element(document.querySelector(selector));
  }

  /**
   * remove .modal element from DOM
   * @protected
   */
  function close() {
    const $modal = _findEl(CONTAINER_CLASS);
    $modal.remove();
  }

  /**
   * show top sheet containing given template
   * @param  {[type]} opts options:
   * - controller {String} a controller to be used with template.
   * - scope {Scope} optional scope to be used with template.
   * - template {String} html template
   * - controllerAs {String} label to used for controller
   * - parentEl {String} selector to used to insert modal into appropriate z-index context.
   * - data {Object} data to be merged into scope for use with template.
   * @throws if template or controller is undefined
   */
  function show(opts) {
    const {
      controller,
      controllerAs: label = 'ctrl',
      scope = $rootScope.$new(),
      template,
      parentEl = 'body',
      data = {}
    } = opts;

    const $parentEl = _findEl(parentEl);

    const $modalTmpl = angular.element(modalTmpl({template}));

    if (!template) {
      throw new Error('template is required.');
    }

    if (!controller) {
      throw new Error('controller is required.');
    }

    angular.extend(scope, data);

    const locals = {};
    Object.assign(locals, {$scope: scope, $element: $modalTmpl});
    const ctrlInstance = $controller(controller, locals, true, label);
    scope[label] = ctrlInstance();

    // bind service methods to controller for use in template
    Object.assign(scope[label], {$animate, _findEl, close});

    // append to DOM
    const $modalEl = $compile($modalTmpl)(scope);
    $parentEl.append($modalEl);
  }

  return {
    close,
    show,
  };
}
