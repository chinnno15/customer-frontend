import errorTmpl from '../../templates/alerts/error.jade';

/**
 * Alert is responsible for displaying success and error messages to the user
 * It is a simple wrapper over 3rd party Flash library.
 */
export default function Alert(Flash, I18n) {
  return {
    info(msg) {
      return I18n.translate(msg)
        .then(str => Flash.create('info', str));
    },

    success(msg) {
      return I18n.translate(msg)
        .then(str => Flash.create('success', str));
    },

    error(msg, detail) {
      return I18n.translate(msg)
        .then(str => Flash.create('danger', errorTmpl({ str, detail })));
    }
  };
};
