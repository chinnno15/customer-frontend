// Just some random value to show the spinners for some time when redirect is happening.
const LOADER_CALLBACK_TIMEOUT = 3000;

/**
 * Checkout service is implementing different checkout flows for different payment providers.
 */
export default class Checkout {
  constructor($state, $timeout, StripePayment, PaymentDAO, OrdersStore, Cart, UrlRequestSender, Currency, STATES, PAYMENT_PROVIDERS) {
    // IMPORTANT! Due to the browser popup blocking feature and Stripe using `window.open()`,
    // `StripePayment.checkout()` must be called directly in this function, not in the callback.
    // Mobile browsers will generally block the popup if it's shown as a result
    // of a callback (e.g. ajax request) and not a direct user action (e.g. click event).
    function checkoutWithStripe(comment, loadedCallback) {
      const price = Math.ceil(Cart.getTotals().price * 100);
      const currency = Currency.getCurrency().name;

      return StripePayment.checkout(price, currency)
        .then((token) => {
          return PaymentDAO.checkoutWithStripe(token, comment);
        })
        .then((order) => {
          loadedCallback();
          Cart.clean(true);
          OrdersStore.setLatestOrder(order);

          // go to the receipt state
          $state.go(STATES.RECEIPT);
        });
    };

    function checkoutWithGear(comment, loadedCallback) {
      return PaymentDAO.checkoutWithGear(comment)
        .then((result) => {
          OrdersStore.setLatestOrder(result.swish_order);

          // redirect to Gear payment page
          UrlRequestSender.sendGet(result.payment_url);

          // stop loader (hackish code)

          // Intuiton says that this is not really needed as we
          // are redirecting user away from the custom-frontend app,
          // however the bfCache remembers the DOM and scripts state,
          // so when user presses the back button the whole app is not loaded
          // from scratch.
          $timeout(loadedCallback, LOADER_CALLBACK_TIMEOUT);
        });
    };

    function checkoutWithOpenBank(comment, loadedCallback) {
      return PaymentDAO.checkoutWithOpenBank(comment)
        .then((result) => {
          OrdersStore.setLatestOrder(result.swish_order);
          UrlRequestSender.sendGet(result.payment_url);
          $timeout(loadedCallback, LOADER_CALLBACK_TIMEOUT);
        });
    };

    function checkoutWithCash(comment, loadedCallback) {
      return PaymentDAO.checkoutWithCash(comment)
        .then((result) => {
          Cart.clean(true);
          OrdersStore.setLatestOrder(result);
          $state.go(STATES.RECEIPT);
          $timeout(loadedCallback, LOADER_CALLBACK_TIMEOUT);
        });
    };

    function checkoutWithBilderlingspay(comment, loadedCallback) {
      return PaymentDAO.checkoutWithBilderlingspay(comment)
        .then((result) => {
          const { payment_url, order, bilderlingspay } = result;

          OrdersStore.setLatestOrder(order);

          // fix lang property name, as Bilderlingpay is expecting '-lang'
          bilderlingspay['-lang'] = bilderlingspay.lang;
          delete bilderlingspay.lang;

          // format price
          bilderlingspay.PAYMENT_AMOUNT = bilderlingspay.PAYMENT_AMOUNT.toFixed(2);

          // redirect to Bilderlingpay page
          UrlRequestSender.sendPost(payment_url, bilderlingspay);

          // stop loader (hackish code)

          // Intuiton says that this is not really needed as we
          // are redirecting user away from the custom-frontend app,
          // however the bfCache remembers the DOM and scripts state,
          // so when user presses the back button the whole app is not loaded
          // from scratch.
          $timeout(loadedCallback, LOADER_CALLBACK_TIMEOUT);
        });
    };

    this.checkouts = {
      [PAYMENT_PROVIDERS.STRIPE]: checkoutWithStripe,
      [PAYMENT_PROVIDERS.GEAR]: checkoutWithGear,
      [PAYMENT_PROVIDERS.BILDERLINGSPAY]: checkoutWithBilderlingspay,
      [PAYMENT_PROVIDERS.OPENBANK]: checkoutWithOpenBank,
      [PAYMENT_PROVIDERS.CASH]: checkoutWithCash,
    };
  }

  checkout(providerId, comment, loadedCallback) {
    return this.checkouts[providerId](comment, loadedCallback);
  }
};
