const ABSOLUTE_URL_REQUEST = /^https?:\/\//;
const HTML_FILE_REQUEST = /.html$/;
const LEADING_OR_TRAILING_SLASH = /^\/|\/$/g;


/**
 * SwishApiInterceptor is responsible for making $http requests Swish API friendly
 * by adding API host URL prefix to all requests.
 */
export default (ENV, $exceptionHandler, $q) => ({

  request: (config) => {
    // exclude absolute URL request and HTML file requests (angular templates)
    if (!ABSOLUTE_URL_REQUEST.test(config.url) && !HTML_FILE_REQUEST.test(config.url)) {
      let server = ENV.apiRoot.replace(LEADING_OR_TRAILING_SLASH, '');
      let resource = config.url.replace(LEADING_OR_TRAILING_SLASH, '');
      config.url = server + '/' + resource + '/';
    }
    return config;
  },
  // responsible to send all filtered errors to the Sentry API
  responseError: (res) => {
    try {
      if (res.status === 500) {
        if (res.data) {
          if (res.data.constructor.toString().indexOf('String')) {
            throw (new Error(JSON.stringify(res.data.substr(0, 250) + '...')));
          } else {
            throw (new Error(JSON.stringify(res.data)));
          }
        }
      }
    } catch (e) {
      $exceptionHandler(e);
    } finally {
      return $q.reject(res);
    }
  }

});
