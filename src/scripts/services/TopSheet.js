import topsheetTmpl from '../../templates/modals/topsheet.jade';

/**
 * TopSheet service is responsible for displaying topsheet modal on the top of the screen.
 */
export default function TopSheet($rootScope, $document, $controller, $compile, $animate, $timeout, $q) {
  /**
  * simplify finding elements w/ jqlite
  * @protected
  * @param  {String} selector
  * @return {Element}
  */
  function _findEl(selector) {
    return angular.element(document.querySelectorAll(selector));
  }

  /**
   * remove .topsheet element from DOM
   * @protected
   */
  function _close() {
    const $topsheet = _findEl('.topsheet');
    $topsheet.remove();
  }

  /**
   * returns true if topsheet is open.
   */
  function isOpen() {
    const $topsheet = _findEl('.topsheet');
    return $topsheet.length > 0;
  }

  /**
   * Hides topsheet and removes it from DOM.
   * @return {Promise} resolves animation result
   */
  function close() {
    const $topsheet = _findEl('.topsheet');

    if ($topsheet.length) {
      const $container = _findEl('.topsheet-container');
      const $overlay = _findEl('.topsheet-overlay');

      return $animate.setClass($container, 'close', 'open')
        .then(() => $animate.setClass($overlay, 'hide', 'show'))
        .then(() => _close());
    }

    return $q.resolve();
  }

  /**
   * show top sheet containing given template
   * @param  {[type]} opts options:
   * - controller {String} a controller to be used with template.
   * - scope {Scope} optional scope to be used with template.
   * - template {String} html template
   * - controllerAs {String} label to used for controller
   * - parentEl {String} jqlite selector to used to insert topsheet into appropriate z-index context.
   * - data {Object} data to be merged into scope for use with template.
   * - canClose {Boolean} show close button
   * @throws if template or controller is undefined
   */
  function show(opts) {
    const {
      controller,
      controllerAs: label = 'ctrl',
      scope = $rootScope.$new(),
      template,
      parentEl = 'body',
      data = {},
      canClose = true
    } = opts;
    const $parentEl = _findEl(parentEl);
    const $topsheetTmpl = angular.element(topsheetTmpl({template}));

    if (!template) {
      throw new Error('template is required.');
    }

    angular.extend(scope, data);

    const locals = {};
    Object.assign(locals, {$scope: scope, $element: $topsheetTmpl});
    if (controller) {
      const ctrlInstance = $controller(controller, locals, true, label);
      scope[label] = ctrlInstance();
      // bind service methods to controller for use in template
      Object.assign(scope[label], {$animate, _findEl, close, _close});
    }


    // append to DOM
    const $topsheetEl = $compile($topsheetTmpl)(scope);
    $parentEl.append($topsheetEl);

    const $overlay = _findEl('.topsheet-overlay');
    const $container = _findEl('.topsheet-container');

    const $closeEl = _findEl('.topsheet-container > .close');

    if (canClose) {
      // close handler
      $closeEl.bind('click', this.close.bind(this));
    } else {
      $closeEl.remove();
    }

    return $animate.addClass($overlay, 'show')
      .then(() => $animate.addClass($container, 'open'));
  }

  return {
    close,
    show,
    isOpen,
  };
}
