/**
 * MultiTranslator is responsible for translating selected properties of multiple
 * objects using GoogleTranslator service.
 * @see GoogleTranslator
 */
export default function MultiTranslator($q, GoogleTranslator) {
  /**
   * Translates selected properties of multiple objects.
   * @param  {Array} objects             - objects to translate
   * @param  {Array} properties          - properties that will be translated
   * @param  {String} sourceLanguage     - ISO 639-1 code of language to translate from
   * @param  {String} targetLanguage     - ISO 639-1 code of language to translate to
   * @return {Promise}                     promise with translated objects
   */
  function translate(objects, properties, sourceLanguage, targetLanguage) {
    const objectsToTranslate = angular.copy(objects);
    const textsToTranslate = [];
    const textsToTranslateMap = [];

    // get objects values for translation
    objectsToTranslate.forEach(object => {
      properties.forEach(property => {
        if (object[property]) {
          textsToTranslate.push(object[property]);

          // also keep the map to prop to-be-translated
          //   so we can populate the translation later
          textsToTranslateMap.push({ object, property });
        }
      });
    });

    const nothingToTranslate = !textsToTranslate.length;
    const languagesNotDefined = !sourceLanguage || !targetLanguage;
    const translationToSameLanguage = sourceLanguage === targetLanguage;

    if (nothingToTranslate || languagesNotDefined || translationToSameLanguage) {
      return $q.resolve(objects);
    }

    return GoogleTranslator.translate(textsToTranslate, sourceLanguage, targetLanguage)
      .then(translations => {
        // populate the translations
        translations.forEach((item, idx) => {
          const { object, property } = textsToTranslateMap[idx];
          object[property] = item.translatedText;
        });

        return objectsToTranslate;
      });
  };

  return {
    translate
  };
}
