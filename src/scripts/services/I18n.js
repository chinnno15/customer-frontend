export default function I18n($translate) {
  return {
    /**
     * @name I18n.load
     * @method
     * @param {String} languageCode
     * @return {I18n} this
     */
    load(langCode) {
      if (langCode !== this.getLangCode()) {
        $translate.use(langCode);
      }
      return this;
    },

    getLangCode() {
      return $translate.use();
    },

    /**
     * @name I18n.translate
     * @method
     * @param {String} key
     */
    translate(key) {
      return $translate(key);
    },
  };
};
