/**
 * GoogleTranslator is responsible for translating text using Google Translate API
 */
export default function GoogleTranslator($q, GoogleApiLoader) {
  let requestQueue = []; // used when Google API is not loaded yet
  let gapi;

  function sendTranslateRequest(textOrArray, sourceLanguage, targetLanguage) {
    const deferred = $q.defer();

    gapi.client.request({
      path: '/language/translate/v2',
      method: 'GET',
      params: {
        q: textOrArray,
        source: sourceLanguage,
        target: targetLanguage,
      },
    })
    .execute(response => {
      if (response.error) {
        deferred.reject(response.error);
      } else {
        deferred.resolve(response.data.translations);
      }
    });

    return deferred.promise;
  }

  function queueTranslateRequest(textOrArray, sourceLanguage, targetLanguage) {
    const deferred = $q.defer();

    requestQueue.push(() => {
      deferred.resolve(sendTranslateRequest(textOrArray, sourceLanguage, targetLanguage));
    });

    return deferred.promise;
  }

  function sendPendingRequests() {
    const promises = requestQueue.map(request => request());

    // clear the queue
    requestQueue = [];

    return $q.all(promises);
  }

  /**
   * Translates text using Google Translate API.
   * @param  {String|Array} textOrArray  - text or texts to translate
   * @param  {String} sourceLanguage     - ISO 639-1 code of language to translate from
   * @param  {String} targetLanguage     - ISO 639-1 code of language to translate to
   * @return {Promise}                     promise with Google API translations
   */
  function translate(textOrArray, sourceLanguage, targetLanguage) {
    if (GoogleApiLoader.isLoaded() && gapi) {
      // just send the request is Google API is loaded
      return sendTranslateRequest(textOrArray, sourceLanguage, targetLanguage);
    }

    // can't make requests if Google API not loaded yet, queue it for later
    return queueTranslateRequest(textOrArray, sourceLanguage, targetLanguage);
  }

  /**
   * Initializes GoogleTranslator by loading Google API and sending all
   * queued translate requests.
   * @return {Promise}
   */
  function initialize() {
    // load the Google API and then send all queued translate requests
    return GoogleApiLoader.load()
      .then(gapiInstance => {
        gapi = gapiInstance;
        return sendPendingRequests();
      });
  }

  return {
    initialize,
    translate,
  };
};
