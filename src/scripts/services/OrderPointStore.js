/**
 * OrderPointStore is responsible for caching order point object and saving it's store name and orderpoint name to local storage.
 */
export default function OrderPointStore($rootScope, $q, $state, localStorageService, OrderPointDAO, CompanyDAO, I18n, STORAGE, EVENTS) {
  const ERRORS = {
    NO_UUID: 'UUID_NOT_FOUND',
  };

  const orderPointDeferred = $q.defer();
  let _isOrderPointRegistered = false;

  /**
   * Returns order point the app is registered at.
   * @return {Promise}                resolves with order point data
   */
  function getOrderPoint() {
    return orderPointDeferred.promise;
  }

  /**
   * Is orderpoint available?
   */
  function isAvailable() {
    return getOrderPoint()
      .then(() => CompanyDAO.getCompany())
      .then(company => {
        // We should always get order point from company instead of the cached
        // orderPointDeferred.promise, as chances are we may have the edge case
        // when people go to the payment page when the store is open, but then
        // click the pay button when it closes and see nothing.
        const orderPoint = company.order_point;

        if (!orderPoint.is_active) {
          return $q.reject({
            error: 'INACTIVE',
          });
        }

        if (!orderPoint.is_open) {
          const labels = [null, 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'];
          const schedule = orderPoint.schedule;

          // append labels along with weekday
          // to easily translate, and human readable
          schedule.forEach(day => {
            day.label = labels[day.weekday];
          });

          return $q.reject({
            error: 'CLOSED',
            schedule,
          });
        }

        return orderPoint;
      });
  }

  /**
   * Registers the app with the orderpoint.
   * @param  {String} store          - store name
   * @param  {String} name           - order point name
   * @return {Promise}                   resolves when all requests finish
   */
  function register(store, name) {
    const storedOrderPointName = localStorageService.get(STORAGE.ORDER_POINT_NAME);

    // reject with error if no order point or no store
    if (!store || !name) {
      return $q.reject(ERRORS.NO_UUID);
    }

    if (!storedOrderPointName || name !== storedOrderPointName) {
      localStorageService.set('accept_terms', false);
    }

    // register with order point
    return OrderPointDAO.register(store, name)
      .then(orderPoint => {
        if (orderPoint.current_language) {
          I18n.load(orderPoint.current_language);
        }
        orderPointDeferred.resolve(orderPoint);
        localStorageService.set(STORAGE.ORDER_POINT_NAME, orderPoint.name);
        localStorageService.set(STORAGE.STORE_NAME, store);

        _isOrderPointRegistered = true;
        $rootScope.$emit(EVENTS.ORDERPOINT_REGISTERED);
      });
  }

  /**
   * check if order point registered
   */
  function isOrderPointRegistered() {
    return _isOrderPointRegistered;
  }

  return {
    register,
    getOrderPoint,
    isAvailable,
    ERRORS,
    isOrderPointRegistered,
  };
};
