#!/bin/bash
set -e

BRANCH=`git rev-parse --abbrev-ref HEAD`
echo "Deploying ${BRANCH} branch to staging..."

git checkout prototype
git merge ${BRANCH}
git push origin prototype
git push prototype-server prototype
fab s:prototype deploy
git checkout ${BRANCH}
