import ngConstant from 'gulp-ng-constant';

gulp.task('config', () => {
  const envConfig = { ENV: config.envConfig };

  return ngConstant({
    name: 'customerApp.env',
    constants: envConfig,
    stream: true
  })
  .pipe(gulp.dest('public'));
});
