import checkDependencies from 'check-dependencies';


gulp.task('check-dependencies', () => sequence(['check-bower', 'check-npm']));

gulp.task('check-bower', () => {
  checkDependencies.sync({
      install: true,
      verbose: true,
      packageManager:'bower'
  });
});

gulp.task('check-npm', () => {
  checkDependencies.sync({
      install: true,
      verbose: true,
      packageManager:'npm'
  });
});
