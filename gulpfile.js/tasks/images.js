gulp.task('images:watch', () => {
  gulp.watch(['src/img/**/*.(jpeg|gif|png|icon|jpg|svg|tiff)'], ['images:move']);
});

gulp.taskSync('images:move', () => {
  gulp.src(['src/img/**/*.*'])
    .pipe($.changed('public/assets/img'))
    .pipe($.size({ title: 'images' }))
    .pipe(gulp.dest('public/assets/img'));
});
