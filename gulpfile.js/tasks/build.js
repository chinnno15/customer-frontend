gulp.task('build', () => {
  return sequence(
    'clean',
    'config',
    'test:unit', // yep, always make sure the tests are passed
    [
      'bundle',
      'images:move'
    ]
  )
});
