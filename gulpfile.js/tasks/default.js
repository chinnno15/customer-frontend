gulp.task('default', () => {
  const tasks = [];

  // check-dependencies is handy, but should be skipable
  //   to save us sometime with everyday development,
  //   especially with slow-machine guys
  if(false !== config.argv['check-dependencies']) {
    tasks.push('check-dependencies');
  }

  tasks.push('clean', 'config');

  // also we should also test before starting a new dev session
  //   to make sure things run smoothly before we start touching it,
  //   but should be skipable with the same reason as above
  if(false !== config.argv['test']) {
    tasks.push('test');
  }

  // browserSync and live reload are cool extra features,
  //   but I personally don't use it much often.
  if(false !== config.argv['browser-sync']) {
    tasks.push('browserSync');
  }

  // the idea is we need `bundle` to run only after images & translation ready
  //   and don't want the `server` to block it, also leave low-priority tasks
  //   at the end (like images:watch ..)
  const preBundleTasks = ['images:move'];
  const postBundleTasks = ['images:watch'];

  tasks.push(
    preBundleTasks,
    [
      'server',
      'bundle',
    ],
    postBundleTasks,
  );

  return sequence.apply(null, tasks);
});
