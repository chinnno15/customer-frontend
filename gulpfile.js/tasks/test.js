import { Server } from 'karma';
import path from 'path';
import childProcess from 'child_process';
import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';


gulp.task('test', () => {
  sequence('clean', 'config', 'test:unit');
});

gulp.task('test:unit', done => {
  new Server({
    configFile: path.resolve(__dirname, '../../tests/karma.config.js'),
    singleRun: !config.argv.nonstop
  }, done).start();
});

gulp.task('test:e2e', () => {
  return sequence(
    'protractor:wm-update',
    'protractor:run'
  );
});

gulp.task('protractor:wm-update', done => {
  // similar to running
  // `node_modules/webdriver-manager/protractor/bin/webdriver-manager update`
  childProcess.spawn(
    getProtractorBinary('webdriver-manager'),
    ['update'],
    { stdio: 'inherit' }
  )
    .once('close', done);
});

gulp.task('protractor:run', done => {
  const wm = childProcess.spawn(
    getProtractorBinary('webdriver-manager'),
    ['start']
  );

  wm.stdout.on('data', listenToStdout);
  wm.stderr.on('data', listenToStdout);

  function listenToStdout(data) {
    if(/\bup and running\b/i.test(data.toString())) {
      new WebpackDevServer(webpack(webpackConfig), webpackConfig.devServer)
        .listen(config.server.port, config.server.host, (err, result) => {
          console.log('webpack development server is listening at %s', config.server.url);

          childProcess
            .spawn(
              getProtractorBinary('protractor'),
              ['tests/protractor.config.js'],
              { stdio: 'inherit' }
            )
            .once('close', () => {
              wm.kill();
            });
        });
    }
  }
});

// @see https://github.com/mllrsohn/gulp-protractor#running-protractor-without-a-plugin
function getProtractorBinary(bin) {
  return path.resolve(
    path.dirname(require.resolve('protractor')),
    '../bin/',
    (bin || 'protractor') + (/^win/.test(process.platform) ? '.cmd' : '')
  );
}
