import path from 'path';
import { spawn } from 'child_process';

import kss from 'kss';
import sassdoc from 'sassdoc';
import del from 'del';

gulp.task('styles:guide', () => {
  const root = path.resolve(__dirname, '../../');
  const dest = path.resolve(root, 'styleguide');

  return del(dest).then(() => spawn(
    path.resolve(root, 'node_modules/kss/bin/kss-node'),
    [
      '--source', path.resolve(root, 'src/styles/'),
      '--destination', dest,
      // TODO render and ref CSS
    ]
  ));
});

gulp.task('styles:doc', () => {
  const root = path.resolve(__dirname, '../../');
  const dest = path.resolve(root, 'sassdoc');

  return del(dest).then(() => {
    return gulp.src(path.resolve(root, 'src/styles/**/*.scss'))
      .pipe(sassdoc({
        // TODO config
      }))
      .resume();
  });
});
