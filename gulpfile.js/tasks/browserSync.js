import browserSync from 'browser-sync';

gulp.task('browserSync', () => {
  browserSyncInstance = browserSync(config.browserSync);
  gulp.watch(['public/*.html', 'public/*.css'], browserSyncInstance.reload);
});

export function taskSync(name, cb) {
  gulp.task(name, () => {
    cb && cb.call && cb.call();
    browserSyncInstance && browserSyncInstance.reload && browserSyncInstance.reload();
  });
}
