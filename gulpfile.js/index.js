require('babel/register');

global.gulp = require('gulp');
global.gulp.taskSync = require('./tasks/browserSync').taskSync;
global.sequence = require('run-sequence');
global.$ = require('gulp-load-plugins')();

global.config = require('../config');
global.webpackConfig = require('../webpack/build/' + config.ENVIRONMENT + '.js');

global.browserSyncInstance = null;
global.watch = false; // TODO do we really need this?

require('require-dir')('./tasks', { recursive: true });
