from __future__ import unicode_literals

import os
# noinspection PyUnresolvedReferences
from swishfab.customer_frontend import *
from swishfab.builds import make_build as base_make_build


@task(alias='mkb')
@setup
def make_build(build_type):
    """
    Makes a new build on the ``master`` branch.

    Calculates all merges, PRs and JIRA tickets that have been merged since the latest build and manifests them into a
    JSON file that can then be presented in a UI tool in engine-app. Run as::

      $ fab mkb:build

    :param build_type: str The type of build you're creating ('major', 'minor' or 'build').
    """
    project_root = os.path.dirname(__file__)
    execute(base_make_build, project_root, 'builds.json', build_type)

