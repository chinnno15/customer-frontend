import { config } from './../package';
import envConfig from './environments'
import argv from './argv';

let env;
let development = false;

if (argv.production) {
  env = 'production';
} else if (argv.prototype) {
  env = 'prototype';
} else if (argv.staging) {
  env = 'staging';
} else {
  env = 'development';
  development = true;
}

export default {
  argv: argv,

  DEVELOPMENT: development,
  ENVIRONMENT: env,

  sourceMap: development && config.sourceMap,
  devtool: 'cheap-module-eval-source-map',

  appName: config.appName,
  envConfig: envConfig[env],

  server: require('./server.js')(config, argv),
  browserSync: require('./browsersync.js')(config, argv),
}
