import minimist from 'minimist';

const argv = minimist(process.argv.slice(2));

// Special flags, for slow machine guys
//   --lite = --no-check-dependencies --no-test --no-browser-sync
//
// Note that we use [] here instead of . to access object key, to easily
//   change it later if needed
if (argv['lite']) {
  argv['check-dependencies'] = argv['test'] = argv['browser-sync'] = false;
}

export default argv;
