import path from 'path';
import merge from 'webpack-merge';
import commonConfig from './common';


export default merge(commonConfig, {
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: [
          path.resolve(__dirname, '../tests'),
          path.resolve(__dirname, '../src/scripts'),
          path.resolve(__dirname, '../lang'),
        ],
        loader: 'babel-loader',
        query: {
          cacheDirectory: true
        }
      },
      { test: /\.css$/, loader: 'style!css!autoprefixer' }
    ]
  }
});
