import path from 'path';
import webpack from 'webpack';
import merge from 'webpack-merge';

import config from '../../config';
import commonConfig from './common';

import ExtractTextPlugin from 'extract-text-webpack-plugin';


export default merge(commonConfig, {
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: ['ng-annotate?sourcemap=false', 'babel?optional=runtime'],
        include: [
          path.resolve(__dirname, '../../src/scripts'),
          path.resolve(__dirname, '../../lang'),
        ],
      },
      { test: /\.scss$/, loader: ExtractTextPlugin.extract('style', 'css!autoprefixer!sass?outputStyle=expanded') },
      { test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css!autoprefixer') },
    ]
  },
  plugins: [
    new webpack.optimize.DedupePlugin(),

    // FIXME: Has problems with XAuth or smth, take a look later
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        screw_ie8: true,
        warnings: false
      },
      mangle: false,
      minimize: true,
      sourceMap: config.sourceMap
    }),

    new webpack.optimize.AggressiveMergingPlugin(),
    new ExtractTextPlugin('assets/css/styles.[contenthash].css'),
  ]
});
