import path from 'path';
import webpack from 'webpack';
import merge from 'webpack-merge';

import config from '../../config';
import commonConfig from './common';


export default merge(commonConfig, {
  entry: [
    'webpack-dev-server/client?' + config.server.url,
    'webpack/hot/dev-server'
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: [
          path.resolve(__dirname, '../../src/scripts'),
          path.resolve(__dirname, '../../lang'),
        ],
        loader: 'babel'
      },
      { test: /\.scss$/, loader: 'style!css!sass?sourceMap' },
      { test: /\.css$/, loader: 'style!css!autoprefixer' }
    ]
  },
  plugins: [
    new webpack.NoErrorsPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ],
  watch: true,
  devServer: {
    contentBase: 'public/',
    publicPath: commonConfig.output.publicPath,
    hot: true,
    noInfo: true,
    historyApiFallback: true,
    quiet: true,
    lazy: false,
    inline: true,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Cache-Control': 'no-cache, no-store, must-revalidate',
      'Pragma': 'no-cache',
      'Expires': '0',
    },
    stats: {
      colors: true,
      reasons: true
    },
    proxy: {
      '/api/v1/*': config.envConfig.apiProxy,
      '/media/*': config.envConfig.apiProxy,
      '/static/*': config.envConfig.apiProxy,
    }
  }
});
