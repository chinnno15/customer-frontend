import productionConfig from './production';

// At the moment, staging and prototype configs are identical to production
export default productionConfig;
