import path from 'path';
import merge from 'webpack-merge';
import HtmlPlugin from 'html-webpack-plugin';

import commonConfig from '../common';


export default merge(commonConfig, {
  entry: [
    'bootstrap-loader',
    path.resolve(__dirname, '../../src/scripts/loader')
  ],
  output: {
    path: path.resolve(__dirname, '../../public'),
    filename: 'assets/js/[name].[hash].js',
    publicPath: '/'
  },
  plugins: [
    new HtmlPlugin({
      title: config.appName,
      template: path.resolve(__dirname, '../../src/templates/index.html')
    })
  ]
});
