import path from 'path';
import webpack from 'webpack';
import config from '../config';
import git from 'git-rev-sync';
import BowerWebpackPlugin from 'bower-webpack-plugin';

const VERSION = git.long();

const BOWER_DIR = path.resolve(__dirname, '../bower_components');

const { NODE_ENV } = process.env;

const _preLoaders = [
  { test: /[\/]angular\.js$/, loader: 'exports?angular' },
  { test: /[\/]angular-spinner\.js$/, loader: 'imports?define=>false' },
];

const eslintLoader = {
  test: /\.js$/,
  include: [
    path.resolve(__dirname, '../src/scripts'),
    path.resolve(__dirname, '../lang'),
    path.resolve(__dirname, '../tests')
  ],
  loader: 'eslint'
};

// If in develpment then eslint is unnecessary as it is already running in editor.
if (NODE_ENV !== 'development') {
  _preLoaders.push(eslintLoader);
}

export default {
  resolve: {
    root: path.resolve(__dirname, '../src'),
    modulesDirectories: ['node_modules'],
    extensions: ['', '.jade', '.scss', '.js'],
  },
  module: {
    stats: false,
    preLoaders: _preLoaders,
    loaders: [
      { test: /\.jade$/, loaders: ['jade'] },
      { test: /\.json/, loaders: ['json'] },

      { test: /\.(jpe?g|png|gif)$/, loaders: ['url?limit=30000&name=assets/img/[name].[hash].[ext]'] },
      { test: /\.svg([\?]?.*)$/, loaders: ['url?limit=10000&mimetype=image/svg+xml&name=assets/img/[name].[hash].[ext]'] },

      // TODO: distinguish between svg image and font
      { test: /\.woff([\?]?.*)$/, loaders: ['url?limit=10000&mimetype=application/font-woff&&name=assets/fonts/[name].[hash].[ext]'] },
      { test: /\.woff2([\?]?.*)$/, loaders: ['url?limit=10000&mimetype=application/font-woff&name=assets/fonts/[name].[hash].[ext]'] },
      { test: /\.ttf([\?]?.*)$/, loaders: ['url?limit=10000&mimetype=application/octet-stream&name=assets/fonts/[name].[hash].[ext]'] },
      { test: /\.otf([\?]?.*)$/, loaders: ['url?limit=10000&mimetype=application/x-font-ttf&name=assets/fonts/[name].[hash].[ext]'] },
      { test: /\.eot([\?]?.*)$/, loaders: ['url?limit=10000&name=assets/fonts/[name].[hash].[ext]'] },

      // force QR code library to work with webpack
      {
        test: path.resolve(BOWER_DIR, 'qrcode-generator/js/qrcode.js'),
        loader: 'exports?qrcode',
      },
      {
        test: path.resolve(BOWER_DIR, 'angular-qrcode/angular-qrcode.js'),
        loader: 'imports?qrcode=qrcode-generator',
      },
    ],
    noParse: /\.min\.js/
  },
  plugins: [
    new BowerWebpackPlugin({
      modulesDirectories: [ BOWER_DIR ],
      manifestFiles: ['bower.json', '.bower.json'],
      searchResolveModulesDirectories: false,
      includes: /.*/,
      excludes: [
        /\bknockout-sortable\.js$/,
        /\breact-sortable-mixin\.js$/,
        /\bangular-flash\.css$/,
        /\bqrcode_UTF8.js$/,
      ],
    }),
    new webpack.optimize.OccurenceOrderPlugin(true),
    new webpack.DefinePlugin({
      __process__: {
        DEVELOPMENT: config.DEVELOPMENT, // boolean, see `config/index.js`
        ENVIRONMENT: '"' + config.ENVIRONMENT + '"',
      },
      __VERSION__: JSON.stringify(VERSION),
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
    }),
  ],
  watch: false,
  target: 'web',
  debug: config.DEVELOPMENT,
  cache: config.DEVELOPMENT,
  devtool: config.DEVELOPMENT ? config.devtool : false,
  stats: {
    children: false,
    colors: true,
    reasons: config.DEVELOPMENT
  },
  eslint: {
    configFile: path.resolve(__dirname, '../.eslintrc'),
    formatter: require('eslint-friendly-formatter'),
    emitWarning: true,
    failOnError: false,
    failOnWarning: false
  }
};
