import webpackConfig from '../webpack/test.js';

// these vars to be used as follows:
//
// REPORTER=progress gulp test --nonstop
//
// or if you want to run chrome instead of PhantomJS:
//
// BROWSER=Chrome gulp test --nonstop
const { NODE_ENV, REPORTER = 'spec', BROWSER = 'PhantomJS'} = process.env;

export default karmaConfig => {
  karmaConfig.set({
    // Add any browsers here
    browsers: [BROWSER],
    frameworks: ['jasmine'],

    // The entry point for our test suite
    basePath: '.',
    files: ['units/run_all.js'],
    preprocessors: {
      // Run this through webpack, and enable inline sourcemaps
      'units/run_all.js': ['webpack', 'sourcemap']
    },

    webpack: webpackConfig,
    client: {
      // log console output in our test console
      captureConsole: true
    },

    reporters: [REPORTER],
    autoWatch: true,
    singleRun: !config.argv.nonstop, // exit after tests have completed

    webpackMiddleware: {
      noInfo: true,
      stats: NODE_ENV === 'development' ? 'errors-only': 'verbose'
    },

    // Webpack takes a little while to compile -- this manifests as a really
    // long load time while webpack blocks on serving the request.
    browserNoActivityTimeout: 60000 // 60 seconds
  });
};
