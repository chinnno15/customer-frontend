import 'phantomjs-polyfill';
import '../../src/scripts/app';
import 'angular-mocks';

// TESTS TO RUN

// filters
import './scripts/filters/price';

// directives
import './scripts/directives/lengthLimit';

// controllers
import './scripts/controllers/CartController';
import './scripts/controllers/CallWaiterController';
import './scripts/controllers/MenuController';
import './scripts/controllers/CompanyController';
import './scripts/controllers/ProductController';
import './scripts/controllers/ReceiptController';
import './scripts/controllers/ToggleController';
import './scripts/controllers/TranslateController';
import './scripts/controllers/ImageController';
import './scripts/controllers/OrderPointController';
import './scripts/controllers/OrderController';
import './scripts/controllers/MainController.js';

// daos
import './scripts/dao/CategoryDAO';
import './scripts/dao/CompanyDAO';
import './scripts/dao/OrderDAO';
import './scripts/dao/OrderPointDAO';
import './scripts/dao/PaymentDAO';
import './scripts/dao/ProductDAO';
import './scripts/dao/CallWaiterDAO';

// services
import './scripts/services/Cart';
import './scripts/services/CartIntegrationTest';
import './scripts/services/CartItemsStore';
import './scripts/services/Checkout';
import './scripts/services/GoogleApiLoader';
import './scripts/services/I18n';
import './scripts/services/OrderPointStore';
import './scripts/services/OrdersStore';
import './scripts/services/CallWaiter';
import './scripts/services/StripePayment';
import './scripts/services/SwishApiInterceptor';
import './scripts/services/SwishAppInit';
import './scripts/services/UrlRequestSender';
import './scripts/services/GoogleTranslator';
import './scripts/services/MultiTranslator';
import './scripts/services/Translator';
import './scripts/services/Lang';
import './scripts/services/Company';
import './scripts/services/Currency';
import './scripts/services/TopSheet';
import './scripts/services/Modal';
import './scripts/services/AppReloader';

import './scripts/app';
import './scripts/states';
