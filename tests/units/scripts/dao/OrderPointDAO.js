/**
* OrderPointDAO tests
*/
describe('customerApp.dao.OrderPointDAO', () => {
  let OrderPointDAO, $httpBackend;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', 'customerApp.dao'));

  // inject services
  beforeEach(inject((_OrderPointDAO_, _$httpBackend_) => {
    $httpBackend = _$httpBackend_;
    // initialize OrderPointDAO
    OrderPointDAO = _OrderPointDAO_;
  }));


  it('should be defined', () => {
    expect(OrderPointDAO).toBeDefined();
  });

  it('should fetch products when register() is called', (done) => {
    $httpBackend.expectPOST(
      'orderpoint/register',
      {
        store_name: 'store-name',
        order_point_name: 'order-point-name',
      }
    ).respond('OK');

    OrderPointDAO.register('store-name', 'order-point-name')
    .then((res) => {
      expect(res).toEqual('OK');
      done();
    });

    $httpBackend.flush();
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });
});
