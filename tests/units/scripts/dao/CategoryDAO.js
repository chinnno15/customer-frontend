/**
* CategoryDAO tests
*/
describe('customerApp.dao.CategoryDAO', () => {
  let CategoryDAO, $httpBackend, Translator, DAOS;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', 'customerApp.dao', ($provide) => {
    // mock Translator service
    Translator = {
      translate: jasmine.createSpy('translate').and.callFake((arr => arr)),
    };
    $provide.value('Translator', Translator);
  }));

  // inject services
  beforeEach(inject((_CategoryDAO_, _$httpBackend_, _DAOS_) => {
    $httpBackend = _$httpBackend_;
    // initialize CategoryDAO
    CategoryDAO = _CategoryDAO_;

    DAOS = _DAOS_;
  }));

  it('should be defined', () => {
    expect(CategoryDAO).toBeDefined();
  });

  describe('listCategories()', () => {
    it('should fetch top level categories when parent is not defined', (done) => {
      $httpBackend
      .expectGET(`category?page_size=${DAOS.NO_PAGINATION}&parent=${DAOS.EMPTY_FILTER}`)
      .respond({
        results: [
          { id: 1, name: 'Deserts' },
          { id: 2, name: 'Mains' },
        ]
      });

      CategoryDAO.listCategories()
      .then((categories) => {
        expect(Translator.translate).toHaveBeenCalledWith(
          [
            { id: 1, name: 'Deserts' },
            { id: 2, name: 'Mains' },
          ],
          ['name', 'description'],
        );
        expect(categories).toEqual([
          { id: 1, name: 'Deserts' },
          { id: 2, name: 'Mains' },
        ]);
        done();
      });

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should fetch subcategories when parent is defined', (done) => {
      $httpBackend.expectGET(`category?page_size=${DAOS.NO_PAGINATION}&parent=3`).respond({
        results: [{ id: 1, name: 'Deserts' }]
      });

      CategoryDAO.listCategories(3)
      .then((categories) => {
        expect(Translator.translate).toHaveBeenCalledWith(
          [{ id: 1, name: 'Deserts' }],
          ['name', 'description'],
        );
        expect(categories).toEqual([{ id: 1, name: 'Deserts' }]);
        done();
      });

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });
});
