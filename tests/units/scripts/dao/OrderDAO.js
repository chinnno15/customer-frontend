/**
* OrderDAO tests
*/
describe('customerApp.dao.OrderDAO', () => {
  let OrderDAO, $httpBackend;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', 'customerApp.dao'));

  // inject services
  beforeEach(inject((_OrderDAO_, _$httpBackend_) => {
    $httpBackend = _$httpBackend_;
    // initialize OrderDAO
    OrderDAO = _OrderDAO_;
  }));


  it('should be defined', () => {
    expect(OrderDAO).toBeDefined();
  });

  describe('getPendingOrdersCount()', () => {
    it('should count orders that are not served yet', (done) => {
      $httpBackend.expectGET('order?state__in=3,2,4').respond({ count: 6 });

      OrderDAO.getPendingOrdersCount()
      .then((numberOfOrders) => {
        expect(numberOfOrders).toEqual(6);
        done();
      });

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });

  describe('getOrder()', () => {
    it('should respond with an order', (done) => {
      $httpBackend.expectGET('order/123').respond({ id: 123 });

      OrderDAO.getOrder(123)
      .then((order) => {
        expect(order).toEqual({ id: 123 });
        done();
      });

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });

  describe('getReceiptUrl()', () => {
    it('should respond with a PDF receipt URL', (done) => {
      $httpBackend.expectPOST('order/123/receipt').respond({ url: 'http://test.te/bla.pdf' });

      OrderDAO.getReceiptUrl(123)
      .then(url => {
        expect(url).toEqual('http://test.te/bla.pdf');
        done();
      });

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });

  describe('sendReceiptByEmail()', () => {
    it('should correctly send a request', () => {
      $httpBackend.expectPOST('order/123/receipt-email', { email: 'john@test.te' }).respond('OK');

      OrderDAO.sendReceiptByEmail(123, 'john@test.te');

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });
});
