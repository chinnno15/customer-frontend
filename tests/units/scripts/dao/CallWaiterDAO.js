/**
* CallWaiterDAO tests
*/
describe('customerApp.dao.CallWaiterDAO', () => {
  let CallWaiterDAO, $httpBackend;

  // load the module
  beforeEach(angular.mock.module('customerApp.dao'));

  // inject services
  beforeEach(inject((_CallWaiterDAO_, _$httpBackend_) => {
    $httpBackend = _$httpBackend_;
    // initialize CallWaiterDAO
    CallWaiterDAO = _CallWaiterDAO_;
  }));


  it('should be defined', () => {
    expect(CallWaiterDAO).toBeDefined();
  });

  describe('getCalls()', () => {
    it('should get state from id', (done) => {
      const callIds = [14, 15];

      $httpBackend.expectGET('callwaiter?id=14,15').respond({
        results: [
          {
            'id': 14,
            'state': 2,
          },
          {
            'id': 15,
            'state': 2,
          }
        ]
      });

      CallWaiterDAO.getCalls(callIds)
        .then((res) => {
          expect(res).toEqual([{
            'id': 14,
            'state': 2,
          },
          {
            'id': 15,
            'state': 2,
          }]);
          done();
        });

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });

  describe('createCall()', () => {
    it('should return new id', (done) => {
      $httpBackend.expectPOST('callwaiter').respond({
        'id': 14,
      });

      CallWaiterDAO.createCall()
        .then((res) => {
          expect(res).toEqual({
            'id': 14,
          });
          done();
        });

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
    it('should return new id, message and order_id', (done) => {
      const id = 14;
      const orderId = 1;
      const message = 'message';
      $httpBackend.expectPOST('callwaiter').respond({
        id,
        order_id: orderId,
        message
      });
      CallWaiterDAO.createCall(orderId, message)
        .then((res) => {
          expect(res).toEqual({
            id,
            order_id: orderId,
            message
          });
          done();
        });

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });
});
