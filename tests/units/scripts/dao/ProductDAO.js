/**
* ProductDAO tests
*/
describe('customerApp.dao.ProductDAO', () => {
  let ProductDAO, Translator, DAOS, $httpBackend;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', 'customerApp.dao', ($provide) => {
    // mock Translator service
    Translator = {
      translate: jasmine.createSpy('translate').and.callFake((arr => arr)),
    };
    $provide.value('Translator', Translator);
  }));

  // inject services
  beforeEach(inject((_ProductDAO_, _$httpBackend_, _DAOS_) => {
    $httpBackend = _$httpBackend_;
    // initialize ProductDAO
    ProductDAO = _ProductDAO_;
    DAOS = _DAOS_;
  }));


  it('should be defined', () => {
    expect(ProductDAO).toBeDefined();
  });

  describe('listProducts()', () => {
    it('should fetch products when categoryId is specified', (done) => {
      $httpBackend.expectGET(`product?category=123&page_size=${DAOS.NO_PAGINATION}`).respond({
        results: [
          { id: 1, name: 'Soup' },
          { id: 2, name: 'Chicken' }
        ]
      });

      ProductDAO.listProducts(123)
      .then((products) => {
        expect(Translator.translate).toHaveBeenCalledWith(
          [
            { id: 1, name: 'Soup' },
            { id: 2, name: 'Chicken' }
          ],
          ['name', 'description'],
        );
        expect(products).toEqual([
          { id: 1, name: 'Soup' },
          { id: 2, name: 'Chicken' }
        ]);
        done();
      });

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should fetch products without category when categoryId is not specified', (done) => {
      $httpBackend
      .expectGET(`product?category=${DAOS.EMPTY_FILTER}&page_size=${DAOS.NO_PAGINATION}`)
      .respond({
        results: [{ id: 1, name: 'Soup' }]
      });

      ProductDAO.listProducts()
      .then((products) => {
        expect(Translator.translate).toHaveBeenCalledWith(
          [{ id: 1, name: 'Soup' }],
          ['name', 'description'],
        );
        expect(products).toEqual([{ id: 1, name: 'Soup' }]);
        done();
      });

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });
});
