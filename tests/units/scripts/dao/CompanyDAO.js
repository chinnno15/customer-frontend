/**
* CompanyDAO tests
*/
describe('customerApp.dao.CompanyDAO', () => {
  let CompanyDAO, $httpBackend;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', 'customerApp.dao'));

  // inject services
  beforeEach(inject((_CompanyDAO_, _$httpBackend_) => {
    $httpBackend = _$httpBackend_;
    // initialize CompanyDAO
    CompanyDAO = _CompanyDAO_;
  }));


  it('should be defined', () => {
    expect(CompanyDAO).toBeDefined();
  });

  it('should fetch company when getCompany() is called', (done) => {
    $httpBackend.expectGET('company').respond({ id: 1, name: 'Test restaurant' });

    CompanyDAO.getCompany()
    .then((company) => {
      expect(company).toEqual({ id: 1, name: 'Test restaurant' });
      done();
    });

    $httpBackend.flush();
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });
});
