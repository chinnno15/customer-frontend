/**
* PaymentDAO tests
*/
describe('customerApp.dao.PaymentDAO', () => {
  let PaymentDAO, $httpBackend;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', 'customerApp.dao'));

  // inject services
  beforeEach(inject((_PaymentDAO_, _$httpBackend_) => {
    $httpBackend = _$httpBackend_;
    // initialize PaymentDAO
    PaymentDAO = _PaymentDAO_;
  }));


  it('should be defined', () => {
    expect(PaymentDAO).toBeDefined();
  });

  describe('checkoutWithGear()', () => {
    it('should make a payment using Gear', (done) => {
      $httpBackend
      .expectPOST('payment/gear')
      .respond({ payment_url: 'http://gear.payment.test' });

      PaymentDAO.checkoutWithGear()
      .then((gear) => {
        expect(gear).toEqual({ payment_url: 'http://gear.payment.test' });
        done();
      });

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });

  describe('checkoutWithBilderlingspay()', () => {
    it('should make a payment using Bilderlingspay', (done) => {
      $httpBackend
      .expectPOST('payment/bilderlingspay')
      .respond({ payment_url: 'http://bilderlingspay.payment.test' });

      PaymentDAO.checkoutWithBilderlingspay()
      .then((result) => {
        expect(result).toEqual({ payment_url: 'http://bilderlingspay.payment.test' });
        done();
      });

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });

  describe('checkoutWithStripe()', () => {
    it('should make a payment using Stripe', (done) => {
      $httpBackend
      .expectPOST('payment/stripe', { token: 'stripe_token' })
      .respond({ id: 5 });

      PaymentDAO.checkoutWithStripe('stripe_token')
      .then((order) => {
        expect(order).toEqual({ id: 5 });
        done();
      });

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });
});
