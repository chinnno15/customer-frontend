/**
* lengthLimit tests
*/
describe('customerApp.directives.lengthLimit', () => {
  let $compile;
  let $scope;
  let getCompiledEl;
  let compEl; // eslint-disable-line
  const maxLength = 5;
  // load the module
  beforeEach(angular.mock.module('customerApp.directives', () => {
  }));

  // inject services
  beforeEach(inject((_$compile_, _$rootScope_) => {
    $compile = _$compile_;
    $scope = _$rootScope_.$new();
    getCompiledEl = function(html) {
      // compile element with directive
      const el = angular.element(html);
      const _compEl = $compile(el)($scope);
      $scope.$digest();
      return _compEl;
    };
    compEl = getCompiledEl(`<textarea ng-model="text" ng-data="{{text}}" length-limit="${maxLength}"></textarea>`);
  }));

  it('should fail if ngModel is not specified', () => {
    expect(function() {
      getCompiledEl('<textarea length-limit="5"></textarea>');
    })
    .toThrow();
  });
  it('should have updated text', () => {
    const _text = 'sixty';
    compEl.val(_text).triggerHandler('change');
    $scope.$digest();
    expect(compEl.val()).toEqual(_text);
  });
  it('should have updated text not exceeding maxLength', () => {
    const _text = 'sixty five';
    const parsedText = _text.substring(0, maxLength);
    compEl.val(_text).triggerHandler('change');
    $scope.$digest();
    expect(compEl.val()).toEqual(parsedText);
  });
});
