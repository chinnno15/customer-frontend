/**
* Cart tests
*/
describe('customerApp.services.Cart', () => {
  let Cart, $q, $rootScope, CartItemsStore, EVENTS;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    // mock CartItemsStore
    CartItemsStore = {
      _isEmpty: true,
      createItem: jasmine.createSpy('createItem').and.callFake(() => $q.resolve()),
      updateQuantity: jasmine.createSpy('updateQuantity').and.callFake(() => $q.resolve()),
      deleteItem: jasmine.createSpy('deleteItem').and.callFake(() => $q.resolve()),
      deleteAllItems: jasmine.createSpy('deleteItem').and.callFake(() => $q.resolve()),
      clean: jasmine.createSpy('clean'),
      getItems: jasmine.createSpy('getItems').and.callFake(() => [
        {
          product: {
            id: 1,
            price: 11,
            name: 'name 1',
            description: 'description 1',
            image: 'image 1',
            is_option_enabled: false,
          },
          quantity: 2
        },
        {
          product: {
            id: 2,
            price: 8,
            name: 'name 2',
            description: 'description 2',
            image: 'image 2',
            is_option_enabled: true,
          },
          quantity: 1,
          optionInObject: {
            preferences: [
              { id: 1, name: 'pref group 1', price: 1 },
              { id: 2, name: 'pref group 2', price: 2 },
            ],
            extras: [
              { id: 1, name: 'extra 1', price: 1, quantity: 10 },
              { id: 2, name: 'extra 2', price: 2, quantity: 2 },
              { id: 3, name: 'extra 3', price: 0, quantity: 1 },
            ]
          }
        }
      ]),
      _hasChanged: true,
      initialize: jasmine.createSpy('initialize').and.callFake(() => {
        // this one is called before $q is set, so I used generic mock instead
        return {
          then: (cb) => cb(CartItemsStore._hasChanged)
        };
      }),
      isEmpty: jasmine.createSpy('isEmpty').and.callFake(() => CartItemsStore._isEmpty),
      setItemOptions: jasmine.createSpy('setItemOptions').and.callFake(() => $q.resolve()),
    };

    $provide.value('CartItemsStore', CartItemsStore);
  }));

  // inject services
  beforeEach(inject((_$q_, _$rootScope_, _Cart_, _EVENTS_) => {
    $q = _$q_;
    EVENTS = _EVENTS_;
    // mock $rootScope
    $rootScope = _$rootScope_;
    $rootScope.$emit = jasmine.createSpy('$emit').and.callThrough();
    // initialize Cart
    Cart = _Cart_;
  }));


  it('should be defined', () => {
    expect(Cart).toBeDefined();
    expect($rootScope.$emit.calls.count()).toEqual(0);
    expect(CartItemsStore.initialize).not.toHaveBeenCalled();
  });

  describe('initialize()', () => {
    it('should correctly initialize the cart', () => {
      expect($rootScope.$emit.calls.count()).toEqual(0);
      Cart.initialize();
      expect(CartItemsStore.isEmpty).toHaveBeenCalled();
      expect(CartItemsStore.initialize).toHaveBeenCalled();
      expect($rootScope.$emit.calls.count()).toEqual(1);
    });

    it('should not initialize cart if is not empty', () => {
      CartItemsStore._isEmpty = false;
      Cart.initialize();
      expect(CartItemsStore.isEmpty).toHaveBeenCalled();
      expect(CartItemsStore.initialize).not.toHaveBeenCalled();
      expect($rootScope.$emit.calls.count()).toEqual(0);
    });

    it('should not send event when invoked with false', () => {
      expect($rootScope.$emit.calls.count()).toEqual(0);
      Cart.initialize(false);
      expect($rootScope.$emit.calls.count()).toEqual(0);
    });
  });

  describe('waitForInitializedCart()', () => {
    it('should resolve when cart gets initialized', (done) => {
      Cart.waitForInitializedCart()
      .then(() => {
        done();
      });

      Cart.initialize();
      $rootScope.$apply();
    });
  });

  describe('getQuantity()', () => {
    it('should work correctly when cart is empty', () => {
      CartItemsStore.getItem = jasmine.createSpy('getItem').and.callFake(() => undefined);
      expect(Cart.getQuantity(5)).toEqual(0);
      expect(CartItemsStore.getItem).toHaveBeenCalledWith(5);
    });

    it('should correctly return quantity', () => {
      CartItemsStore.getItem = jasmine.createSpy('getItem').and.callFake(() => ({ quantity: 3 }));
      expect(Cart.getQuantity(5)).toEqual(3);
      expect(CartItemsStore.getItem).toHaveBeenCalledWith(5);
    });
  });

  describe('setQuantity()', () => {
    it('should add cart item when quantity > 0', () => {
      CartItemsStore.getItem = jasmine.createSpy('getItem').and.callFake(() => undefined);
      Cart.setQuantity({ id: 3, price: 8 }, 1);
      expect(CartItemsStore.createItem).toHaveBeenCalledWith({ id: 3, price: 8 }, 1, undefined);
      expect(CartItemsStore.deleteItem).not.toHaveBeenCalled();
    });

    it('should remove cart item when quantity > 0', () => {
      Cart.setQuantity({ id: 3, price: 8 }, 0);
      expect(CartItemsStore.createItem).not.toHaveBeenCalled();
      expect(CartItemsStore.updateQuantity).not.toHaveBeenCalled();
      expect(CartItemsStore.deleteItem).toHaveBeenCalledWith(3);
    });

    it('should send change notification on each call', () => {
      CartItemsStore.getItem = jasmine.createSpy('getItem').and.callFake(() => undefined);
      expect($rootScope.$emit.calls.count()).toEqual(0);
      Cart.setQuantity({ id: 1, price: 8 }, 1);
      expect($rootScope.$emit.calls.count()).toEqual(1);

      Cart.setQuantity({ id: 1, price: 8 }, 2);
      expect($rootScope.$emit.calls.count()).toEqual(2);

      Cart.setQuantity({ id: 2, price: 8 }, 1);
      expect($rootScope.$emit.calls.count()).toEqual(3);

      Cart.setQuantity({ id: 2, price: 8 }, 0);
      expect($rootScope.$emit.calls.count()).toEqual(4);
    });
  });

  describe('getTotals()', () => {
    it('should work correctly when cart is empty', () => {
      expect(Cart.getTotals()).toEqual({ price: 0, quantity: 0 });
      expect(CartItemsStore.getItems).not.toHaveBeenCalled();
    });

    it('should correctly compute totals and cache result', () => {
      expect(CartItemsStore.getItems.calls.count()).toEqual(0);
      expect(Cart.getTotals()).toEqual({ price: 0, quantity: 0 });

      Cart.setQuantity({ id: 0 }, 0);
      expect(Cart.getTotals()).toEqual({ price: 39, quantity: 3 });
      expect(CartItemsStore.getItems.calls.count()).toEqual(1);
    });
  });

  describe('getProducts()', () => {
    it('should work correctly when cart is empty', () => {
      // We already have a cache in CartItemStore. Why do we need to cache it again?
      // Answer: because of CartItemsStore.getItems which converts object values to array
      expect(Cart.getProducts()).toEqual([]);
      expect(CartItemsStore.getItems).not.toHaveBeenCalled();
    });

    it('should correctly compute totals and cache result', () => {
      expect(CartItemsStore.getItems.calls.count()).toEqual(0);
      expect(Cart.getTotals()).toEqual({ price: 0, quantity: 0 });

      Cart.setQuantity({ id: 0 }, 0);

      expect(Cart.getProducts()).toEqual([
        {
          product: {
            id: 1,
            price: 11,
            name: 'name 1',
            description: 'description 1',
            image: 'image 1',
            is_option_enabled: false,
          },
          quantity: 2
        },
        {
          product: {
            id: 2,
            price: 8,
            name: 'name 2',
            description: 'description 2',
            image: 'image 2',
            is_option_enabled: true,
          },
          quantity: 1,
          optionInObject: {
            preferences: [
              { id: 1, name: 'pref group 1', price: 1 },
              { id: 2, name: 'pref group 2', price: 2 },
            ],
            extras: [
              { id: 1, name: 'extra 1', price: 1, quantity: 10 },
              { id: 2, name: 'extra 2', price: 2, quantity: 2 },
              { id: 3, name: 'extra 3', price: 0, quantity: 1 },
            ]
          }
        }
      ]);
      expect(CartItemsStore.getItems.calls.count()).toEqual(1);

      expect(Cart.getProducts()).toEqual([
        {
          product: {
            id: 1,
            price: 11,
            name: 'name 1',
            description: 'description 1',
            image: 'image 1',
            is_option_enabled: false,
          },
          quantity: 2
        },
        {
          product: {
            id: 2,
            price: 8,
            name: 'name 2',
            description: 'description 2',
            image: 'image 2',
            is_option_enabled: true,
          },
          quantity: 1,
          optionInObject: {
            preferences: [
              { id: 1, name: 'pref group 1', price: 1 },
              { id: 2, name: 'pref group 2', price: 2 },
            ],
            extras: [
              { id: 1, name: 'extra 1', price: 1, quantity: 10 },
              { id: 2, name: 'extra 2', price: 2, quantity: 2 },
              { id: 3, name: 'extra 3', price: 0, quantity: 1 },
            ]
          }
        }
      ]);
      expect(CartItemsStore.getItems.calls.count()).toEqual(1);
    });
  });

  describe('clean()', () => {
    it('should correctly clear the cart', () => {
      expect($rootScope.$emit.calls.count()).toEqual(0);
      expect(CartItemsStore.clean).not.toHaveBeenCalled();
      Cart.clean();
      expect(CartItemsStore.clean).toHaveBeenCalled();
      expect($rootScope.$emit).toHaveBeenCalledWith(EVENTS.CART_UPDATED);
      expect($rootScope.$emit.calls.count()).toEqual(1);
    });

    it('should correctly clear the cart after state change', () => {
      expect($rootScope.$emit.calls.count()).toEqual(0);
      expect(CartItemsStore.clean).not.toHaveBeenCalled();

      Cart.clean(true);
      expect($rootScope.$emit.calls.count()).toEqual(0);
      expect(CartItemsStore.clean).toHaveBeenCalled();

      $rootScope.$broadcast('$stateChangeSuccess');
      $rootScope.$apply();
      expect($rootScope.$emit).toHaveBeenCalledWith(EVENTS.CART_UPDATED);
      expect($rootScope.$emit.calls.count()).toEqual(1);
    });
  });

  describe('deleteAllItems()', () => {
    it('should correctly delete all items from the cart', () => {
      expect($rootScope.$emit.calls.count()).toEqual(0);
      expect(CartItemsStore.deleteAllItems).not.toHaveBeenCalled();
      Cart.deleteAllItems();
      expect(CartItemsStore.deleteAllItems).toHaveBeenCalled();
      expect($rootScope.$emit).toHaveBeenCalledWith(EVENTS.CART_UPDATED);
      expect($rootScope.$emit.calls.count()).toEqual(1);
    });
  });

  describe('reload()', () => {
    it('should clean and reinitialize the cart', () => {
      expect($rootScope.$emit.calls.count()).toEqual(0);
      Cart.reload();
      expect(CartItemsStore.clean).toHaveBeenCalled();
      expect(CartItemsStore.isEmpty).toHaveBeenCalled();
      expect(CartItemsStore.initialize).toHaveBeenCalled();
      expect($rootScope.$emit).toHaveBeenCalledWith(EVENTS.CART_UPDATED);
      expect($rootScope.$emit.calls.count()).toEqual(1);
    });
  });

  describe('getItemPrice()', () => {
    it('should using default price when is_option_enabled = false', () => {
      const cartItem = {
        product: {
          price: 11,
          is_option_enabled: false,
        }
      };

      expect(Cart.getItemPrice(cartItem)).toEqual(11);
    });

    it('should calculate price using preferences and extras when is_option_enabled = true', () => {
      const cartItem = {
        product: {
          price: 11,
          is_option_enabled: true,
        },
        optionInObject: {
          preferences: [
            { id: 1, name: 'pref group 1', price: 1 },
            { id: 2, name: 'pref group 2', price: 2 },
          ],
          extras: [
            { id: 1, name: 'extra 1', price: 1, quantity: 10 },
            { id: 2, name: 'extra 2', price: 2, quantity: 2 },
            { id: 3, name: 'extra 3', price: 0, quantity: 1 },
            { id: 3, name: 'extra 4', price: 10},
          ]
        }
      };

      expect(Cart.getItemPrice(cartItem)).toEqual(17);
    });
  });

  describe('getOption()', () => {
    it('getOption should work correctly when cart is empty', () => {
      CartItemsStore.getItem = jasmine.createSpy('getItem').and.callFake(() => undefined);
      expect(Cart.getOption(5)).toEqual({});
      expect(CartItemsStore.getItem).toHaveBeenCalledWith(5);
    });

    it('should correctly return option', () => {
      CartItemsStore.getItem = jasmine.createSpy('getItem').and.callFake(() => ({
        optionInObject: {
          preferences: [],
          extras: []
        }
      }));
      expect(Cart.getOption(5)).toEqual({
        preferences: [],
        extras: []
      });
      expect(CartItemsStore.getItem).toHaveBeenCalledWith(5);
    });
  });

  describe('setOption()', () => {
    it('should do nothing when product is_option_enabled = false', () => {
      Cart.setOption({ id: 3, price: 8, is_option_enabled: false }, {});

      expect(CartItemsStore.setItemOptions).not.toHaveBeenCalled();
    });

    it('should work when is_option_enabled = true', () => {
      Cart.setOption({ id: 3, price: 8, is_option_enabled: true }, {
        preferences: [
          { name: 1, price: 1 },
          { name: 2, price: 2 },
        ],
        extras: [
          { name: 1, price: 1, quantity: 1 },
          { name: 2, price: 2, quantity: 2 },
        ]
      });

      expect(CartItemsStore.setItemOptions).toHaveBeenCalledWith(3, {
        preferences: [
          { name: 1, price: 1 },
          { name: 2, price: 2 },
        ],
        extras: [
          { name: 1, price: 1, quantity: 1 },
          { name: 2, price: 2, quantity: 2 },
        ]
      });
    });
  });
});
