/**
* OrdersStore tests
*/
describe('customerApp.services.OrdersStore', () => {
  let OrderDAO, Alert, localStorageService, $q, $rootScope, $interval, EVENTS, OrderPointStore;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    // mock OrderDAO
    $provide.factory('OrderDAO', (_$q_) => ({
      _getPendingOrdersCountDeferred: _$q_.defer(),
      getPendingOrdersCount: jasmine.createSpy('getPendingOrdersCount').and.callFake(() => {
        return OrderDAO._getPendingOrdersCountDeferred.promise;
      }),
      getOrder: jasmine.createSpy('getOrder').and.callFake(() => $q.resolve({ id: 123, state: 3 })),
    }));

    // mock Alert
    Alert = {
      error: jasmine.createSpy('error')
    };
    $provide.value('Alert', Alert);

    OrderPointStore = {
      isOrderPointRegistered: () => true
    };
    $provide.value('OrderPointStore', OrderPointStore);

    // mock localStorageService
    localStorageService = {
      set: jasmine.createSpy('set'),
      get: jasmine.createSpy('get').and.callFake(() => localStorageService.__latestOrderId),
    };
    $provide.value('localStorageService', localStorageService);
  }));

  // inject services
  beforeEach(inject((_$q_, _$rootScope_, _$interval_, _OrderDAO_, _EVENTS_) => {
    $q = _$q_;
    $rootScope = _$rootScope_;
    $rootScope.$emit = jasmine.createSpy('$emit');
    $interval = _$interval_;
    OrderDAO = _OrderDAO_;
    EVENTS = _EVENTS_;
  }));


  it('should be defined and initialize correctly', () => {
    let OrdersStore;
    OrderDAO._getPendingOrdersCountDeferred.resolve(5);
    inject((_OrdersStore_) => { OrdersStore = _OrdersStore_; });
    $rootScope.$apply();

    expect(OrdersStore).toBeDefined();
    expect(Alert.error).not.toHaveBeenCalled();
    expect(OrderDAO.getPendingOrdersCount).toHaveBeenCalled();
    expect(OrdersStore.getNumberOfPendingOrders()).toEqual(5);
    expect($rootScope.$emit).toHaveBeenCalledWith(EVENTS.NUMBER_OF_ORDERS_UPDATED);
    expect($rootScope.$emit.calls.count()).toEqual(1);
  });

  it('should be display error when initialization fails', () => {
    let OrdersStore;
    OrderDAO._getPendingOrdersCountDeferred.reject();
    inject((_OrdersStore_) => { OrdersStore = _OrdersStore_; });
    $rootScope.$apply();

    expect(OrderDAO.getPendingOrdersCount).toHaveBeenCalled();
    expect(OrdersStore.getNumberOfPendingOrders()).toEqual(0);
    expect($rootScope.$emit).not.toHaveBeenCalled();
    expect(Alert.error).toHaveBeenCalledWith('ORDERS.PENDING_COUNT_FAILED');
  });

  it('should refresh number of pending orders after 10 sek', () => {
    let OrdersStore;
    OrderDAO._getPendingOrdersCountDeferred.resolve(1);
    inject((_OrdersStore_) => { OrdersStore = _OrdersStore_; });
    $rootScope.$apply();

    expect(OrdersStore.getNumberOfPendingOrders()).toEqual(1);

    // init new promise and move forward 10 sek
    OrderDAO._getPendingOrdersCountDeferred = $q.defer();
    $interval.flush(10 * 1000);
    OrderDAO._getPendingOrdersCountDeferred.resolve(3);
    $rootScope.$apply();

    expect(Alert.error).not.toHaveBeenCalled();
    expect(OrderDAO.getPendingOrdersCount.calls.count()).toEqual(2);
    expect(OrdersStore.getNumberOfPendingOrders()).toEqual(3);
    expect($rootScope.$emit.calls.count()).toEqual(2);
  });

  describe('setLatestOrder()', () => {
    it('should correctly set latest order', () => {
      let OrdersStore;
      OrderDAO._getPendingOrdersCountDeferred.resolve(1);
      inject((_OrdersStore_) => { OrdersStore = _OrdersStore_; });
      $rootScope.$apply();

      expect(OrdersStore.getNumberOfPendingOrders()).toEqual(1);
      expect($rootScope.$emit.calls.count()).toEqual(1);

      OrdersStore.setLatestOrder({ id: 12 });
      expect(OrdersStore.latestOrder).toEqual({ id: 12 });
      expect(OrdersStore.getNumberOfPendingOrders()).toEqual(2);
      expect($rootScope.$emit.calls.count()).toEqual(2);
      expect(localStorageService.set).toHaveBeenCalledWith('latestOrderId', 12);
    });
  });

  describe('getLatestOrder()', () => {
    it('should get latest order if it was set', (done) => {
      let OrdersStore;
      OrderDAO._getPendingOrdersCountDeferred.resolve(1);
      inject((_OrdersStore_) => { OrdersStore = _OrdersStore_; });
      $rootScope.$apply();

      OrdersStore.setLatestOrder({ id: 123 });
      OrdersStore.getLatestOrder()
      .then((order) => {
        expect(order).toEqual({ id: 123 });
        done();
      });

      $rootScope.$apply();
    });

    it('should return null if order was not set and its id is not in localeStorage', (done) => {
      let OrdersStore;
      OrderDAO._getPendingOrdersCountDeferred.resolve(1);
      inject((_OrdersStore_) => { OrdersStore = _OrdersStore_; });
      $rootScope.$apply();

      OrdersStore.getLatestOrder()
      .then((order) => {
        expect(order).toEqual(null);
        done();
      });

      $rootScope.$apply();
    });

    it('should fetch order from backend if its id is in localeStorage', (done) => {
      let OrdersStore;
      OrderDAO._getPendingOrdersCountDeferred.resolve(1);
      inject((_OrdersStore_) => { OrdersStore = _OrdersStore_; });
      $rootScope.$apply();

      localStorageService.__latestOrderId = 123;

      OrdersStore.getLatestOrder()
      .then((order) => {
        expect(order).toEqual({ id: 123, state: 3 });
        expect(localStorageService.get).toHaveBeenCalledWith('latestOrderId');
        expect(OrderDAO.getOrder).toHaveBeenCalledWith(123);
        done();
      });

      $rootScope.$apply();
    });
  });
});
