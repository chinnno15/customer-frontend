/**
* Currency tests
*/
describe('customerApp.service.Currency', () => {
  let Currency, $q, $rootScope, Alert, Company, CurrencyDAO, EVENTS;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    // mock Company
    $provide.factory('Company', () => {
      Company = {
        _company: { default_currency: 6 },
        getCompany: jasmine.createSpy('getCompany').and.callFake(() => Company._company),
      };
      return Company;
    });

    // mock CurrencyDAO
    $provide.factory('CurrencyDAO', (_$q_) => {
      CurrencyDAO = {
        _getCurrencyDeferred: _$q_.defer(),
        getCurrency: jasmine.createSpy('getCurrency').and.callFake(() => {
          return CurrencyDAO._getCurrencyDeferred.promise;
        }),
      };
      return CurrencyDAO;
    });

    // mock Alert
    Alert = {
      error: jasmine.createSpy('error')
    };
    $provide.value('Alert', Alert);
  }));

  // inject services
  beforeEach(inject((_Currency_, _$rootScope_, _$q_, _EVENTS_) => {
    $rootScope = _$rootScope_;
    $q = _$q_;

    Currency = _Currency_;
    EVENTS = _EVENTS_;
  }));


  it('should be defined', () => {
    expect(Currency).toBeDefined();
  });

  it('should initialize correctly', () => {
    CurrencyDAO._getCurrencyDeferred.resolve({ id: 6, symbol: '$' });
    $rootScope.$apply();

    expect(Currency.getCurrency()).toEqual({ id: 6, symbol: '$' });
    expect(Alert.error).not.toHaveBeenCalled();
    expect(Company.getCompany).toHaveBeenCalled();
    expect(CurrencyDAO.getCurrency).toHaveBeenCalledWith(6);
  });

  it('should reinitialize on company update', () => {
    CurrencyDAO._getCurrencyDeferred.resolve({ id: 6, symbol: '$' });

    $rootScope.$emit(EVENTS.COMPANY_UPDATED);
    $rootScope.$apply();

    expect(Currency.getCurrency()).toEqual({ id: 6, symbol: '$' });

    Company._company = { default_currency: 7 };
    CurrencyDAO._getCurrencyDeferred = $q.defer();
    CurrencyDAO._getCurrencyDeferred.resolve({ id: 7, symbol: 'NZD' });

    $rootScope.$emit(EVENTS.COMPANY_UPDATED);
    $rootScope.$apply();
    expect(Currency.getCurrency()).toEqual({ id: 7, symbol: 'NZD' });
  });
});
