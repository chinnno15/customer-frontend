/**
* SwishApiInterceptor tests
*/
describe('customerApp.services.SwishApiInterceptor', () => {
  let $http, $httpBackend;

  // ENV constant mock
  let ENV = {
    apiRoot: 'http://testserver/api'
  };

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide, $httpProvider) => {
    // mock ENV constant
    $provide.constant('ENV', ENV);
    // initialize SwishApiInterceptor
    $httpProvider.interceptors.push('SwishApiInterceptor');
  }));

  // inject services
  beforeEach(inject((_$http_, _$httpBackend_) => {
    $http = _$http_;
    $httpBackend = _$httpBackend_;
  }));

  // flush and validate $http requests
  afterEach(() => {
    $httpBackend.flush();
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });


  it('adds an URL prefix and trailing slash for Swish server API requests', () => {
    $httpBackend.expectGET('http://testserver/api/test/').respond('OK');
    $http.get('/test');
    $httpBackend.expectGET('http://testserver/api/test/').respond('OK');
    $http.get('test');
    $httpBackend.expectGET('http://testserver/api/test/').respond('OK');
    $http.get('test/');
    $httpBackend.expectPOST('http://testserver/api/test/').respond('OK');
    $http.post('test');
  });

  it('does not add an URL prefix for html view requests', () => {
    $httpBackend.expectGET('views/test.html').respond('OK');
    $http.get('views/test.html');
  });

  it('does not add an URL prefix for absolute URL requests', () => {
    $httpBackend.expectGET('http://someserver/test').respond('OK');
    $http.get('http://someserver/test');
  });
});
