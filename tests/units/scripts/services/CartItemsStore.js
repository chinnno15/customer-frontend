/**
* CartItemsStore tests
*/
describe('customerApp.services.CartItemsStore', () => {
  let CartItemsStore, OrderItemDAO, $q, $rootScope;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    // mock OrderItemDAO
    OrderItemDAO = {
      createOrderItem: jasmine.createSpy('createOrderItem').and.callFake(() => {
        return $q.resolve({ id: 123 });
      }),
      updateOrderItem: jasmine.createSpy('updateOrderItem').and.callFake(() => $q.resolve()),
      deleteOrderItem: jasmine.createSpy('deleteOrderItem').and.callFake(() => $q.resolve()),
      listOrderItems: jasmine.createSpy('listOrderItems').and.callFake(() => $q.resolve([])),
      prioritizeOrderItem: jasmine.createSpy('prioritizeOrderItem').and.callFake(() => $q.resolve()),
    };
    $provide.value('OrderItemDAO', OrderItemDAO);
  }));

  // inject services
  beforeEach(inject((_$q_, _$rootScope_, _CartItemsStore_) => {
    $q = _$q_;
    $rootScope = _$rootScope_;
    // initialize CartItemsStore
    CartItemsStore = _CartItemsStore_;
  }));


  it('should be defined', () => {
    expect(CartItemsStore).toBeDefined();
  });

  describe('initialize()', () => {
    it('should initialize store with items from the server', (done) => {
      OrderItemDAO.listOrderItems = jasmine.createSpy('listOrderItems').and.callFake(() => $q.resolve([{
        id: 11,
        product: 1,
        price: 8,
        quantity: 1,
        product_name: 'name 1',
        product_description: 'description 1',
        product_image: 'image 1',
        product_thumbs: { '50x50': 'image 1' },
        product_default_icon: 16,
        product_default_color: '#fff',
        priority: 1,
        product_option_enabled: true,
        options: [
          { id: 1, is_extra: false, name: 'pref 1' },
          { id: 2, is_extra: false, name: 'pref 2' },
          { id: 3, is_extra: true, name: 'extra 1', quantity: 2 },
        ]
      }, {
        id: 22,
        product: 2,
        price: 22,
        quantity: 2,
        product_name: 'name 2',
        product_description: 'description 2',
        product_image: 'image 2',
        product_thumbs: { '50x50': 'image 2' },
        product_default_icon: 8,
        product_default_color: '#000',
        priority: 2,
        product_option_enabled: false,
      },
      ]));
      CartItemsStore.initialize()
      .then(() => {
        expect(OrderItemDAO.listOrderItems).toHaveBeenCalled();
        expect(CartItemsStore.getItem(1)).toEqual({
          product: {
            id: 1,
            price: 8,
            name: 'name 1',
            description: 'description 1',
            image: 'image 1',
            thumbs: { '50x50': 'image 1' },
            default_icon: 16,
            default_color: '#fff',
            priority: 1,
            is_option_enabled: true,
          },
          quantity: 1,
          optionInObject: {
            preferences: [
              { id: 1, is_extra: false, name: 'pref 1' },
              { id: 2, is_extra: false, name: 'pref 2' },
            ],
            extras: [
              { id: 3, is_extra: true, name: 'extra 1', quantity: 2 },
            ]
          }
        });
        expect(CartItemsStore.getItem(2)).toEqual({
          product: {
            id: 2,
            price: 22,
            name: 'name 2',
            description: 'description 2',
            image: 'image 2',
            thumbs: { '50x50': 'image 2' },
            default_icon: 8,
            default_color: '#000',
            priority: 2,
            is_option_enabled: false,
          },
          quantity: 2,
          optionInObject: {
            preferences: [],
            extras: []
          }
        });
        done();
      });
      $rootScope.$apply();
    });
  });

  describe('createItem()', () => {
    it('should correctly create new cart item', () => {
      CartItemsStore.createItem({ id: 3, price: 6 }, 2);
      expect(OrderItemDAO.createOrderItem).toHaveBeenCalledWith(3, 2);
      expect(CartItemsStore.getItem(3)).toEqual({
        product: { id: 3, price: 6 },
        quantity: 2
      });
    });
  });

  describe('prioritizeItem()', () => {
    it('should fail when specified item does not exist, just like updateQuantity()', (done) => {
      CartItemsStore.updateQuantity(3, 3)
      .catch((err) => {
        expect(CartItemsStore.getItem(3)).toBeUndefined();
        expect(err).toEqual(CartItemsStore.ERRORS.NOT_FOUND);
        done();
      });
      $rootScope.$apply();
    });

    it('should correctly prioritize cart item', (done) => {
      CartItemsStore.createItem({ id: 3, price: 8 }, 2);
      const promise = CartItemsStore.prioritizeItem(3, 3);
      expect(CartItemsStore.getItem(3)).toEqual({
        product: { id: 3, price: 8 },
        quantity: 2,
        priority: 3,
      });
      promise.then(() => {
        expect(OrderItemDAO.prioritizeOrderItem).toHaveBeenCalledWith(123, 3, 3);
        done();
      });
      $rootScope.$apply();
    });
  });

  describe('updateQuantity()', () => {
    it('should fail when specified item does not exist', (done) => {
      CartItemsStore.updateQuantity(3, 3)
      .catch((err) => {
        expect(CartItemsStore.getItem(3)).toBeUndefined();
        expect(err).toEqual(CartItemsStore.ERRORS.NOT_FOUND);
        done();
      });
      $rootScope.$apply();
    });

    it('should correctly update cart item', (done) => {
      CartItemsStore.createItem({ id: 3, price: 8 }, 2);
      const promise = CartItemsStore.updateQuantity(3, 3);
      expect(CartItemsStore.getItem(3)).toEqual({
        product: { id: 3, price: 8 },
        quantity: 3,
      });
      promise.then(() => {
        expect(OrderItemDAO.updateOrderItem).toHaveBeenCalledWith({ id: 123, product: 3, quantity: 3 });
        done();
      });
      $rootScope.$apply();
    });
  });

  describe('deleteItem()', () => {
    it('should fail when specified item does not exist', (done) => {
      CartItemsStore.deleteItem(3)
      .catch((err) => {
        expect(err).toEqual(CartItemsStore.ERRORS.NOT_FOUND);
        done();
      });
      $rootScope.$apply();
    });

    it('should correctly delete cart item', (done) => {
      CartItemsStore.createItem({ id: 4, price: 8 }, 2);
      const promise = CartItemsStore.deleteItem(4);
      expect(CartItemsStore.getItem(4)).toBeUndefined();
      promise.then(() => {
        expect(OrderItemDAO.deleteOrderItem).toHaveBeenCalledWith(123);
        done();
      });
      $rootScope.$apply();
    });
  });

  describe('deleteAllItems()', () => {
    it('should correctly delete all cart items', (done) => {
      CartItemsStore.createItem({ id: 4, price: 8 }, 2);
      CartItemsStore.createItem({ id: 3, price: 10 }, 1);
      CartItemsStore.createItem({ id: 2, price: 4 }, 6);
      const promise = CartItemsStore.deleteAllItems();
      expect(CartItemsStore.getItem(4)).toBeUndefined();
      expect(CartItemsStore.getItem(3)).toBeUndefined();
      expect(CartItemsStore.getItem(2)).toBeUndefined();
      promise.then(() => {
        expect(OrderItemDAO.deleteOrderItem).toHaveBeenCalledWith(123);
        expect(OrderItemDAO.deleteOrderItem.calls.count()).toEqual(3);
        done();
      });
      $rootScope.$apply();
    });
  });

  describe('createOrUpdateItem()', () => {
    it('should call createItem() when item does not exist', () => {
      CartItemsStore.createOrUpdateItem({ id: 3, price: 8 }, 2);
      expect(OrderItemDAO.createOrderItem).toHaveBeenCalledWith(3, 2);
    });

    it('should call updateQuantity() when item already exist', (done) => {
      CartItemsStore.createItem({ id: 3, price: 8 }, 1);
      CartItemsStore.createOrUpdateItem({ id: 3, price: 8 }, 2)
      .then(() => {
        expect(OrderItemDAO.updateOrderItem).toHaveBeenCalledWith({ id: 123, product: 3, quantity: 2 });
        done();
      });
      $rootScope.$apply();
    });
  });

  describe('getItems()', () => {
    it('should return empty array when store is empty', () => {
      expect(CartItemsStore.getItems()).toEqual([]);
    });

    it('should return store items', (done) => {
      $q.all([
        CartItemsStore.createItem({ id: 1, price: 6 }, 1),
        CartItemsStore.createItem({ id: 2, price: 8 }, 2),
      ])
      .then(() => {
        expect(CartItemsStore.getItems()).toEqual([
          { product: { id: 1, price: 6 }, quantity: 1 },
          { product: { id: 2, price: 8 }, quantity: 2 },
        ]);
        done();
      });
      $rootScope.$apply();
    });
  });

  describe('clean()', () => {
    it('should empty the store', (done) => {
      $q.all([
        CartItemsStore.createItem({ id: 1, price: 6 }, 1),
        CartItemsStore.createItem({ id: 2, price: 8 }, 2),
      ])
      .then(() => {
        CartItemsStore.clean();
        expect(CartItemsStore.getItems()).toEqual([]);
        done();
      });
      $rootScope.$apply();
    });
  });

  describe('setItemOptions()', () => {
    it('should not set option when product is not available in Cart', () => {
      CartItemsStore.setItemOptions(10, {});
      expect(OrderItemDAO.updateOrderItem).not.toHaveBeenCalled();
    });

    it('should work and call update item', (done) => {
      CartItemsStore.createItem({ id: 11, price: 6 }, 1);

      const promise = CartItemsStore.setItemOptions(11, {
        preferences: [
          { id: 1, is_extra: false, name: 'pref 1' },
        ],
        extras: [
          { id: 3, is_extra: true, name: 'extra 1', quantity: 2 },
        ]
      });
      promise.then(() => {
        expect(OrderItemDAO.updateOrderItem).toHaveBeenCalledWith({
          id: 123,
          product: 11,
          quantity: 1,
          options: [
            { id: 1, is_extra: false, name: 'pref 1' },
            { id: 3, is_extra: true, name: 'extra 1', quantity: 2 },
          ]
        });
        done();
      });

      $rootScope.$apply();
    });
  });

  describe('convertOptionToArray()', () => {
    it('should convert optionInObject to an array', () => {
      const arr = CartItemsStore.convertOptionToArray({
        preferences: [
          { id: 1, is_extra: false, name: 'pref 1' },
        ],
        extras: [
          { id: 3, is_extra: true, name: 'extra 1', quantity: 2 },
        ]
      });

      expect(arr).toEqual([
        { id: 1, is_extra: false, name: 'pref 1' },
        { id: 3, is_extra: true, name: 'extra 1', quantity: 2 },
      ]);
    });
  });
});
