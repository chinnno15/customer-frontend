/**
* GoogleApiLoader tests
*/
describe('customerApp.services.GoogleApiLoader', () => {
  let GoogleApiLoader, $rootScope, $window, $q, originalDefer;

  beforeEach(angular.mock.module('customerApp.services'));

  beforeEach(inject((_GoogleApiLoader_, _$q_, _$rootScope_, _$window_) => {
    $rootScope = _$rootScope_;
    $window = _$window_;
    $q = _$q_;
    GoogleApiLoader = _GoogleApiLoader_;

    // This is a hack to resolve promises defined by GoogleApiLoader outside
    // the Angular digest loop. Resolving promises outside Angular digest loop in normal
    // execution is fine, as $q service will run the digest loop if needed.
    // However this is not the case in tests. Standard way of calling $rootScope.$apply()
    // in the test to resolve pending promises would not work, as GAPI script loading is async,
    // so the $digest loop will run before the script loads and GoogleApiLoader even
    // creates the promise.
    //
    // This hack basically redefines $q.defer.resolve() method to sth similar to standard non-test
    // implementation, and makes resolve() to call $rootScope.$apply() to start digest loop.
    originalDefer = $q.defer;
    $q.defer = () => {
      const deferred = originalDefer();
      const originalResolve = deferred.resolve;

      deferred.resolve = (data) => {
        originalResolve(data);
        if (!$rootScope.$$phase) {
          $rootScope.$apply();
        }
      };
      return deferred;
    };
  }));

  afterEach(() => {
    // make sure $q.defer goes back to normal for other tests
    $q.defer = originalDefer;
  });

  it('should be defined', () => {
    expect(GoogleApiLoader).toBeDefined();
  });

  describe('load function', () => {
    it('should be defined', () => {
      expect(GoogleApiLoader.load).toBeDefined();
    });

    it('should load the gapi', (done) => {
      GoogleApiLoader.load()
        .then((gapi) => {
          expect(gapi.client).toBeDefined();
          expect($window.gapi.client).toBeDefined();
          done();
        });
    }, 20000);
  });

  describe('isLoaded function', () => {
    it('should be defined', () => {
      expect(GoogleApiLoader.isLoaded).toBeDefined();
    });

    it('should return true after gapi is loaded', (done) => {
      expect(GoogleApiLoader.isLoaded()).toEqual(false);

      GoogleApiLoader.load()
        .then(() => {
          expect(GoogleApiLoader.isLoaded()).toEqual(true);
          done();
        });
    }, 20000);
  });
});
