describe('I18n', () => {
  let I18n;

  beforeEach(angular.mock.module('customerApp'));

  beforeEach(inject((_I18n_) => {
    I18n = _I18n_;
  }));


  it('should be defined', () => {
    expect(I18n).toBeDefined();
  });


  describe('load()', () => {
    let $httpBackend;

    beforeEach(inject((_$httpBackend_) => {
      $httpBackend = _$httpBackend_;
    }));

    it('should make request', () => {
      $httpBackend.expectGET('lang/en-us.json').respond({});
      I18n.load('en-us');
    });
  });
});
