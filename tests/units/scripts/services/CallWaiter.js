/**
* CallWaiter tests
*/
describe('customerApp.services.CallWaiter', () => {
  let $rootScope;
  let CallWaiterDAO;
  let CallWaiter;
  let Alert;
  let $interval;
  let $q;
  let localStorageService;
  let TopSheet;
  let Modal;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide, $controllerProvider) => {
    // mock CallWaiterDAO
    CallWaiterDAO = {
      _state: 2,
      getCalls: jasmine.createSpy('getCalls').and.callFake((callIds = []) => {
        const result = callIds.map((callId)=>{
          return { state: CallWaiterDAO._state, id: callId, company_order_id: 1 };
        });

        return $q.resolve(result);
      }),
      createCall: jasmine.createSpy('createCall').and.callFake(() => $q.resolve({ id: 123 })),
    };

    $provide.value('CallWaiterDAO', CallWaiterDAO);

    // mock localStorageService
    localStorageService = {
      _store: {},
      set: jasmine.createSpy('set').and.callFake((k, v) => localStorageService._store[k] = v),
      get: jasmine.createSpy('get').and.callFake((k) => localStorageService._store[k]),
      remove: jasmine.createSpy('remove').and.callFake((k) => delete localStorageService._store[k]),
      keys: jasmine.createSpy('keys').and.callFake(() => Object.keys(localStorageService._store))
    };

    $provide.value('localStorageService', localStorageService);

    // mock Alert service
    Alert = {
      info: jasmine.createSpy('info'),
      success: jasmine.createSpy('success'),
      error: jasmine.createSpy('error'),
    };

    $provide.value('Alert', Alert);

    // mock TopSheet service
    TopSheet = {
      show: jasmine.createSpy('show'),
      close: jasmine.createSpy('close')
    };

    $provide.value('TopSheet', TopSheet);

    // mock Modal service
    Modal = {
      show: jasmine.createSpy('show'),
      close: jasmine.createSpy('close')
    };

    $provide.value('Modal', Modal);

    // mock CallWaiterController
    $controllerProvider.register('CallWaiterController', function() {
      return this;
    });
  }));

  // inject services
  beforeEach(angular.mock.inject((_$q_, _$rootScope_, _CallWaiter_, _$interval_) => {
    $q = _$q_;
    CallWaiter = _CallWaiter_;
    CallWaiter.init();
    $interval = _$interval_;
    $rootScope = _$rootScope_;
  }));

  describe('callWaiter()', () => {
    it('should start the interval and fetch waiter call after 5000 ms', (done) => {
      CallWaiter.callWaiter()
        .then(callId => {
          expect(callId).toEqual(123);
          expect(CallWaiterDAO.createCall).toHaveBeenCalled();
          expect(localStorageService.get).toHaveBeenCalledWith('CALL_WAITER_ID');
          expect(localStorageService.set).toHaveBeenCalledWith('CALL_WAITER_ID', 123);
          done();
        });
      $rootScope.$apply();
    });
    it('should start the interval and fetch waiter call after 5000 ms w/orderId and message', (done) => {
      const orderId = 2;
      const callOrderId = `CALL_WAITER_ID:${orderId}`;
      CallWaiter.callWaiter(orderId, 'testing..')
        .then(callId => {
          expect(callId).toEqual(123);
          expect(localStorageService.set).toHaveBeenCalledWith(callOrderId, 123);
          expect(CallWaiterDAO.createCall).toHaveBeenCalledWith(2, 'testing..');
          expect(localStorageService.get).toHaveBeenCalledWith(callOrderId);
          expect(localStorageService.set).toHaveBeenCalledWith(callOrderId, 123);
          expect(CallWaiterDAO.getCalls).toHaveBeenCalledWith([123]);
          done();
        });
      $interval.flush(5000);
      $rootScope.$apply();
    });
    it('should start the interval and fetch waiter call after 5000 ms w/orderId and message for two separate orders', (done) => {
      const orderIdOne = 1;
      const orderIdTwo = 2;
      const callOrderIdOne = CallWaiter._generateOrderCallId(orderIdOne);
      const callOrderIdTwo = CallWaiter._generateOrderCallId(orderIdTwo); // eslint-disable-line no-unused-vars

      $q.all([
        CallWaiter.callWaiter(orderIdTwo, 'testing..'),
        CallWaiter.callWaiter(orderIdOne, 'testing..')
      ])
        .then(() => {
          // check first call
          expect(localStorageService.set).toHaveBeenCalledWith(callOrderIdOne, 123);
          expect(localStorageService.get).toHaveBeenCalledWith(callOrderIdOne);
          expect(CallWaiterDAO.createCall).toHaveBeenCalledWith(orderIdOne, 'testing..');

          // check second call
          expect(localStorageService.get).toHaveBeenCalledWith(callOrderIdTwo);
          expect(localStorageService.set).toHaveBeenCalledWith(callOrderIdTwo, 123);
          expect(CallWaiterDAO.createCall).toHaveBeenCalledWith(orderIdTwo, 'testing..');

          expect(CallWaiterDAO.getCalls).toHaveBeenCalled();
          done();
        });

      $interval.flush(5000);
      $rootScope.$apply();
    });

    it('should only call waiter once, even if called multiple times', (done) => {
      CallWaiter.callWaiter()
        .then(() => CallWaiter.callWaiter())
        .catch(() => {
          done();
        });
      $rootScope.$apply();
    });

    it('should stop interval after waiter accepts the call', () => {
      CallWaiter.callWaiter();
      $rootScope.$apply();

      expect(CallWaiterDAO.getCalls.calls.count()).toEqual(0);

      $interval.flush(5000);
      $rootScope.$apply();

      expect(CallWaiterDAO.getCalls.calls.count()).toEqual(1);

      $interval.flush(5000);
      $rootScope.$apply();

      expect(CallWaiterDAO.getCalls.calls.count()).toEqual(1);
    });

    it('should stop interval after waiter rejects the call', () => {
      CallWaiterDAO._state = 100;
      CallWaiter.callWaiter();
      $rootScope.$apply();

      expect(CallWaiterDAO.getCalls.calls.count()).toEqual(0);

      $interval.flush(5000);
      $rootScope.$apply();

      expect(CallWaiterDAO.getCalls.calls.count()).toEqual(1);

      $interval.flush(5000);
      $rootScope.$apply();

      expect(CallWaiterDAO.getCalls.calls.count()).toEqual(1);
    });

    it('should stop interval after call expires', () => {
      CallWaiterDAO._state = 3;
      CallWaiter.callWaiter();
      $rootScope.$apply();

      expect(CallWaiterDAO.getCalls.calls.count()).toEqual(0);

      $interval.flush(5000);
      $rootScope.$apply();

      expect(CallWaiterDAO.getCalls.calls.count()).toEqual(1);

      $interval.flush(5000);
      $rootScope.$apply();

      expect(CallWaiterDAO.getCalls.calls.count()).toEqual(1);
    });

    it('should not stop interval if waiter have not responded', () => {
      CallWaiterDAO._state = 1;
      CallWaiter.callWaiter();
      $rootScope.$apply();

      expect(CallWaiterDAO.getCalls.calls.count()).toEqual(0);

      $interval.flush(5000);
      $rootScope.$apply();

      expect(CallWaiterDAO.getCalls.calls.count()).toEqual(1);

      $interval.flush(5000);
      $rootScope.$apply();

      expect(CallWaiterDAO.getCalls.calls.count()).toEqual(2);
    });

    it('should not create new call if waiter was already called before', (done) => {
      localStorageService._store.CALL_WAITER_ID = 456;
      CallWaiter.callWaiter()
      .catch(()=>{
        expect(localStorageService.set).not.toHaveBeenCalled();
        expect(CallWaiterDAO.createCall).not.toHaveBeenCalled();
        expect(localStorageService.get).toHaveBeenCalledWith('CALL_WAITER_ID');
        expect(localStorageService.set).not.toHaveBeenCalled();
        done();
      });

      $interval.flush(5000);
      $rootScope.$apply();
    });

    it('should not create new call w/message if waiter was already called before', (done) => {
      const orderId = 1;
      const callOrderId = CallWaiter._generateOrderCallId(orderId);
      localStorageService._store[callOrderId] = 456;
      CallWaiter.callWaiter(orderId, 'I have a question about my order.')
      .catch(() => {
        expect(localStorageService.get).toHaveBeenCalledWith(callOrderId);
        expect(localStorageService.set).not.toHaveBeenCalled();
        done();
      });


      $interval.flush(5000);
      $rootScope.$apply();
    });
  });
  describe('_clearCallOrder()', () => {
    it('should clear all call order ids', () => {
      const orderId = 2;
      const callId = 456;
      const callOrderId = CallWaiter._generateOrderCallId(orderId);

      // set callOrderId/callId pair in localStorage
      localStorageService._store[callOrderId] = callId;

      // remove callOrderId key from localstorage for matching callId
      CallWaiter._clearCallOrder(callId);

      expect(localStorageService.remove).toHaveBeenCalledWith(callOrderId);
    });
  });
  describe('_getCallIds()', () => {
    it('should get all call ids in localStorage', () => {
      const orderId = 2;
      const callId = 456;
      const callOrderId = CallWaiter._generateOrderCallId(orderId);

      // set callOrderId/callId pair in localStorage
      localStorageService._store[callOrderId] = callId;

      // remove callOrderId key from localstorage for matching callId
      const callIds = CallWaiter._getCallIds();

      expect(callIds).toEqual([callId]);
      expect(localStorageService.get).toHaveBeenCalledWith(callOrderId);
    });
  });
  describe('_checkCalls()', () => {
    it('should get state for each waiter call in localstorage', () => {
      const orderId = 2;
      const callId = 456;
      const callOrderId = CallWaiter._generateOrderCallId(orderId);

      // set callOrderId/callId pair in localStorage
      localStorageService._store[callOrderId] = callId;

      CallWaiter._checkCalls();

      expect(CallWaiterDAO.getCalls).toHaveBeenCalledWith([callId]);
    });
  });
  describe('showCallWaiterMessageSheet()', () => {
    it('should show topsheet with callwaiter.jade template', () => {
      const orderId = 2;
      CallWaiter.showCallWaiterMessageSheet(orderId);
      expect(TopSheet.show).toHaveBeenCalled();
    });
    it('should not show topsheet if waiter has already been called for this order', (done) => {
      const orderId = 2;
      const callOrderId = `CALL_WAITER_ID:${orderId}`;
      CallWaiter.callWaiter(orderId, 'I have a question about my order.')
      .then(() => {
        expect(localStorageService.set).toHaveBeenCalledWith(callOrderId, 123);
        expect(CallWaiterDAO.createCall).toHaveBeenCalled();
        done();
      });

      $rootScope.$apply();

      CallWaiter.showCallWaiterMessageSheet(orderId);

      expect(TopSheet.show).not.toHaveBeenCalled();

      $interval.flush(5000);
      $rootScope.$apply();
    });
  });
});
