/**
* Checkout tests
*/
describe('customerApp.services.Checkout', () => {
  let Checkout;
  let $timeout;
  let $rootScope;
  let $q;
  let $state;
  let UrlRequestSender;
  let StripePayment;
  let PaymentDAO;
  let OrdersStore;
  let Cart;
  let Currency;
  let STATES, PAYMENT_PROVIDERS;

  // load modules
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    // create $state mock
    $state = {
      go: jasmine.createSpy('go')
    };

    // create UrlRequestSender mock
    UrlRequestSender = {
      sendGet: jasmine.createSpy('sendGet'),
      sendPost: jasmine.createSpy('sendPost'),
    };

    // create StripePayment mock
    StripePayment = {
      checkout: jasmine.createSpy('checkout').and.callFake(() => $q.resolve('stripe_token')),
    };

    // create PaymentDAO mock
    PaymentDAO = {
      checkoutWithStripe: jasmine
        .createSpy('checkoutWithStripe')
        .and.callFake(() => $q.resolve({ id: 4, state: 2 })),
      checkoutWithGear: jasmine
        .createSpy('checkoutWithGear')
        .and.callFake(() => $q.resolve({
          payment_url: 'http://gear.com/test',
          swish_order: { id: 5, state: 1 },
        })),
      checkoutWithBilderlingspay: jasmine
        .createSpy('checkoutWithBilderlingspay')
        .and.callFake(() => $q.resolve({
          payment_url: 'http://bilderlingpay.com/test',
          order: { id: 6, state: 1 },
          bilderlingspay: {
            lang: 'en',
            PAYMENT_AMOUNT: 36
          },
        })),
    };

    // create OrdersStore mock
    OrdersStore = {
      setLatestOrder: jasmine.createSpy('setLatestOrder'),
    };

    // create Cart mock
    Cart = {
      getTotals: () => ({ price: 49.5 }),
      clean: jasmine.createSpy('clean'),
    };

    // create Currency mock
    Currency = {
      getCurrency: jasmine.createSpy('getCurrency').and.callFake(() => ({ name: 'EUR' }))
    };

    // provide mocks
    $provide.value('$state', $state);
    $provide.value('UrlRequestSender', UrlRequestSender);
    $provide.value('StripePayment', StripePayment);
    $provide.value('PaymentDAO', PaymentDAO);
    $provide.value('OrdersStore', OrdersStore);
    $provide.value('Cart', Cart);
    $provide.value('Currency', Currency);
  }));

  // inject services
  beforeEach(inject((_Checkout_, _$rootScope_, _$q_, _$timeout_, _STATES_, _PAYMENT_PROVIDERS_) => {
    $q = _$q_;
    $rootScope = _$rootScope_;
    $timeout = _$timeout_;
    STATES = _STATES_;
    PAYMENT_PROVIDERS = _PAYMENT_PROVIDERS_;

    // initialize Checkout
    Checkout = _Checkout_;
  }));


  it('should be defined', () => {
    expect(Checkout).toBeDefined();
  });

  describe('checkout()', () => {
    it('should checkout correctly with Stripe payment', (done) => {
      const loaded = jasmine.createSpy('loaded');

      Checkout.checkout(PAYMENT_PROVIDERS.STRIPE, 'comment', loaded)
      .then(() => {
        expect(Currency.getCurrency).toHaveBeenCalled();
        expect(StripePayment.checkout).toHaveBeenCalledWith(4950, 'EUR');
        expect(Cart.clean).toHaveBeenCalledWith(true);
        expect(loaded).toHaveBeenCalled();
        expect(PaymentDAO.checkoutWithStripe).toHaveBeenCalledWith('stripe_token', 'comment');
        expect(OrdersStore.setLatestOrder).toHaveBeenCalledWith({ id: 4, state: 2 });
        expect($state.go).toHaveBeenCalledWith(STATES.RECEIPT);
        done();
      });
      $rootScope.$apply();
    });

    it('should checkout correctly with Gear payment', () => {
      const loaded = jasmine.createSpy('loaded');

      Checkout.checkout(PAYMENT_PROVIDERS.GEAR, 'comment', loaded);

      $rootScope.$apply();
      $timeout.flush(3000);

      expect(StripePayment.checkout).not.toHaveBeenCalled();
      expect(Cart.clean).not.toHaveBeenCalled();
      expect(loaded).toHaveBeenCalled();
      expect(PaymentDAO.checkoutWithGear).toHaveBeenCalledWith('comment');
      expect(OrdersStore.setLatestOrder).toHaveBeenCalledWith({ id: 5, state: 1 });
      expect(UrlRequestSender.sendGet).toHaveBeenCalledWith('http://gear.com/test');
    });

    it('should checkout correctly with Bilderlingspay payment', () => {
      const loaded = jasmine.createSpy('loaded');

      Checkout.checkout(PAYMENT_PROVIDERS.BILDERLINGSPAY, 'comment', loaded);

      $rootScope.$apply();
      $timeout.flush(3000);

      expect(StripePayment.checkout).not.toHaveBeenCalled();
      expect(Cart.clean).not.toHaveBeenCalled();
      expect(loaded).toHaveBeenCalled();
      expect(PaymentDAO.checkoutWithBilderlingspay).toHaveBeenCalledWith('comment');
      expect(OrdersStore.setLatestOrder).toHaveBeenCalledWith({ id: 6, state: 1 });
      expect(UrlRequestSender.sendPost)
        .toHaveBeenCalledWith('http://bilderlingpay.com/test', {
          '-lang': 'en',
          PAYMENT_AMOUNT: '36.00',
        });
    });
  });
});
