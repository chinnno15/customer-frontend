/**
* UrlRequestSender tests
*/
describe('customerApp.services.UrlRequestSender', () => {
  let UrlRequestSender, $window, $, formSubmitFn, formRemoveFn;

  // inject services
  beforeEach(() => {
    // mock angular.element to mock .submit and .remove methods
    $ = angular.element;

    angular.element = el => {
      const $el = $(el);

      if ($el[0].tagName === 'FORM') {
        formSubmitFn = $el[0].submit = jasmine.createSpy('submit');
        formRemoveFn = $el.remove = jasmine.createSpy('remove');
      }

      return $el;
    };
  });

  afterEach(() => {
    // unmock angular.element
    angular.element = $;
  });


  describe('sendGet()', () => {
    // load the module
    beforeEach(angular.mock.module('customerApp.services', ($provide) => {
      // mock $window
      $window = {
        location: {}
      };
      $provide.value('$window', $window);
    }));

    // inject services
    beforeEach(inject((_UrlRequestSender_) => {
      UrlRequestSender = _UrlRequestSender_;
    }));

    it('should change location to provided url', () => {
      UrlRequestSender.sendGet('http://test.test');
      expect($window.location.href).toEqual('http://test.test');
    });
  });


  describe('sendPost()', () => {
    // load the module
    beforeEach(angular.mock.module('customerApp.services'));

    // inject services
    beforeEach(inject((_UrlRequestSender_, _$window_) => {
      UrlRequestSender = _UrlRequestSender_;
      $window = _$window_;
    }));

    it('should generate and submit a form', () => {
      UrlRequestSender.sendPost('http://test.test', {
        param1: 'value',
        param2: 123.45,
      });

      expect(formSubmitFn).toHaveBeenCalled();
      expect(formRemoveFn).toHaveBeenCalled();

      const $body = $($window.document.body);
      const $form = $body.find('form');

      expect($form.attr('method')).toEqual('POST');
      expect($form.attr('action')).toEqual('http://test.test');

      expect($($form.children()[0]).attr('type')).toEqual('hidden');
      expect($($form.children()[0]).attr('name')).toEqual('param1');
      expect($($form.children()[0]).attr('value')).toEqual('value');
      expect($($form.children()[1]).attr('type')).toEqual('hidden');
      expect($($form.children()[1]).attr('name')).toEqual('param2');
      expect($($form.children()[1]).attr('value')).toEqual('123.45');
    });
  });
});
