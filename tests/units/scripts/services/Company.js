/**
* Company tests
*/
describe('customerApp.service.Company', () => {
  let Company, $q, $rootScope, $interval, CompanyDAO, Alert, EVENTS, INTERVALS;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    // CompanyDAO mock
    $provide.factory('CompanyDAO', (_$q_) => {
      CompanyDAO = {
        _getCompanyDeferred: _$q_.defer(),
        getCompany: jasmine.createSpy('getCompany').and.callFake(() => CompanyDAO._getCompanyDeferred.promise),
      };
      return CompanyDAO;
    });

    // mock Alert
    Alert = {
      error: jasmine.createSpy('error')
    };

    // provide mocks
    $provide.value('Alert', Alert);
  }));

  // inject services
  beforeEach(inject((_Company_, _$rootScope_, _$q_, _$interval_, _EVENTS_, _INTERVALS_) => {
    $rootScope = _$rootScope_;
    $interval = _$interval_;
    $q = _$q_;

    spyOn($rootScope, '$emit').and.callThrough();

    // initialize Company
    Company = _Company_;
    EVENTS = _EVENTS_;
    INTERVALS = _INTERVALS_;
  }));


  it('should be defined', () => {
    expect(Company).toBeDefined();
  });

  it('should fetch company after initialize is called', () => {
    expect(Company.getCompany()).toEqual(null);

    CompanyDAO._getCompanyDeferred.resolve({ name: 'TEST' });
    Company.initialize();
    $rootScope.$apply();

    expect($rootScope.$emit).toHaveBeenCalledWith(EVENTS.COMPANY_UPDATED);
    expect(Company.getCompany()).toEqual({ name: 'TEST' });
    expect(Alert.error).not.toHaveBeenCalled();
  });

  it('should display error when fetching company fails', () => {
    CompanyDAO._getCompanyDeferred.reject();
    Company.initialize();
    $rootScope.$apply();

    expect($rootScope.$emit).not.toHaveBeenCalled();
    expect(Company.getCompany()).toEqual(null);
    expect(Alert.error).toHaveBeenCalledWith('COMPANY.DATA_FAILED');
  });

  it('should fetch company multiple times on interval', () => {
    expect($rootScope.$emit.calls.count()).toEqual(0);

    CompanyDAO._getCompanyDeferred.resolve({ name: 'TEST' });
    Company.initialize();
    $rootScope.$apply();

    expect($rootScope.$emit.calls.count()).toEqual(1);

    expect(Company.getCompany()).toEqual({ name: 'TEST' });

    CompanyDAO._getCompanyDeferred = $q.defer();
    CompanyDAO._getCompanyDeferred.resolve({ name: 'TEST2' });
    $rootScope.$apply();
    expect(Company.getCompany()).toEqual({ name: 'TEST' });

    $interval.flush(INTERVALS.FETCH_COMPANY);
    $rootScope.$apply();
    expect($rootScope.$emit.calls.count()).toEqual(2);
    expect(Company.getCompany()).toEqual({ name: 'TEST2' });

    CompanyDAO._getCompanyDeferred = $q.defer();
    CompanyDAO._getCompanyDeferred.resolve({ name: 'TEST3' });
    $interval.flush(INTERVALS.FETCH_COMPANY);
    $rootScope.$apply();
    expect($rootScope.$emit.calls.count()).toEqual(3);
    expect(Company.getCompany()).toEqual({ name: 'TEST3' });
  });
});
