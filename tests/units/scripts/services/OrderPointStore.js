/**
* OrderPointStore tests
*/
describe('customerApp.services.OrderPointStore', () => {
  let OrderPointStore;
  let OrderPointDAO;
  let localStorageService;
  let $q;
  let $state;
  let $rootScope;
  let orderPoint;
  let company;
  let CompanyDAO;
  let I18n;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    // mock OrderPointDAO
    orderPoint = {
      uuid: '2a887958-fb11-498a-b159-737b14d1318d',
      name: 'Test order point',
      current_language: 'en'
    };

    company = {
      order_point: orderPoint,
      waiters_working: true
    };

    OrderPointDAO = {
      register: jasmine.createSpy('register').and.callFake(() => $q.resolve(orderPoint)),
    };

    CompanyDAO = {
      getCompany: jasmine.createSpy().and.callFake(() => $q.resolve(company)),
    };

    // mock $state
    $state = {
      go: jasmine.createSpy('go'),
    };

    // mock localStorageService
    localStorageService = {
      _store: {},
      set: jasmine.createSpy('set').and.callFake((k, v) => localStorageService._store[k] = v),
      get: jasmine.createSpy('get').and.callFake((k) => localStorageService._store[k]),
    };

    I18n = {
      load: jasmine.createSpy('load')
    };

    $provide.value('$state', $state);
    $provide.value('OrderPointDAO', OrderPointDAO);
    $provide.value('CompanyDAO', CompanyDAO);
    $provide.value('localStorageService', localStorageService);
    $provide.value('I18n', I18n);
  }));

  // inject services
  beforeEach(inject((_$q_, _$rootScope_, _OrderPointStore_) => {
    $q = _$q_;
    $rootScope = _$rootScope_;

    // initialize OrderPointStore
    OrderPointStore = _OrderPointStore_;
  }));


  it('should be defined', () => {
    expect(OrderPointStore).toBeDefined();
  });

  describe('register()', () => {
    it('should execute correctly when UUID is provided', (done) => {
      OrderPointStore.register('a-store', 'an-orderpoint')
        .then(() => {
          expect(OrderPointDAO.register).toHaveBeenCalledWith('a-store', 'an-orderpoint');
          expect(localStorageService.get).toHaveBeenCalledWith('orderPointName');
          expect(localStorageService.set).toHaveBeenCalledWith(
            'orderPointName',
            'Test order point'
          );
          return OrderPointStore.getOrderPoint();
        })
        .then((op) => {
          expect(op).toEqual({
            uuid: '2a887958-fb11-498a-b159-737b14d1318d',
            name: 'Test order point',
            current_language: 'en'
          });
          done();
        });
      $rootScope.$apply();
    });

    it('should fail when orderPointName is not provided and not in localStorage', (done) => {
      OrderPointStore.register()
        .catch((err) => {
          expect(err).toEqual(OrderPointStore.ERRORS.NO_UUID);
          expect(OrderPointDAO.register).not.toHaveBeenCalled();
          expect(localStorageService.get).toHaveBeenCalledWith('orderPointName');
          expect(localStorageService.set).not.toHaveBeenCalled();
          expect(OrderPointStore.orderPoint).toBeUndefined();
          done();
        });
      $rootScope.$apply();
    });

    it('should call I18n.load() when current_language is provided', () => {
      OrderPointStore.register('a-store', 'an-order');
      $rootScope.$apply();
      expect(I18n.load).toHaveBeenCalledWith('en');
    });

    it('should not call I18n.load() when current_language is not provided', () => {
      orderPoint.current_language = undefined;
      OrderPointStore.register('a-store', 'an-order');
      $rootScope.$apply();
      expect(I18n.load).not.toHaveBeenCalled();
    });
  });

  describe('isAvailable()', () => {
    it('should reject if orderpoint is inactive', (done) => {
      orderPoint.is_active = false;

      // these should be ignored, as inactive gets higher priority over closed
      orderPoint.is_open = false;
      orderPoint.schedule = [];

      OrderPointStore.register('a-store', 'an-order');
      $rootScope.$apply();

      OrderPointStore.isAvailable()
        .catch(e => {
          expect(e).toEqual({ error: 'INACTIVE' });
        })
        .finally(done);

      $rootScope.$apply();
      $rootScope.$apply();
    });

    it('should reject if orderpoint is closed', (done) => {
      orderPoint.is_active = true;
      orderPoint.is_open = false;
      orderPoint.schedule = [
        { weekday: 1, times: [{from: '11:11', to: '22:22'}] },
        { weekday: 7, times: [{from: '01:23', to: '23:34'}] },
      ];

      OrderPointStore.register('a-store', 'an-order');
      $rootScope.$apply();

      // please note the schedule has labels appended
      OrderPointStore.isAvailable()
        .catch(e => {
          expect(e).toEqual({
            error: 'CLOSED',
            schedule: [
              { label: 'MON', weekday: 1, times: [{from: '11:11', to: '22:22'}] },
              { label: 'SUN', weekday: 7, times: [{from: '01:23', to: '23:34'}] },
            ],
          });
        })
        .finally(done);

      $rootScope.$apply();
      $rootScope.$apply();
    });

    it('should reject if waiters is not working', (done) => {
      company.waiters_working = false;
      orderPoint.is_active = true;
      orderPoint.is_open = true;

      OrderPointStore.register('a-store', 'an-order');
      $rootScope.$apply();
      OrderPointStore.isAvailable()
        .catch(e => {
          expect(e).toEqual({ error: 'NO_WAITERS' });
        })
        .finally(done);

      $rootScope.$apply();
    });

    it('should resolve if orderpoint is available', () => {
      orderPoint.is_active = true;
      orderPoint.is_open = true;
      company.waiters_working = true;

      OrderPointStore.register('a-store', 'an-order');
      $rootScope.$apply();

      let resolve = false;
      OrderPointStore.isAvailable().then(() => { resolve = true; });

      $rootScope.$apply();
      $rootScope.$apply();

      expect(resolve).toBe(true);
    });
  });
});
