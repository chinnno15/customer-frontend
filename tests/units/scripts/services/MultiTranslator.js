/**
* MultiTranslator tests
*/
describe('customerApp.service.MultiTranslator', () => {
  let MultiTranslator, $q, $rootScope, GoogleTranslator;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    // GoogleTranslator mock
    GoogleTranslator = {
      translate: jasmine.createSpy('translate').and.callFake(() => $q.resolve([
        { translatedText: 'ケーキ' },
        { translatedText: '凄いケーキ' },
        { translatedText: 'お茶' },
        { translatedText: '美味しいお茶' },
      ])),
    };

    // provide mocks
    $provide.value('GoogleTranslator', GoogleTranslator);
  }));

  // inject services
  beforeEach(inject((_MultiTranslator_, _$rootScope_, _$q_) => {
    $rootScope = _$rootScope_;
    $q = _$q_;

    // initialize MultiTranslator
    MultiTranslator = _MultiTranslator_;
  }));


  it('should be defined', () => {
    expect(MultiTranslator).toBeDefined();
  });

  describe('translate()', () => {
    it('should return passed objects array if it is empty', (done) => {
      MultiTranslator.translate([], ['desc'], 'en', 'jp')
        .then(objects => {
          expect(objects).toEqual([]);
          done();
        });
      $rootScope.$apply();
    });

    it('should return passed objects array if properties not specified', (done) => {
      MultiTranslator.translate([{ name: 'Cake' }, { name: 'Tea' }], [], 'en', 'jp')
        .then(objects => {
          expect(objects).toEqual([{ name: 'Cake' }, { name: 'Tea' }]);
          done();
        });
      $rootScope.$apply();
    });

    it('should return passed objects array if nothing to translate', (done) => {
      MultiTranslator.translate([{ name: 'Cake' }, { name: 'Tea' }], ['desc'], 'en', 'jp')
        .then(objects => {
          expect(objects).toEqual([{ name: 'Cake' }, { name: 'Tea' }]);
          done();
        });
      $rootScope.$apply();
    });

    it('should return passed objects array if same language', (done) => {
      MultiTranslator.translate([{ name: 'Cake' }, { name: 'Tea' }], ['name'], 'en', 'en')
        .then(objects => {
          expect(objects).toEqual([{ name: 'Cake' }, { name: 'Tea' }]);
          done();
        });
      $rootScope.$apply();
    });

    it('should return translated objects array', (done) => {
      MultiTranslator.translate([
        { name: 'Cake', desc: 'Amazing cake', id: 123 },
        { name: 'Tea', desc: 'Delicious tea', id: 124, comment: 'Bla bla' },
      ], ['name', 'desc'], 'en', 'jp')
        .then(objects => {
          expect(objects).toEqual([
            { name: 'ケーキ', desc: '凄いケーキ', id: 123 },
            { name: 'お茶', desc: '美味しいお茶', id: 124, comment: 'Bla bla' },
          ]);
          done();
        });
      $rootScope.$apply();
    });
  });
});
