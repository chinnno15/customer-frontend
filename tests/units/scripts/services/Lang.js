/**
* Lang tests
*/
describe('customerApp.service.Lang', () => {
  let Lang, $q, $rootScope, $window, localStorageService, CompanyDAO, EVENTS;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    // localStorageService mock
    localStorageService = {
      set: jasmine.createSpy('set'),
      get: jasmine.createSpy('get').and.callFake(() => localStorageService._lang),
    };

    // CompanyDAO mock
    CompanyDAO = {
      getCompany: jasmine.createSpy('getCompany').and.callFake(() => $q.resolve({
        default_language: 'en',
      }))
    };

    // $window mock
    $window = {
      navigator: {}
    };

    // provide mocks
    $provide.value('localStorageService', localStorageService);
    $provide.value('CompanyDAO', CompanyDAO);
    $provide.value('$window', $window);
  }));

  // inject services
  beforeEach(inject((_Lang_, _$rootScope_, _$q_, _EVENTS_) => {
    $rootScope = _$rootScope_;
    $q = _$q_;

    spyOn($rootScope, '$emit').and.callThrough();

    // initialize Lang
    Lang = _Lang_;
    EVENTS = _EVENTS_;
  }));


  it('should be defined', () => {
    expect(Lang).toBeDefined();
  });

  describe('getDefaultLanguage()', () => {
    it('should return default language', (done) => {
      Lang.getDefaultLanguage()
        .then(defaultLanguage => {
          expect(CompanyDAO.getCompany).toHaveBeenCalled();
          expect(defaultLanguage).toEqual('en');
          done();
        });

      $rootScope.$apply();
    });

    it('should cache default language and send a request only once', (done) => {
      Lang.getDefaultLanguage();
      $rootScope.$apply();

      Lang.getDefaultLanguage();
      $rootScope.$apply();

      Lang.getDefaultLanguage()
        .then(() => {
          expect(CompanyDAO.getCompany.calls.count()).toEqual(1);
          done();
        });

      $rootScope.$apply();
    });
  });

  describe('getSelectedLanguage()', () => {
    it('should return default language if language not selected', (done) => {
      Lang.getSelectedLanguage()
        .then(selectedLanguage => {
          expect(CompanyDAO.getCompany).toHaveBeenCalled();
          expect(localStorageService.get).toHaveBeenCalledWith('MenuSelectedLanguage');
          expect(selectedLanguage).toEqual('en');
          done();
        });

      $rootScope.$apply();
    });

    it('should return selected language', (done) => {
      localStorageService._lang = 'jp';
      Lang.getSelectedLanguage()
        .then(selectedLanguage => {
          expect(CompanyDAO.getCompany).toHaveBeenCalled();
          expect(localStorageService.get).toHaveBeenCalledWith('MenuSelectedLanguage');
          expect(selectedLanguage).toEqual('jp');
          done();
        });

      $rootScope.$apply();
    });
  });

  describe('setSelectedLanguage()', () => {
    it('should set selected language', () => {
      Lang.setSelectedLanguage('ru');
      expect(CompanyDAO.getCompany).not.toHaveBeenCalled();
      expect(localStorageService.get).toHaveBeenCalledWith('MenuSelectedLanguage');
      expect(localStorageService.set).toHaveBeenCalledWith('MenuSelectedLanguage', 'ru');
      expect($rootScope.$emit).toHaveBeenCalledWith(EVENTS.MENU_LANGUAGE_CHANGED);
    });

    it('should convert language code to ISO 639-1', () => {
      Lang.setSelectedLanguage('en-US');
      expect(CompanyDAO.getCompany).not.toHaveBeenCalled();
      expect(localStorageService.get).toHaveBeenCalledWith('MenuSelectedLanguage');
      expect(localStorageService.set).toHaveBeenCalledWith('MenuSelectedLanguage', 'en');
      expect($rootScope.$emit).toHaveBeenCalledWith(EVENTS.MENU_LANGUAGE_CHANGED);
    });

    it('should not emit event if language have not changed', () => {
      localStorageService._lang = 'ru';
      Lang.setSelectedLanguage('ru');
      expect(CompanyDAO.getCompany).not.toHaveBeenCalled();
      expect(localStorageService.get).toHaveBeenCalledWith('MenuSelectedLanguage');
      expect(localStorageService.set).toHaveBeenCalledWith('MenuSelectedLanguage', 'ru');
      expect($rootScope.$emit).not.toHaveBeenCalled();
    });
  });

  describe('getLangShortCode()', () => {
    it('should convert language to ISO 639-1 code', () => {
      expect(Lang.getLangShortCode('en-US')).toEqual('en');
      expect(Lang.getLangShortCode('en_GB')).toEqual('en');
      expect(Lang.getLangShortCode('en')).toEqual('en');
      expect(Lang.getLangShortCode('EN')).toEqual('en');
      expect(Lang.getLangShortCode('EN_NZ')).toEqual('en');
    });

    it('should not fail on empty input', () => {
      expect(Lang.getLangShortCode('')).toEqual('');
      expect(Lang.getLangShortCode(null)).toEqual(null);
      expect(Lang.getLangShortCode(undefined)).toEqual(undefined);
    });
  });

  describe('getBrowserLanguage()', () => {
    it('should get browser language in modern browsers', () => {
      $window.navigator.languages = ['en-US', 'en'];
      $window.navigator.language = 'en';
      expect(Lang.getBrowserLanguage()).toEqual('en-US');
    });

    it('should get browser language in older browsers', () => {
      $window.navigator.language = 'en';
      expect(Lang.getBrowserLanguage()).toEqual('en');
    });
  });
});
