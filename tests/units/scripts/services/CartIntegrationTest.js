/**
* Cart integration tests
*/
describe('customerApp.services.Cart (integration test)', () => {
  let Cart, Translator, DAOS, $httpBackend;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', 'customerApp.dao', ($provide) => {
    // mock out Translator service
    Translator = { translate: arr => arr };
    $provide.value('Translator', Translator);
  }));

  // inject services
  beforeEach(inject((_$httpBackend_, _Cart_, _DAOS_) => {
    $httpBackend = _$httpBackend_;
    DAOS = _DAOS_;
    // initialize Cart
    Cart = _Cart_;
  }));

  describe('setQuantity()', () => {
    it('should execute all orderitem requests in a correct sequence', (done) => {
      $httpBackend
      .expectPOST('orderitem', { product: 1, quantity: 1, status: 0 })
      .respond({ id: 6 });

      $httpBackend.expectPUT('orderitem/6', { product: 1, quantity: 2 }).respond('OK');

      $httpBackend.expectPUT('orderitem/6', { product: 1, quantity: 1 }).respond('OK');

      $httpBackend.expectDELETE('orderitem/6').respond('OK');

      $httpBackend
      .expectPOST('orderitem', { product: 1, quantity: 1, status: 0 })
      .respond({ id: 7 });

      $httpBackend.expectDELETE('orderitem/7').respond('OK');

      $httpBackend
      .expectPOST('orderitem', { product: 1, quantity: 1, status: 0 })
      .respond({ id: 8 });

      $httpBackend.expectPUT('orderitem/8', { product: 1, quantity: 2 }).respond('OK');

      Cart.setQuantity({ id: 1, price: 6 }, 1);
      Cart.setQuantity({ id: 1, price: 6 }, 2);
      Cart.setQuantity({ id: 1, price: 6 }, 1);
      Cart.setQuantity({ id: 1, price: 6 }, 0);
      Cart.setQuantity({ id: 1, price: 6 }, 1);
      Cart.setQuantity({ id: 1, price: 6 }, 0);
      Cart.setQuantity({ id: 1, price: 6 }, 1);
      Cart.setQuantity({ id: 1, price: 6 }, 2)
      .then(() => {
        expect(Cart.getQuantity(1)).toEqual(2);
        expect(Cart.getTotals()).toEqual({ price: 12, quantity: 2 });
        done();
      });
      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should correctly remove cart item after its been created', (done) => {
      $httpBackend
      .expectPOST('orderitem', { product: 1, quantity: 1, status: 0 })
      .respond({ id: 6 });

      $httpBackend.expectDELETE('orderitem/6').respond('OK');

      Cart.setQuantity({ id: 1, price: 6 }, 1);
      Cart.setQuantity({ id: 1, price: 6 }, 0)
      .then(() => {
        expect(Cart.getQuantity(1)).toEqual(0);
        expect(Cart.getTotals()).toEqual({ price: 0, quantity: 0 });
        done();
      });
      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should correctly remove all cart item', (done) => {
      $httpBackend
      .expectPOST('orderitem', { product: 1, quantity: 1, status: 0 })
      .respond({ id: 11 });
      $httpBackend
      .expectPOST('orderitem', { product: 2, quantity: 2, status: 0 })
      .respond({ id: 12 });
      $httpBackend
      .expectPOST('orderitem', { product: 3, quantity: 4, status: 0 })
      .respond({ id: 13 });
      $httpBackend
      .expectPOST('orderitem', { product: 4, quantity: 6, status: 0 })
      .respond({ id: 14 });

      $httpBackend.expectDELETE('orderitem/11').respond('OK');
      $httpBackend.expectDELETE('orderitem/12').respond('OK');
      $httpBackend.expectDELETE('orderitem/13').respond('OK');
      $httpBackend.expectDELETE('orderitem/14').respond('OK');

      Cart.setQuantity({ id: 1, price: 6 }, 1);
      Cart.setQuantity({ id: 2, price: 8 }, 2);
      Cart.setQuantity({ id: 3, price: 10 }, 4);
      Cart.setQuantity({ id: 4, price: 12 }, 6);

      Cart.deleteAllItems()
      .then(() => {
        expect(Cart.getQuantity(1)).toEqual(0);
        expect(Cart.getQuantity(2)).toEqual(0);
        expect(Cart.getQuantity(3)).toEqual(0);
        expect(Cart.getQuantity(4)).toEqual(0);
        expect(Cart.getTotals()).toEqual({ price: 0, quantity: 0 });
        done();
      });
      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should correctly remove cart item that was fetched from backend', (done) => {
      $httpBackend.expectGET(`orderitem?page_size=${DAOS.NO_PAGINATION}&status=0`).respond({
        results: [
          { id: 12, product: 5, price: 35, quantity: 1 }
        ]
      });

      Cart.initialize();

      $httpBackend.flush();
      expect(Cart.getTotals()).toEqual({ price: 35, quantity: 1 });

      $httpBackend.expectDELETE('orderitem/12').respond('OK');

      Cart.setQuantity({ id: 5, price: 35 }, 0)
      .then(() => {
        expect(Cart.getQuantity(5)).toEqual(0);
        expect(Cart.getTotals()).toEqual({ price: 0, quantity: 0 });
        done();
      });
      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should correctly update cart item that was fetched from backend', (done) => {
      $httpBackend
      .expectGET(`orderitem?page_size=${DAOS.NO_PAGINATION}&status=0`)
      .respond({
        results: [
          { id: 12, product: 5, price: 35, quantity: 2 }
        ]
      });

      Cart.initialize();

      $httpBackend.flush();
      expect(Cart.getTotals()).toEqual({ price: 70, quantity: 2 });

      $httpBackend.expectPUT('orderitem/12', { product: 5, quantity: 1 }).respond('OK');

      Cart.setQuantity({ id: 5, price: 35 }, 1)
      .then(() => {
        expect(Cart.getQuantity(5)).toEqual(1);
        expect(Cart.getTotals()).toEqual({ price: 35, quantity: 1 });
        done();
      });
      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });
});
