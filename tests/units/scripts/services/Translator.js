/**
* Translator tests
*/
describe('customerApp.service.Translator', () => {
  let Translator, $q, $rootScope, Lang, MultiTranslator, GoogleTranslator, Alert;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    // mock MultiTranslator
    $provide.factory('MultiTranslator', (_$q_) => ({
      _translateDeferred: _$q_.defer(),
      translate: jasmine.createSpy('translate').and.callFake(
        () => MultiTranslator._translateDeferred.promise
      ),
    }));

    // mock GoogleTranslator
    $provide.factory('GoogleTranslator', (_$q_) => ({
      _initializeDeferred: _$q_.defer(),
      initialize: jasmine.createSpy('initialize').and.callFake(
        () => GoogleTranslator._initializeDeferred.promise
      ),
    }));

    // Lang mock
    Lang = {
      getDefaultLanguage: jasmine.createSpy('getDefaultLanguage').and.callFake(() => $q.resolve('en')),
      getSelectedLanguage: jasmine.createSpy('getSelectedLanguage').and.callFake(() => $q.resolve('jp')),
    };

    // Alert mock
    Alert = {
      error: jasmine.createSpy('error'),
    };

    // provide mocks
    $provide.value('Lang', Lang);
    $provide.value('Alert', Alert);
  }));

  // inject services
  beforeEach(inject((_Translator_, _MultiTranslator_, _GoogleTranslator_, _$rootScope_, _$q_) => {
    $rootScope = _$rootScope_;
    $q = _$q_;

    // initialize Translator
    Translator = _Translator_;
    MultiTranslator = _MultiTranslator_;
    GoogleTranslator = _GoogleTranslator_;
  }));


  it('should be defined', () => {
    expect(Translator).toBeDefined();
  });

  describe('translate()', () => {
    it('should initialize GoogleTranslator', (done) => {
      MultiTranslator._translateDeferred.resolve([{ name: 'ケーキ' }, { name: 'お茶' }]);

      Translator.translate([{ name: 'Cake' }, { name: 'Tea' }], ['name'])
        .then(objects => {
          expect(Lang.getDefaultLanguage).toHaveBeenCalled();
          expect(Lang.getSelectedLanguage).toHaveBeenCalled();
          expect(Alert.error).not.toHaveBeenCalled();
          expect(MultiTranslator.translate).toHaveBeenCalledWith(
            [{ name: 'Cake' }, { name: 'Tea' }],
            ['name'],
            'en', 'jp'
          );
          expect(objects).toEqual([{ name: 'ケーキ' }, { name: 'お茶' }]);
          done();
        });

      $rootScope.$apply();
    });

    it('should handle translation error', (done) => {
      MultiTranslator._translateDeferred.reject();

      Translator.translate([{ name: 'Cake' }, { name: 'Tea' }], ['name'])
        .then(objects => {
          expect(Lang.getDefaultLanguage).toHaveBeenCalled();
          expect(Lang.getSelectedLanguage).toHaveBeenCalled();
          expect(Alert.error).toHaveBeenCalledWith('TRANSLATE.SERVICE_UNAVAILABLE');
          expect(MultiTranslator.translate).toHaveBeenCalledWith(
            [{ name: 'Cake' }, { name: 'Tea' }],
            ['name'],
            'en', 'jp'
          );
          expect(objects).toEqual([{ name: 'Cake' }, { name: 'Tea' }]);
          done();
        });

      $rootScope.$apply();
    });
  });

  describe('initialize()', () => {
    it('should correctly translate objects', (done) => {
      GoogleTranslator._initializeDeferred.resolve();

      Translator.initialize()
        .then(() => {
          expect(GoogleTranslator.initialize).toHaveBeenCalled();
          expect(Alert.error).not.toHaveBeenCalled();
          done();
        });

      $rootScope.$apply();
    });

    it('should handle initialization error', (done) => {
      GoogleTranslator._initializeDeferred.reject();

      Translator.initialize()
        .then(() => {
          expect(GoogleTranslator.initialize).toHaveBeenCalled();
          expect(Alert.error).toHaveBeenCalledWith('TRANSLATE.SERVICE_UNAVAILABLE');
          done();
        });

      $rootScope.$apply();
    });
  });
});
