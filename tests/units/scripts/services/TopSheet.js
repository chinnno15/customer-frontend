/**
* TopSheet tests
*/
describe('customerApp.service.TopSheet', () => {
  let $rootScope;
  let $animate;
  let localStorageService;
  let TopSheet;

  // load ngAnimateMock module to mock $animate
  beforeEach(angular.mock.module('ngAnimateMock'));

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide, $controllerProvider) => {
    // mock localStorageService
    localStorageService = {
      _store: {},
      set: jasmine.createSpy('set').and.callFake((k, v) => localStorageService._store[k] = v),
      get: jasmine.createSpy('get').and.callFake((k) => localStorageService._store[k]),
    };
    $provide.value('localStorageService', localStorageService);
    $controllerProvider.register('topSheetController', function() {
      return this;
    });
  }));

  // inject services
  beforeEach(angular.mock.inject((_$rootScope_, _$animate_, _TopSheet_) => {
    $rootScope = _$rootScope_;
    $animate = _$animate_;
    TopSheet = _TopSheet_;

    // remove topsheet el
    const $topsheetEl = angular.element(document.querySelector('.topsheet'));

    if ($topsheetEl.length) {
      $topsheetEl.remove();
    }
  }));

  describe('show()', () => {
    it('should append .topsheet to DOM', () => {
      TopSheet.show({
        template: '<div class="template"></div>',
        controller: 'topSheetController'
      });
      const $el = angular.element(document.querySelector('.topsheet'));
      expect($el.length).toEqual(1);
    });

    it('should append .topsheet to DOM along with template', () => {
      TopSheet.show({
        template: '<div class="template"></div>',
        controller: 'topSheetController'
      });
      const $template = angular.element(document.querySelector('.topsheet .template'));
      expect($template.length).toEqual(1);
    });
  });

  describe('close()', () => {
    it('should remove .topsheet from DOM', () => {
      TopSheet.show({
        template: '<div class="template"></div>',
        controller: 'topSheetController'
      })
      .then(()=>{
        let $el = angular.element(document.querySelector('.topsheet'));
        expect($el.length).toEqual(1);

        TopSheet.close();
        $rootScope.$apply();
        $animate.closeAndFlush();

        $el = angular.element(document.querySelector('.topsheet'));
        expect($el.length).toEqual(0);
      });
    });
  });

  describe('isOpen()', () => {
    it('should return true when topsheet is open.', () => {
      TopSheet.show({
        template: '<div class="template"></div>',
        controller: 'topSheetController'
      })
      .then(() => {
        expect(TopSheet.isOpen()).toBeTrue();
      });
    });
  });
});
