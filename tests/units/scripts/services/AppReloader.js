/**
* AppReloader tests
*/
describe('customerApp.services.AppReloader', () => {
  let AppReloader;
  let TopSheet = {};
  let $httpBackend;
  let $q;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    $provide.value('TopSheet', TopSheet);
  }));

  // inject services
  beforeEach(angular.mock.inject((_AppReloader_, _$httpBackend_, _$q_) => {
    AppReloader = _AppReloader_;
    $httpBackend = _$httpBackend_;
    $q = _$q_;

    // mock TopSheet service
    Object.assign(TopSheet, {
      _isOpen: false,
      _closeDeferred: $q.defer(),
      show: jasmine.createSpy('show'),
      close: jasmine.createSpy('close').and.callFake(() => TopSheet._closeDeferred.promise),
      isOpen: jasmine.createSpy('isOpen').and.callFake(() => TopSheet._isOpen),
    });

    AppReloader._appName = 'customer';
  }));

  describe('checkAppVersion()', () => {
    it('prompt user to reload when commit hash does not match', () => {
      $httpBackend.expectGET('serverinfo').respond({
        versions: {
          customer: 'commit hash'
        }
      });

      AppReloader.checkAppVersion();

      TopSheet._closeDeferred.resolve();

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();

      expect(TopSheet.show).toHaveBeenCalled();
    });
    it('do not prompt user to reload if commit hash is the same', () => {
      $httpBackend.expectGET('serverinfo').respond({
        versions: {
          customer: __VERSION__ // eslint-disable-line no-undef
        }
      });

      AppReloader.checkAppVersion();

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();

      expect(TopSheet.show).not.toHaveBeenCalled();
    });
  });
});
