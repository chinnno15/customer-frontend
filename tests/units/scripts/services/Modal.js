/**
* Modal tests
*/
describe('customerApp.service.Modal', () => {
  let $rootScope;
  let localStorageService;
  let Modal;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide, $controllerProvider) => {
    // mock localStorageService
    localStorageService = {
      _store: {},
      set: jasmine.createSpy('set').and.callFake((k, v) => localStorageService._store[k] = v),
      get: jasmine.createSpy('get').and.callFake((k) => localStorageService._store[k]),
    };
    $provide.value('localStorageService', localStorageService);
    // mock controller
    $controllerProvider.register('modalController', function() {
      return this;
    });
  }));

  // inject services
  beforeEach(angular.mock.inject((_$rootScope_, _Modal_) => {
    $rootScope = _$rootScope_;
    Modal = _Modal_;

    // remove modal el
    const $modalEl = angular.element(document.querySelector('.overlay.active'));
    if ($modalEl.length) {
      $modalEl.remove();
    }
  }));

  describe('show()', () => {
    it('.overlay.active should not exist in DOM', () => {
      const $el = angular.element(document.querySelector('.overlay.active'));
      expect($el.length).toEqual(0);
    });
    it('should append .overlay.active to DOM', () => {
      Modal.show({
        template: '<div class="template"></div>',
        controller: 'modalController'
      });
      const $el = angular.element(document.querySelector('.overlay.active'));
      expect($el.length).toEqual(1);
    });

    it('should append .overlay.active to DOM along with template', () => {
      Modal.show({
        template: '<div class="template"></div>',
        controller: 'modalController'
      });
      const $template = angular.element(document.querySelector('.overlay.active .template'));
      expect($template.length).toEqual(1);
    });
  });

  describe('close()', () => {
    it('should remove .overlay.active from DOM', () => {
      Modal.show({
        template: '<div class="template"></div>',
        controller: 'modalController'
      });

      let $el = angular.element(document.querySelector('.overlay'));
      expect($el.length).toEqual(1);

      Modal.close();
      $rootScope.$apply();

      expect(angular.element(document.querySelector('.overlay')).length).toEqual(0);
    });
  });
});
