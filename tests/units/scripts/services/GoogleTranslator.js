/**
* GoogleTranslator tests
*/
describe('customerApp.service.GoogleTranslator', () => {
  let GoogleTranslator, $q, $rootScope, gapi, GoogleApiLoader;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    // gapi mock
    gapi = {
      _response: null,
      client: {
        request: jasmine.createSpy('request').and.callFake(() => ({
          execute: cb => cb(gapi._response),
        }))
      }
    };

    // GoogleApiLoader mock
    GoogleApiLoader = {
      _isLoaded: false,
      load: jasmine.createSpy('load').and.callFake(() => {
        GoogleApiLoader._isLoaded = true;
        return $q.resolve(gapi);
      }),
      isLoaded: jasmine.createSpy('isLoaded').and.callFake(() => GoogleApiLoader._isLoaded),
    };

    // provide mocks
    $provide.value('GoogleApiLoader', GoogleApiLoader);
  }));

  // inject services
  beforeEach(inject((_GoogleTranslator_, _$rootScope_, _$q_) => {
    $rootScope = _$rootScope_;
    $q = _$q_;

    // initialize GoogleTranslator
    GoogleTranslator = _GoogleTranslator_;
  }));


  it('should be defined', () => {
    expect(GoogleTranslator).toBeDefined();
  });

  describe('translate()', () => {
    it('should queue requests when gapi is not loaded', (done) => {
      gapi._response = {
        data: { translations: ['ケーキ'] }
      };

      GoogleTranslator.translate('Cake', 'en', 'jp')
        .then(res => {
          expect(gapi.client.request).toHaveBeenCalledWith({
            path: '/language/translate/v2',
            method: 'GET',
            params: {
              q: 'Cake',
              source: 'en',
              target: 'jp',
            },
          });
          expect(res).toEqual(['ケーキ']);
          done();
        });

      $rootScope.$apply();
      expect(GoogleApiLoader.isLoaded).toHaveBeenCalled();
      expect(gapi.client.request).not.toHaveBeenCalled();

      GoogleTranslator.initialize();
      $rootScope.$apply();
    });

    it('should make requests when gapi is loaded', (done) => {
      gapi._response = {
        data: { translations: ['ケーキ'] }
      };

      GoogleTranslator.initialize();

      GoogleTranslator.translate('Cake', 'en', 'jp')
        .then(res => {
          expect(gapi.client.request).toHaveBeenCalledWith({
            path: '/language/translate/v2',
            method: 'GET',
            params: {
              q: 'Cake',
              source: 'en',
              target: 'jp',
            },
          });
          expect(res).toEqual(['ケーキ']);
          done();
        });

      $rootScope.$apply();
    });

    it('should translate multiple texts', (done) => {
      gapi._response = {
        data: { translations: ['ケーキ', 'お茶'] }
      };

      GoogleTranslator.initialize();

      GoogleTranslator.translate(['Cake', 'Tea'], 'en', 'jp')
        .then(res => {
          expect(gapi.client.request).toHaveBeenCalledWith({
            path: '/language/translate/v2',
            method: 'GET',
            params: {
              q: ['Cake', 'Tea'],
              source: 'en',
              target: 'jp',
            },
          });
          expect(res).toEqual(['ケーキ', 'お茶']);
          done();
        });

      $rootScope.$apply();
    });

    it('should respond with rejected promise when gapi request fails', (done) => {
      gapi._response = { error: 'SOME ERROR' };

      GoogleTranslator.initialize();

      GoogleTranslator.translate('Cake', 'en', 'jp')
        .catch(err => {
          expect(gapi.client.request).toHaveBeenCalledWith({
            path: '/language/translate/v2',
            method: 'GET',
            params: {
              q: 'Cake',
              source: 'en',
              target: 'jp',
            },
          });
          expect(err).toEqual('SOME ERROR');
          done();
        });

      $rootScope.$apply();
    });
  });

  describe('initialize()', () => {
    it('should load google API', () => {
      GoogleTranslator.initialize();
      $rootScope.$apply();

      expect(GoogleApiLoader.load).toHaveBeenCalled();
    });
  });
});
