/**
* StripePayment tests
*/
describe('customerApp.services.StripePayment', () => {
  let StripePayment, PaymentDAO = {}, StripeCheckout = {}, $rootScope, $interval, $q;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    // provide mocks
    $provide.value('PaymentDAO', PaymentDAO);
    $provide.value('StripeCheckout', StripeCheckout);
  }));

  // inject services
  beforeEach(inject((_StripePayment_, _$rootScope_, _$interval_, _$q_) => {
    $q = _$q_;
    $interval = _$interval_;
    $rootScope = _$rootScope_;
    $rootScope.$emit = jasmine.createSpy('$emit').and.callThrough();

    // mock PaymentDAO service
    PaymentDAO._getStripeKeyDeferred = $q.defer();
    PaymentDAO.getStripeKey = jasmine.createSpy('getStripeKey').and.callFake(function() {
      return this._getStripeKeyDeferred.promise;
    });

    // mock StripeCheckout service
    StripeCheckout.open = jasmine.createSpy('open').and.callFake(() => {
      return $q.resolve([{ id: 'test_token' }]);
    });
    StripeCheckout.configure = jasmine.createSpy('configure').and.callFake(() => ({
      open: StripeCheckout.open
    }));

    // initialize StripePayment
    StripePayment = _StripePayment_;
  }));


  it('should be defined', () => {
    expect(StripePayment).toBeDefined();
  });

  it('should respond with error when initialization fails', (done) => {
    PaymentDAO._getStripeKeyDeferred.reject({ data: { detail: 'some error' }});
    StripePayment.initializeStripe()
    .catch((err) => {
      expect(err).toEqual(StripePayment.ERRORS.FAIL);
      expect(PaymentDAO.getStripeKey).toHaveBeenCalled();
      expect($rootScope.$emit).not.toHaveBeenCalled();
      done();
    });
    // propagate promises resolutions
    $rootScope.$apply();
  });

  it('should initialize correctly when key is provided', (done) => {
    PaymentDAO._getStripeKeyDeferred.resolve('test_key_123');
    StripePayment.initializeStripe()
    .then(() => {
      expect(StripePayment.key).toEqual('test_key_123');
      expect(PaymentDAO.getStripeKey).toHaveBeenCalled();
      expect($rootScope.$emit).not.toHaveBeenCalled();
      done();
    });
    // propagate promises resolutions
    $rootScope.$apply();
  });

  it('should emit an event when key changed', (done) => {
    StripePayment.key = 'test_key_123';
    PaymentDAO._getStripeKeyDeferred.resolve('test_key_456');
    StripePayment.initializeStripe()
    .then(() => {
      expect(StripePayment.key).toEqual('test_key_456');
      expect(PaymentDAO.getStripeKey).toHaveBeenCalled();
      expect($rootScope.$emit).toHaveBeenCalledWith('swish:stripe-key-changed');
      done();
    });
    // propagate promises resolutions
    $rootScope.$apply();
  });

  it('should periodicly check for key changes', () => {
    PaymentDAO._getStripeKeyDeferred.resolve('test_key_456');
    StripePayment.initializeStripe();
    $rootScope.$apply();

    expect(PaymentDAO.getStripeKey.calls.count()).toEqual(1);
    expect($rootScope.$emit).not.toHaveBeenCalled();

    PaymentDAO._getStripeKeyDeferred = $q.defer();
    $interval.flush(30 * 1000);
    PaymentDAO._getStripeKeyDeferred.resolve('test_key_456');
    // $rootScope.$apply();

    expect(PaymentDAO.getStripeKey.calls.count()).toEqual(2);
    expect($rootScope.$emit).not.toHaveBeenCalled();
  });

  it('should respond with error when trying to checkout before initialization', (done) => {
    StripePayment.checkout(1000, 'USD')
    .catch((err) => {
      expect(err).toEqual(StripePayment.ERRORS.NO_KEY);
      done();
    });
    // propagate promises resolutions
    $rootScope.$apply();
  });

  it('should correctly complete a checkout', (done) => {
    PaymentDAO._getStripeKeyDeferred.resolve('test_key_123');
    StripePayment.initializeStripe()
    .then(() => StripePayment.checkout(1000, 'USD'))
    .then((token) => {
      expect(token).toEqual('test_token');
      expect(StripeCheckout.configure).toHaveBeenCalledWith({
        name: 'Checkout',
        key: 'test_key_123',
      });
      expect(StripeCheckout.open).toHaveBeenCalledWith({
        amount: 1000,
        currency: 'USD',
      });
      done();
    });
    // propagate promises resolutions
    $rootScope.$apply();
  });
});
