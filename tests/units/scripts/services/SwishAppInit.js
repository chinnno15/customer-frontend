/**
* SwishAppInit tests
*/
describe('customerApp.services.SwishAppInit', () => {
  let SwishAppInit;
  let $state;
  let $timeout;
  let $window;
  let $rootScope;
  let OrderPointStore;
  let OrderPointDAO;
  let StripePayment;
  let Alert;
  let Company;
  let STATES;
  let EVENTS;
  let Translator;
  let Cart;
  let I18n;
  let localStorageService;

  // load the module
  beforeEach(angular.mock.module('customerApp.services', ($provide) => {
    // create $state mock
    $state = {
      go: jasmine.createSpy('go')
    };

    // create $window mock
    $window = {
      location: {
        reload: jasmine.createSpy('reload')
      },
      addEventListener: jasmine.createSpy('addEventListener')
    };

    // create Alert mock
    Alert = {
      error: jasmine.createSpy('error'),
      info: jasmine.createSpy('info')
    };

    // mock localStorageService
    localStorageService = {
      _store: {},
      set: jasmine.createSpy('set').and.callFake((k, v) => localStorageService._store[k] = v),
      get: jasmine.createSpy('get').and.callFake((k) => localStorageService._store[k]),
    };

    // create Cart mock
    $provide.factory('Cart', (_$q_) => ({
      _initializeDeferred: _$q_.defer(),
      initialize: jasmine.createSpy('initialize').and.callFake(() => {
        return Cart._initializeDeferred.promise;
      }),
      deleteAllItems: jasmine.createSpy('deleteAllItems'),
      reload: jasmine.createSpy('reload'),
    }));

    // create OrderPointStore mock
    $provide.factory('OrderPointStore', (_$q_) => ({
      _registerDeferred: _$q_.defer(),
      register: jasmine.createSpy('register').and.callFake(() => OrderPointStore._registerDeferred.promise),

      _isAvailableDeferred: _$q_.defer(),
      isAvailable: jasmine.createSpy('isAvailable').and.callFake(() => OrderPointStore._isAvailableDeferred.promise),
    }));

    $provide.factory('OrderPointDAO', (_$q_) => ({
      _getOrderPointDeferred: _$q_.defer(),
      getOrderPoint: jasmine.createSpy('getOrderPoint').and.callFake(() => OrderPointDAO._getOrderPointDeferred.promise)
    }));

    // mock StripePayment service
    $provide.factory('StripePayment', (_$q_) => ({
      _initializeStripe: _$q_.defer(),
      initializeStripe: jasmine.createSpy('initializeStripe').and.callFake(() => {
        return StripePayment._initializeStripe.promise;
      }),
      ERRORS: { NO_KEY: 'NO_KEY' },
    }));

    $provide.factory('Company', (_$q_) => ({
      _initializeDeferred: _$q_.defer(),
      initialize: jasmine.createSpy('initialize').and.callFake(() => {
        return Company._initializeDeferred.promise;
      }),
    }));

    $provide.factory('GoogleApiLoader', (_$q_) => ({
      load: jasmine.createSpy('load').and.callFake(() => {
        return _$q_.defer().promise;
      }),
    }));

    $provide.factory('Translator', (_$q_) => ({
      initialize: jasmine.createSpy('initialize').and.callFake(() => {
        return _$q_.defer().promise;
      }),
    }));

    $provide.factory('I18n', () => ({
      load: jasmine.createSpy('I18n#load')
    }));

    $provide.value('$state', $state);
    $provide.value('$window', $window);
    $provide.value('Alert', Alert);
    $provide.value('localStorageService', localStorageService);
  }));

  // inject services
  beforeEach(inject((_SwishAppInit_, _$rootScope_, _$timeout_, _OrderPointStore_, _StripePayment_, _Cart_, _Company_, _STATES_, _EVENTS_, _Translator_, _I18n_, _OrderPointDAO_) => {
    $rootScope = _$rootScope_;
    $timeout = _$timeout_;
    OrderPointStore = _OrderPointStore_;
    OrderPointDAO = _OrderPointDAO_;
    StripePayment = _StripePayment_;
    Cart = _Cart_;
    Company = _Company_;
    STATES = _STATES_;
    EVENTS = _EVENTS_;
    Translator = _Translator_;
    I18n = _I18n_;

    // initialize SwishAppInit
    SwishAppInit = _SwishAppInit_;
  }));

  it('should be defined', () => {
    expect(SwishAppInit).toBeDefined();
  });

  it('should register order point correctly', () => {
    SwishAppInit.registerOrderPoint('a-store', 'an-orderpoint');

    OrderPointStore._registerDeferred.resolve();
    Company._initializeDeferred.resolve();
    Cart._initializeDeferred.resolve();
    StripePayment._initializeStripe.resolve();

    $rootScope.$apply();

    expect(OrderPointStore.register).toHaveBeenCalledWith('a-store', 'an-orderpoint');
    expect(Cart.initialize).toHaveBeenCalledWith(false);
    expect(Cart.deleteAllItems).toHaveBeenCalled();
    expect(StripePayment.initializeStripe).toHaveBeenCalled();
    expect(Alert.error).not.toHaveBeenCalled();
  });

  it('should go to menu if current state is main', () => {
    $state.current = {
      name: STATES.MAIN
    };
    SwishAppInit.registerOrderPoint('a-store', 'an-orderpoint');

    OrderPointStore._registerDeferred.resolve();
    Company._initializeDeferred.resolve();
    Cart._initializeDeferred.resolve();
    StripePayment._initializeStripe.resolve();

    $rootScope.$apply();

    expect($state.go).toHaveBeenCalledWith(STATES.MENU);
  });

  it('should not clean cart when order point same previous', () => {
    localStorageService.set('storeName', 'a-store');
    localStorageService.set('orderPointName', 'an-orderpoint');
    SwishAppInit.registerOrderPoint('a-store', 'an-orderpoint');

    OrderPointStore._registerDeferred.resolve();
    Company._initializeDeferred.resolve();
    Cart._initializeDeferred.resolve();
    StripePayment._initializeStripe.resolve();

    $rootScope.$apply();

    expect(Cart.initialize).toHaveBeenCalledWith(true);
    expect(Cart.deleteAllItems).not.toHaveBeenCalled();
  });

  it('should redirect to NO_ORDERPOINT on any state transition when registration fails', () => {
    SwishAppInit.registerOrderPoint('a-store', 'an-orderpoint');

    OrderPointStore._registerDeferred.reject();
    $rootScope.$apply();

    expect(OrderPointStore.register).toHaveBeenCalledWith('a-store', 'an-orderpoint');
    expect($state.go).toHaveBeenCalledWith(STATES.NO_ORDERPOINT);
  });

  it('should display error on any state transition when Company init fails', () => {
    SwishAppInit.registerOrderPoint('a-store', 'an-orderpoint');

    Company._initializeDeferred.reject();
    OrderPointStore._registerDeferred.resolve();
    Cart._initializeDeferred.resolve();
    StripePayment._initializeStripe.resolve();

    $rootScope.$apply();

    expect(OrderPointStore.register).toHaveBeenCalled();
    expect(Cart.initialize).toHaveBeenCalledWith(false);
    expect(StripePayment.initializeStripe).toHaveBeenCalled();
    expect(Alert.error).toHaveBeenCalledWith('GENERAL.INIT_FAILED');
  });

  it('should display error on any state transition when stripe init fails', () => {
    SwishAppInit.registerOrderPoint('a-store', 'an-orderpoint');

    OrderPointStore._registerDeferred.resolve();
    Company._initializeDeferred.resolve();
    Cart._initializeDeferred.resolve();
    StripePayment._initializeStripe.reject();

    $rootScope.$apply();

    expect(OrderPointStore.register).toHaveBeenCalled();
    expect(Cart.initialize).toHaveBeenCalledWith(false);
    expect(StripePayment.initializeStripe).toHaveBeenCalled();
    expect(Alert.error).toHaveBeenCalledWith('GENERAL.INIT_FAILED');
  });

  it('should display error on any state transition when Cart init fails', () => {
    SwishAppInit.registerOrderPoint('a-store', 'an-orderpoint');

    OrderPointStore._registerDeferred.resolve();
    Company._initializeDeferred.resolve();
    Cart._initializeDeferred.reject();
    StripePayment._initializeStripe.resolve();

    OrderPointStore._registerDeferred.resolve();
    $rootScope.$apply();

    expect(OrderPointStore.register).toHaveBeenCalled();
    expect(Company.initialize).toHaveBeenCalled();
    expect(Cart.initialize).toHaveBeenCalledWith(false);
    expect(Cart.deleteAllItems).not.toHaveBeenCalled();
    expect(StripePayment.initializeStripe).toHaveBeenCalled();
    expect(Alert.error).toHaveBeenCalledWith('GENERAL.INIT_FAILED');
  });

  it('should prepare Translator for using', () => {
    SwishAppInit.initialize();
    expect(Translator.initialize).toHaveBeenCalled();
  });

  it('should reload cart on menu language change', () => {
    SwishAppInit.initialize();
    expect(Cart.reload).not.toHaveBeenCalled();
    $rootScope.$emit(EVENTS.MENU_LANGUAGE_CHANGED);
    $rootScope.$apply();

    expect(Cart.reload).toHaveBeenCalled();
  });

  it('should load I18n and default language', () => {
    SwishAppInit.initialize();

    expect(I18n.load).toHaveBeenCalledWith('en');
  });

  it('should reload the app when Stripe key change event occurs', () => {
    SwishAppInit.initialize();

    $rootScope.$emit(EVENTS.STRIPE_KEY_CHANGED);
    expect(Alert.info).toHaveBeenCalledWith('GENERAL.STRIPE_KEY_CHANGED');

    expect($window.location.reload).not.toHaveBeenCalled();
    $timeout.flush();
    expect($window.location.reload).toHaveBeenCalled();
    $timeout.verifyNoPendingTasks();
  });

  describe('compatible with old orderpoint url', () => {
    it('should compatible with old orderpoint url', () => {
      SwishAppInit.initialize();

      OrderPointDAO._getOrderPointDeferred.resolve({
        store_name: 'store-1',
        orderpoint_name: 'orderpoint-a'
      });

      const toState = { name: STATES.ORDER_POINT };
      const toParams = { orderPointId: 'interesting-name-bbars-55dc44b4-9508-430a-9545-dc21c0b4c36e' };
      $rootScope.$broadcast('$stateChangeStart', toState, toParams);
      $rootScope.$apply();

      expect(OrderPointDAO.getOrderPoint).toHaveBeenCalledWith('55dc44b4-9508-430a-9545-dc21c0b4c36e');
      expect($state.go).toHaveBeenCalledWith(STATES.MAIN, {
        storeName: 'store-1',
        orderPointName: 'orderpoint-a'
      });
    });

    it('should redirect to no orderpoint state if orderpoint uuid not found', () => {
      SwishAppInit.initialize();

      OrderPointDAO._getOrderPointDeferred.reject();

      const toState = { name: STATES.ORDER_POINT };
      const toParams = { orderPointId: 'interesting-name-bbars-55dc44b4-9508-430a-9545-dc21c0b4c36e' };
      $rootScope.$broadcast('$stateChangeStart', toState, toParams);
      $rootScope.$apply();

      expect(OrderPointDAO.getOrderPoint).toHaveBeenCalledWith('55dc44b4-9508-430a-9545-dc21c0b4c36e');
      expect($state.go).toHaveBeenCalledWith(STATES.NO_ORDERPOINT);
    });
  });
});
