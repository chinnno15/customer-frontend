/**
* customerApp configuration tests
*/
describe('states', () => {
  let $state;

  // load the module
  beforeEach(angular.mock.module('customerApp'));

  // run injector
  beforeEach(inject((_$state_) => {
    $state = _$state_;
  }));

  describe('orderPoint', () => {
    it('should respond to URL', () => {
      expect($state.href('orderpoint', { orderPointId: 'uuid1234' })).toEqual('#/orderpoint/uuid1234');
    });
  });

  describe('main', () => {
    it('should respond to URL', () => {
      expect($state.href('main', {
        storeName: 'store',
        orderPointName: 'order'
      }))
        .toEqual('#/store/order');
    });
  });

  describe('main.menu', () => {
    it('should respond to URL', () => {
      expect($state.href('main.menu')).toMatch('/menu/');
      expect($state.href('main.menu', { categoryId: 1 })).toMatch('/menu\/1');
    });
  });

  describe('main.orders', () => {
    it('should respond to URL', () => {
      expect($state.href('main.orders')).toMatch('/orders');
    });
  });

  describe('main.order', () => {
    it('should respond to URL', () => {
      expect($state.href('main.order', { orderId: 1 })).toMatch('/order\/1');
    });
  });

  describe('main.cart', () => {
    it('should respond to URL', () => {
      expect($state.href('main.cart')).toMatch('/cart');
    });
  });

  describe('main.receipt', () => {
    it('should respond to URL', () => {
      expect($state.href('main.receipt')).toMatch('/receipt/');
    });
    it('should respond to URL with param', () => {
      expect($state.href('main.receipt', { orderId: 1 })).toMatch('/receipt\/1');
    });
  });

  describe('main.about', () => {
    it('should respond to URL', () => {
      expect($state.href('main.about')).toMatch('/about');
    });
  });

  describe('main.terms_of_use', () => {
    it('should respond to URL', () => {
      expect($state.href('main.terms_of_use')).toMatch('/terms-of-use');
    });
  });

  describe('main.payment_error', () => {
    it('should respond to URL', () => {
      expect($state.href('main.payment_error')).toMatch('/payment-error');
    });
  });
});
