/**
* CallWaiterController tests
*/
describe('customerApp.controllers.CallWaiterController', () => {
  let CallWaiterController;
  let Alert;
  let $scope;
  let $controller;
  let CallWaiter;
  let localStorageService;

  // load the module
  beforeEach(angular.mock.module('customerApp.controllers', ($provide) => {
    // mock localStorageService
    localStorageService = {
      _store: {},
      set: jasmine.createSpy('set').and.callFake((k, v) => localStorageService._store[k] = v),
      get: jasmine.createSpy('get').and.callFake((k) => localStorageService._store[k]),
      remove: jasmine.createSpy('remove').and.callFake((k) => delete localStorageService._store[k]),
      keys: jasmine.createSpy('keys').and.callFake(() => Object.keys(localStorageService._store))
    };
    $provide.value('localStorageService', localStorageService);
  }));

  // Initialize the controller and mocks
  beforeEach(inject((_$controller_, $q, _$rootScope_) => {
    $scope = _$rootScope_.$new();
    $controller = _$controller_;

    // create CallWaiter mock
    CallWaiter = {
      _wasCallMade: false,
      wasCallMade: jasmine.createSpy('wasCallMade').and.callFake(() => CallWaiter._wasCallMade),
      _callWaiterDeferred: $q.defer(),
      callWaiter: jasmine.createSpy('callWaiter').and.callFake(() => CallWaiter._callWaiterDeferred.promise),
      init: jasmine.createSpy('init'),
    };

    // create Alert mock
    Alert = {
      error: jasmine.createSpy('error'),
      success: jasmine.createSpy('success')
    };
  }));

  it('should not call waiter when initializing', () => {
    // initialize CallWaiterController
    CallWaiterController = $controller('CallWaiterController', {
      Alert,
      CallWaiter,
      $scope
    });

    $scope.$apply();
    expect(CallWaiter.callWaiter).not.toHaveBeenCalled();
  });

  describe('callWaiter()', () => {
    beforeEach(() => {
      // initialize CallWaiterController
      CallWaiterController = $controller('CallWaiterController', {
        Alert,
        CallWaiter,
        $scope
      });

      CallWaiterController.callWaiter();

      expect(CallWaiter.callWaiter).toHaveBeenCalled();
    });

    it('should display confirmation alert', () => {
      expect(Alert.success).toHaveBeenCalledWith('CALL_WAITER.FINDING');
    });
  });
});
