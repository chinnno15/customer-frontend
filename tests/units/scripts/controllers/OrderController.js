/**
* OrderController tests
*/
describe('customerApp.services.OrdersStore', () => {
  let $controller;
  let $httpBackend;
  let $q;
  let $rootScope;
  let $scope;
  let $stateParams;
  let Alert;
  let OrderController;
  let OrderDAO;
  let OrderItemDAO;
  let Translator;

  // load the module
  beforeEach(angular.mock.module('customerApp.controllers', 'customerApp.dao', ($provide) => {
    // mock Alert
    Alert = {
      error: jasmine.createSpy('error')
    };
    $provide.value('Alert', Alert);

    Translator = { translate: arr => arr };
    $provide.value('Translator', Translator);
  }));

  // inject services
  beforeEach(inject((_$q_, _$rootScope_, _$httpBackend_, _$controller_, _OrderItemDAO_) => {
    $q = _$q_;
    $rootScope = _$rootScope_;
    $rootScope.$emit = jasmine.createSpy('$emit');
    $httpBackend = _$httpBackend_;
    $controller = _$controller_;
    $scope = $rootScope.$new();
    $stateParams = {};
    OrderItemDAO = _OrderItemDAO_;

    // mock OrderDAO
    OrderDAO = {
      _getOrderDeferred: $q.defer(),
      _sendReceiptByEmailDeferred: $q.defer(),
      _getReceiptUrlDeferred: $q.defer(),
      getOrder: jasmine.createSpy('getOrder').and.callFake(() => OrderDAO._getOrderDeferred.promise),
      sendReceiptByEmail: jasmine.createSpy('sendReceiptByEmail').and.callFake(() => OrderDAO._sendReceiptByEmailDeferred.promise),
      getReceiptUrl: jasmine.createSpy('getReceiptUrl').and.callFake(() => OrderDAO._getReceiptUrlDeferred.promise),
    };

    OrderController = $controller('OrderController', {
      $scope,
      Alert,
      $stateParams,
      OrderDAO,
      OrderItemDAO,
    });
  }));


  describe('setRating()', () => {
    it('should update order item rating', () => {
      const orderItem = {
        id: 27,
        product: 5
      };
      const newRating = 3;

      OrderController.setRating(newRating, orderItem);

      $httpBackend.expectPUT('orderitem/27', { product: 5, rating: newRating }).respond('OK');

      $httpBackend.flush();
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });
});
