/**
 * TranslateController tests
 */
describe('customerApp.controllers.TranslateController', () => {
  let $rootScope, $state, STATES, gettextCatalog, LanguagesDAO, Lang, Alert, TranslateController;


  beforeEach(angular.mock.module('customerApp.controllers'));

  beforeEach(inject(($controller, $q, _$rootScope_, _STATES_) => {
    $rootScope = _$rootScope_;
    STATES = _STATES_;

    Lang = {
      getDefaultLanguage: jasmine.createSpy('getDefaultLanguage').and.callFake(() => $q.resolve('en')),
      setSelectedLanguage: jasmine.createSpy('setSelectedLanguage').and.callFake(() => $q.resolve('en')),
      getSelectedLanguage: jasmine.createSpy('getSelectedLanguage').and.callFake(() => $q.resolve('en')),
    };

    LanguagesDAO = {
      listGoogleTranslateLanguages: jasmine.createSpy('listGoogleTranslateLanguages').and.callFake(() => $q.resolve({
        'en-US': {
          code: 'en-US',
          localized_title: 'English',
          title: 'English'
        },
        ru: {
          code: 'ru',
          localized_title: 'Русский',
          title: 'Russian'
        }
      }))
    };

    $state = {
      go: jasmine.createSpy('go')
    };

    // create gettextCatalog mock
    gettextCatalog = {
      getString: jasmine.createSpy('gettextCatalog#getString').and.callFake(str => str),
    };

    TranslateController = $controller('TranslateController', {
      $state,
      $rootScope,
      gettextCatalog,
      Lang,
      LanguagesDAO,
      Alert,
      STATES,
    });

    $rootScope.$apply();
  }));


  it('should initialize controller correctly', () => {
    expect(TranslateController).toBeDefined();
    expect(LanguagesDAO.listGoogleTranslateLanguages).toHaveBeenCalled();
    expect(Lang.getSelectedLanguage).toHaveBeenCalled();
    expect(TranslateController.languages).toEqual({
      'en-US': {
        code: 'en-US',
        localized_title: 'English',
        title: 'English'
      },
      ru: {
        code: 'ru',
        localized_title: 'Русский',
        title: 'Russian'
      }
    });
    expect(TranslateController.selectedLanguage).toEqual('en-US');
  });


  describe('dontTranslate function', () => {
    it('should be defined', () => {
      expect(TranslateController.dontTranslate).toBeDefined();
    });

    it('should reset selected language', () => {
      TranslateController.selectedLanguage = 'ru';
      TranslateController.dontTranslate();
      $rootScope.$apply();

      expect(Lang.getDefaultLanguage).toHaveBeenCalled();
      expect(Lang.setSelectedLanguage).toHaveBeenCalledWith('en');
      expect($state.go).toHaveBeenCalledWith(STATES.MENU);
    });
  });


  describe('changeLanguage function', () => {
    it('should be defined', () => {
      expect(TranslateController.changeLanguage).toBeDefined();
    });

    it('should save selected language', () => {
      TranslateController.selectedLanguage = 'ru';
      TranslateController.changeLanguage();

      expect(Lang.setSelectedLanguage).toHaveBeenCalledWith('ru');
      expect($state.go).toHaveBeenCalledWith(STATES.MENU);
    });
  });

  describe('setSelectedLanguage function', () => {
    it('should be defined', () => {
      expect(TranslateController.setSelectedLanguage).toBeDefined();
    });

    it('should set language to nearest prefix match', () => {
      TranslateController.setSelectedLanguage('en');
      expect(TranslateController.selectedLanguage).toEqual('en-US');
    });
  });
});
