/**
* ToggleController tests
*/
describe('customerApp.controllers.ToggleController', () => {
  let ToggleController, scope;

  // load the module
  beforeEach(angular.mock.module('customerApp.controllers'));

  // Initialize the controller and mocks
  beforeEach(inject(($controller, $rootScope) => {
    // create new scope
    scope = $rootScope.$new();
    // initialize ToggleController
    ToggleController = $controller('ToggleController', {
      $scope: scope
    });
  }));


  it('should be defined', () => {
    expect(ToggleController).toBeDefined();
  });

  it('should initialize controller correctly', () => {
    expect(ToggleController.isOpen).toEqual(false);
  });

  it('should correctly toggle isOpen state', () => {
    ToggleController.toggle();
    expect(ToggleController.isOpen).toEqual(true);
    ToggleController.toggle();
    expect(ToggleController.isOpen).toEqual(false);
  });

  it('should correctly set isOpen to false on stage change event', () => {
    // make sure isOpen is true
    ToggleController.toggle();
    expect(ToggleController.isOpen).toEqual(true);
    // broadcast state change event
    scope.$broadcast('$stateChangeStart');
    // assert isOpen is false
    expect(ToggleController.isOpen).toEqual(false);
  });
});
