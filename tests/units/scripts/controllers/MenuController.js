/**
* MenuController tests
*/
describe('customerApp.controllers.MenuController', () => {
  let $controller, CategoryDAO, ProductDAO, Alert, $rootScope, state;

  // load the module
  beforeEach(angular.mock.module('customerApp.controllers', ($provide) => {
    state = {
      go: jasmine.createSpy('state.go')
    };

    $provide.value('$state', state);
  }));

  // Initialize the controller and mocks
  beforeEach(inject((_$controller_, $q, _$rootScope_) => {
    $rootScope = _$rootScope_;
    $controller = _$controller_;

    // create CategoryDAO mock
    CategoryDAO = {
      _listCategoriesDeferred: $q.defer(),
      listCategories: jasmine.createSpy('listCategories').and.callFake(() => {
        return CategoryDAO._listCategoriesDeferred.promise;
      }),
      getCategory: jasmine.createSpy('getCategory').and.callFake(() => $q.resolve({
        id: 999,
        name: 'Nuts',
      }))
    };

    // create ProductDAO mock
    ProductDAO = {
      _listProductsDeferred: $q.defer(),
      listProducts: jasmine.createSpy('listProducts').and.callFake(() => {
        return ProductDAO._listProductsDeferred.promise;
      }),
    };

    // create Alert mock
    Alert = {
      error: jasmine.createSpy('error')
    };
  }));
  // DISABLED terms of use
  it('should show Disclaimer Beta Test dialog when customer opens first time', () => {
    // initialize MenuController
    let MenuController = $controller('MenuController', {
      CategoryDAO,
      ProductDAO,
      Alert,
      $stateParams: {}
    });

    expect(MenuController).toBeDefined();
  });

  it('should not show Disclaimer Beta Test dialog when customer opens not first time', () => {
    let MenuController = $controller('MenuController', {
      CategoryDAO,
      ProductDAO,
      Alert,
      $stateParams: {}
    });

    expect(MenuController).toBeDefined();
  });

  it('should initialize correctly for top level categories', () => {
    // initialize MenuController
    let MenuController = $controller('MenuController', {
      CategoryDAO,
      ProductDAO,
      Alert,
      $stateParams: {},
    });

    expect(MenuController).toBeDefined();

    CategoryDAO._listCategoriesDeferred.resolve([
      { id: 123, name: 'Desert', products: [{ name: 'Ice Cream' }, { name: 'Cake' }], sorting: 2 },
      { id: 456, name: 'Appetizer', products: [], sorting: 1 },
    ]);
    ProductDAO._listProductsDeferred.resolve([
      { id: 789, name: 'Ice on fire', sorting: 0 },
    ]);

    // propagate promises resolutions
    $rootScope.$apply();

    expect(CategoryDAO.getCategory).not.toHaveBeenCalled();
    expect(CategoryDAO.listCategories).toHaveBeenCalled();
    expect(ProductDAO.listProducts).toHaveBeenCalled();

    expect(MenuController.categoriesCount).toEqual(2);
    expect(MenuController.productsCount).toEqual(1);

    expect(MenuController.category).toBeUndefined();

    expect(MenuController.items).toEqual([
      { id: 789, name: 'Ice on fire', sorting: 0 },
      { id: 456, name: 'Appetizer', products: [], sorting: 1 },
      { id: 123, name: 'Desert', products: [{ name: 'Ice Cream' }, { name: 'Cake' }], sorting: 2 },
    ]);

    expect(Alert.error).not.toHaveBeenCalled();
  });

  it('should initialize correctly for subcategories', () => {
    // initialize MenuController
    let MenuController = $controller('MenuController', {
      CategoryDAO,
      ProductDAO,
      Alert,
      $stateParams: { categoryId: 3 },
    });

    expect(MenuController).toBeDefined();

    CategoryDAO._listCategoriesDeferred.resolve([
      { id: 123, name: 'Desert', products: [{ name: 'Ice Cream' }, { name: 'Cake' }], sorting: 2 },
      { id: 456, name: 'Appetizer', products: [], sorting: 1 },
    ]);
    ProductDAO._listProductsDeferred.resolve([
      { id: 789, name: 'Ice on fire', sorting: 0 },
    ]);

    // propagate promises resolutions
    $rootScope.$apply();

    expect(CategoryDAO.getCategory).toHaveBeenCalledWith(3);
    expect(CategoryDAO.listCategories).toHaveBeenCalledWith(3);
    expect(ProductDAO.listProducts).toHaveBeenCalledWith(3);

    expect(MenuController.categoriesCount).toEqual(2);
    expect(MenuController.productsCount).toEqual(1);

    expect(MenuController.category).toEqual({ id: 999, name: 'Nuts' });

    expect(MenuController.items).toEqual([
      { id: 789, name: 'Ice on fire', sorting: 0 },
      { id: 456, name: 'Appetizer', products: [], sorting: 1 },
      { id: 123, name: 'Desert', products: [{ name: 'Ice Cream' }, { name: 'Cake' }], sorting: 2 },
    ]);

    expect(Alert.error).not.toHaveBeenCalled();
  });

  it('should display error message when fetching categories fails', () => {
    // initialize MenuController
    let MenuController = $controller('MenuController', {
      CategoryDAO,
      ProductDAO,
      Alert,
      $stateParams: {},
    });

    CategoryDAO._listCategoriesDeferred.reject();
    ProductDAO._listProductsDeferred.resolve([]);

    // propagate promises resolutions
    $rootScope.$apply();

    expect(CategoryDAO.listCategories).toHaveBeenCalled();
    expect(ProductDAO.listProducts).toHaveBeenCalled();
    expect(MenuController.categories).toBeUndefined();
    expect(MenuController.products).toBeUndefined();
    expect(Alert.error).toHaveBeenCalledWith('MENU.DATA_FAILED');
  });

  it('should display error message when fetching products fails', () => {
    // initialize MenuController
    let MenuController = $controller('MenuController', {
      CategoryDAO,
      ProductDAO,
      Alert,
      $stateParams: {},
    });

    CategoryDAO._listCategoriesDeferred.resolve([]);
    ProductDAO._listProductsDeferred.reject();

    // propagate promises resolutions
    $rootScope.$apply();

    expect(CategoryDAO.listCategories).toHaveBeenCalled();
    expect(ProductDAO.listProducts).toHaveBeenCalled();
    expect(MenuController.categories).toBeUndefined();
    expect(MenuController.products).toBeUndefined();
    expect(Alert.error).toHaveBeenCalledWith('MENU.DATA_FAILED');
  });

  it('should show modal when category is unavailable', () => {
    // initialize MenuController
    let MenuController = $controller('MenuController', {
      CategoryDAO,
      ProductDAO,
      Alert,
      $stateParams: {},
    });

    MenuController.go({
      id: 789,
      name: 'Ice on fire',
      sorting: 0,
      is_sellable: false,
      schedule: []
    });

    expect(MenuController.showModal).toEqual(true);
    expect(MenuController.unavailableItem).toEqual({
      id: 789,
      name: 'Ice on fire',
      sorting: 0,
      is_sellable: false,
      schedule: []
    });
  });

  it('should redirect to subcategories/item when category is available', () => {
    // initialize MenuController
    let MenuController = $controller('MenuController', {
      CategoryDAO,
      ProductDAO,
      Alert,
      $stateParams: {},
    });

    MenuController.go({
      id: 789,
      name: 'Ice on fire',
      sorting: 0,
      is_sellable: true,
      schedule: []
    });

    expect(MenuController.showModal).toEqual(false);
    expect(state.go).toHaveBeenCalledWith('main.menu', { categoryId: 789 });
  });
});
