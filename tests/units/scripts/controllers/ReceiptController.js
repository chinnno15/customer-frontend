/**
* ReceiptController tests
*/
describe('customerApp.controllers.ReceiptController', () => {
  let $scope, PushNotification, PushNotificationDAO, OrderPointStore, OrdersStore, OrderDAO, FeedbackDAO, CompanyDAO, Alert, $state, $rootScope, $controller, STATES, I18n;

  // load the module
  beforeEach(angular.mock.module('customerApp.controllers'));

  // Initialize the controller and mocks
  beforeEach(inject((_$controller_, $q, _$rootScope_, _STATES_) => {
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    $scope = $rootScope.$new();
    STATES = _STATES_;

    // create $state mock
    $state = {
      go: jasmine.createSpy('go')
    };

    // create OrderPointStore mock
    OrderPointStore = {
      getOrderPoint: jasmine.createSpy('getOrderPoint').and.callFake(() => $q.resolve({
        uuid: '55dc44b4-9508-430a-9545-dc21c0b4c36e'
      }))
    };

    // create OrdersStore mock
    OrdersStore = {
      _getLatestOrderDeferred: $q.defer(),
      getLatestOrder: jasmine.createSpy('getLatestOrder').and.callFake(() => OrdersStore._getLatestOrderDeferred.promise)
    };

    // mock OrderDAO
    OrderDAO = {
      _getOrderDeferred: $q.defer(),
      _sendReceiptByEmailDeferred: $q.defer(),
      _getReceiptUrlDeferred: $q.defer(),
      getOrder: jasmine.createSpy('getOrder').and.callFake(() => OrderDAO._getOrderDeferred.promise),
      sendReceiptByEmail: jasmine.createSpy('sendReceiptByEmail').and.callFake(() => OrderDAO._sendReceiptByEmailDeferred.promise),
      getReceiptUrl: jasmine.createSpy('getReceiptUrl').and.callFake(() => OrderDAO._getReceiptUrlDeferred.promise),
    };

    // mock FeedbackDAO
    FeedbackDAO = {
      _createFeedbackDeferred: $q.defer(),
      createFeedback: jasmine.createSpy('FeedbackDAO.createFeedback').and.callFake(() => FeedbackDAO._createFeedbackDeferred.promise),
    };

    // mock CompanyDAO
    CompanyDAO = {
      getCompany: jasmine.createSpy('CompanyDAO.getCompany').and.callFake(() => $q.resolve({})),
    };

    // mock PushNotificationDAO
    PushNotificationDAO = {
      post: jasmine.createSpy('PushNotificationDAO.post').and.callFake(() => $q.resolve({})),
    };

    // mock PushNotification service
    PushNotification = {
      check: jasmine.createSpy('PushNotification.check').and.callFake(() => true),
    };

    // create Alert mock
    Alert = {
      info: jasmine.createSpy('info'),
      error: jasmine.createSpy('error')
    };

    // mock I18n
    I18n = {
      translate: str => $q.resolve(str),
    };
  }));


  it('should initialize controller correctly', () => {
    const ReceiptController = $controller('ReceiptController', {
      $scope, PushNotification, PushNotificationDAO, OrderPointStore, OrdersStore, OrderDAO, FeedbackDAO, CompanyDAO, Alert, $state, I18n,
      $stateParams: {},
    });
    $rootScope.$apply();

    expect(ReceiptController).toBeDefined();
    expect(OrderPointStore.getOrderPoint).toHaveBeenCalled();
    expect(CompanyDAO.getCompany).toHaveBeenCalled();
    expect(ReceiptController.orderPoint).toEqual({ uuid: '55dc44b4-9508-430a-9545-dc21c0b4c36e' });
  });

  it('should fetch order if orderId is provided', () => {
    const ReceiptController = $controller('ReceiptController', {
      $scope, PushNotification, PushNotificationDAO, OrderPointStore, OrdersStore, OrderDAO, FeedbackDAO, CompanyDAO, Alert, $state, I18n,
      $stateParams: { orderId: 123 },
    });

    OrderDAO._getOrderDeferred.resolve({
      id: 123,
      items: [{ product: 1, quantity: 1 }, { product: 2, quantity: 3 }],
    });
    OrderDAO._getReceiptUrlDeferred.resolve('http://test.te/media/test.pdf');
    $rootScope.$apply();

    expect(OrdersStore.getLatestOrder).not.toHaveBeenCalled();
    expect(OrderDAO.getOrder).toHaveBeenCalledWith(123);
    expect(OrderDAO.getReceiptUrl).toHaveBeenCalledWith(123);
    expect(Alert.error).not.toHaveBeenCalled();
    expect(ReceiptController.order).toEqual({
      id: 123,
      items: [{ product: 1, quantity: 1 }, { product: 2, quantity: 3 }],
    });
    expect(ReceiptController.itemsTotal).toEqual(4);
    expect(ReceiptController.downloadUrl).toEqual('http://test.te/media/test.pdf');
  });

  it('should display error message on order fetch failure', () => {
    const ReceiptController = $controller('ReceiptController', {
      $scope, PushNotification, PushNotificationDAO, OrderPointStore, OrdersStore, OrderDAO, FeedbackDAO, CompanyDAO, Alert, $state, I18n,
      $stateParams: { orderId: 123 },
    });

    OrderDAO._getOrderDeferred.reject({ status: 400 });
    $rootScope.$apply();

    expect(OrderDAO.getOrder).toHaveBeenCalledWith(123);
    expect(OrderDAO.getReceiptUrl).not.toHaveBeenCalled();
    expect(Alert.error).toHaveBeenCalledWith('RECEIPT.DATA_FAILED');
    expect(ReceiptController.order).toBeUndefined();
    expect(ReceiptController.itemsTotal).toBeUndefined();
    expect(ReceiptController.downloadUrl).toBeUndefined();
  });

  it('should display unpaid message when order unpaid and payment_method is null', () => {
    $controller('ReceiptController', {
      $scope,
      PushNotification,
      PushNotificationDAO,
      OrderPointStore,
      OrdersStore,
      OrderDAO,
      FeedbackDAO,
      CompanyDAO,
      Alert,
      $state,
      I18n,
      $stateParams: { orderId: 123 },
    });

    OrderDAO._getOrderDeferred.resolve({
      id: 123,
      items: [{ product: 1, quantity: 1 }, { product: 2, quantity: 3 }],
      paid: false,
      payment_method: null
    });
    $rootScope.$apply();

    expect(OrdersStore.getLatestOrder).not.toHaveBeenCalled();
    expect(OrderDAO.getOrder).toHaveBeenCalledWith(123);
    expect(Alert.error).toHaveBeenCalledWith('RECEIPT.UNPAID_MESSAGE');
  });

  it('should display error message on PDF receipt URL fetch failure', () => {
    const ReceiptController = $controller('ReceiptController', {
      $scope, PushNotification, PushNotificationDAO, OrderPointStore, OrdersStore, OrderDAO, FeedbackDAO, CompanyDAO, Alert, $state, I18n,
      $stateParams: { orderId: 123 },
    });

    OrderDAO._getOrderDeferred.resolve({
      id: 123,
      items: [{ product: 1, quantity: 1 }, { product: 2, quantity: 3 }],
    });
    OrderDAO._getReceiptUrlDeferred.reject({ status: 400 });
    $rootScope.$apply();

    expect(OrderDAO.getOrder).toHaveBeenCalledWith(123);
    expect(OrderDAO.getReceiptUrl).toHaveBeenCalledWith(123);
    expect(Alert.error).toHaveBeenCalledWith('RECEIPT.DATA_FAILED');
    expect(ReceiptController.downloadUrl).toBeUndefined();
  });

  it('should get latest order if orderId is not provided', () => {
    const ReceiptController = $controller('ReceiptController', {
      $scope, PushNotification, PushNotificationDAO, OrderPointStore, OrdersStore, OrderDAO, FeedbackDAO, CompanyDAO, Alert, $state, I18n,
      $stateParams: {},
    });

    OrdersStore._getLatestOrderDeferred.resolve({
      id: 123,
      items: [{ product: 1, quantity: 1 }, { product: 2, quantity: 3 }],
    });
    $rootScope.$apply();

    expect(OrderDAO.getOrder).not.toHaveBeenCalled();
    expect(OrdersStore.getLatestOrder).toHaveBeenCalled();
    expect(Alert.error).not.toHaveBeenCalled();
    expect(ReceiptController.order).toEqual({
      id: 123,
      items: [{ product: 1, quantity: 1 }, { product: 2, quantity: 3 }],
    });
    expect(ReceiptController.itemsTotal).toEqual(4);
  });

  it('should display error message on latest order fetch failure', () => {
    const ReceiptController = $controller('ReceiptController', {
      $scope, PushNotification, PushNotificationDAO, OrderPointStore, OrdersStore, OrderDAO, FeedbackDAO, CompanyDAO, Alert, $state, I18n,
      $stateParams: {},
    });

    OrdersStore._getLatestOrderDeferred.reject({ status: 400 });
    $rootScope.$apply();

    expect(OrdersStore.getLatestOrder).toHaveBeenCalled();
    expect(OrderDAO.getReceiptUrl).not.toHaveBeenCalled();
    expect(Alert.error).toHaveBeenCalledWith('RECEIPT.DATA_FAILED');
    expect(ReceiptController.order).toBeUndefined();
    expect(ReceiptController.itemsTotal).toBeUndefined();
    expect(ReceiptController.downloadUrl).toBeUndefined();
  });

  it('should redirect to menu state when no latest order nor orderId', () => {
    const ReceiptController = $controller('ReceiptController', {
      $scope, PushNotification, PushNotificationDAO, OrderPointStore, OrdersStore, OrderDAO, FeedbackDAO, CompanyDAO, Alert, $state, I18n,
      $stateParams: {},
    });

    OrdersStore._getLatestOrderDeferred.resolve(null);
    $rootScope.$apply();

    expect(OrderDAO.getOrder).not.toHaveBeenCalled();
    expect(OrderDAO.getReceiptUrl).not.toHaveBeenCalled();
    expect(OrdersStore.getLatestOrder).toHaveBeenCalled();
    expect(Alert.error).not.toHaveBeenCalled();
    expect(ReceiptController.order).toBeUndefined();
    expect(ReceiptController.itemsTotal).toBeUndefined();
    expect(ReceiptController.downloadUrl).toBeUndefined();
    expect($state.go).toHaveBeenCalledWith(STATES.MENU);
  });

  describe('sendByEmail', () => {
    let ReceiptController;

    beforeEach(() => {
      ReceiptController = $controller('ReceiptController', {
        $scope, PushNotification, PushNotificationDAO, OrderPointStore, OrdersStore, OrderDAO, FeedbackDAO, CompanyDAO, Alert, $state, I18n,
        $stateParams: {},
      });

      // Set default order, with id 123
      ReceiptController.order = { id: 123 };
    });

    it('should do nothing when email is empty', () => {
      ReceiptController.sendByEmail();
      OrderDAO._sendReceiptByEmailDeferred.resolve();
      $rootScope.$apply();

      expect(OrderDAO.sendReceiptByEmail).not.toHaveBeenCalled();
    });

    it('should display error when sending receipt fails', () => {
      ReceiptController.email = 'test@test.te';
      ReceiptController.sendByEmail();
      OrderDAO._sendReceiptByEmailDeferred.reject();
      $rootScope.$apply();

      expect(OrderDAO.sendReceiptByEmail).toHaveBeenCalledWith(123, 'test@test.te');
      expect(Alert.error).toHaveBeenCalledWith('RECEIPT.SAVE.EMAIL_FAILED');
    });

    it('should correct send a receipt', () => {
      ReceiptController.email = 'test@test.te';
      ReceiptController.sendByEmail();
      OrderDAO._sendReceiptByEmailDeferred.resolve();
      $rootScope.$apply();

      expect(OrderDAO.sendReceiptByEmail).toHaveBeenCalledWith(123, 'test@test.te');
      expect(ReceiptController.email).toEqual('');
      expect(Alert.info).toHaveBeenCalledWith('RECEIPT.SAVE.EMAIL_SUCCESS');
    });
  });

  describe('sendFeedback()', () => {
    let ReceiptController;

    beforeEach(() => {
      ReceiptController = $controller('ReceiptController', {
        $scope, PushNotification, PushNotificationDAO, OrderPointStore, OrdersStore, OrderDAO, FeedbackDAO, CompanyDAO, Alert, $state, I18n,
        $stateParams: {},
      });

      // Set default order, with id 123
      ReceiptController._setOrder({
        id: 123,
        items: [
          { product: 1, quantity: 1 },
          { product: 2, quantity: 3 },
        ],
      });
    });

    it('should delegate to FeedbackDAO#createFeedback', () => {
      ReceiptController.feedback = 'a feedback content';
      ReceiptController.sendFeedback();
      expect(FeedbackDAO.createFeedback).toHaveBeenCalledWith(123, 'a feedback content');
    });

    it('should update feedback if success', () => {
      ReceiptController.feedback = 'a feedback content';
      ReceiptController.sendFeedback();
      FeedbackDAO._createFeedbackDeferred.resolve({ feedback: 'a feedback content' });
      $rootScope.$apply();

      expect(ReceiptController.order.feedback).toBe('a feedback content');
    });

    it('should display generic error if failed for no reason', () => {
      ReceiptController.sendFeedback();
      FeedbackDAO._createFeedbackDeferred.reject({});
      $rootScope.$apply();

      expect(Alert.error).toHaveBeenCalledWith('RECEIPT.FEEDBACK.CREATE_FAILED');
    });

    it('should display detailed error if any', () => {
      ReceiptController.sendFeedback();
      FeedbackDAO._createFeedbackDeferred.reject({ data: { detail: 'a detailed error', }, });
      $rootScope.$apply();

      expect(Alert.error).toHaveBeenCalledWith('a detailed error');
    });
  });
});
