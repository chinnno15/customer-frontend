/**
* CompanyController tests
*/
describe('customerApp.controllers.CompanyController', () => {
  let CompanyController;
  let CompanyDAO;
  let Alert;
  let $rootScope;
  let $scope;
  let OrderPointStore;

  // load the module
  beforeEach(angular.mock.module('customerApp.controllers'));

  // Initialize the controller and mocks
  beforeEach(inject(($controller, $q, _$rootScope_) => {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();

    // create CompanyDAO mock
    CompanyDAO = {
      _getCompanyDeferred: $q.defer(),
      getCompany: jasmine.createSpy('getCompany').and.callFake(() => CompanyDAO._getCompanyDeferred.promise)
    };

    // create Alert mock
    Alert = {
      error: jasmine.createSpy('error'),
      success: jasmine.createSpy('success')
    };

    OrderPointStore = {
      isOrderPointRegistered: () => true
    };

    // initialize CompanyController
    CompanyController = $controller('CompanyController', {
      CompanyDAO,
      Alert,
      $scope,
      OrderPointStore,
    });
  }));


  it('should be defined', () => {
    expect(CompanyController).toBeDefined();
  });

  it('should initialize controller correctly', () => {
    CompanyDAO._getCompanyDeferred.resolve({ id: 1, name: 'Test Restaurant' });

    // propagate promises resolutions
    $rootScope.$apply();

    expect(CompanyDAO.getCompany).toHaveBeenCalled();
    expect(CompanyController.company).toEqual({ id: 1, name: 'Test Restaurant' });
    expect(Alert.error).not.toHaveBeenCalled();
  });

  it('should display error message when fetching company fails', () => {
    CompanyDAO._getCompanyDeferred.reject();

    // propagate promises resolutions
    $rootScope.$apply();

    expect(CompanyDAO.getCompany).toHaveBeenCalled();
    expect(CompanyController.company).toBeUndefined();
    expect(Alert.error).toHaveBeenCalledWith('COMPANY.DATA_FAILED');
  });
});
