/**
* MainController tests
*/
describe('customerApp.controllers.mainController', () => {
  let MainController;
  let scope;
  let stateParams;
  let SwishAppInit;

  // load the module
  beforeEach(angular.mock.module('customerApp.controllers'));

  // Initialize the controller and mocks
  beforeEach(inject(($controller, $rootScope) => {
    // create new scope
    scope = $rootScope.$new();

    SwishAppInit = {
      registerOrderPoint: jasmine.createSpy('registerOrderPoint')
    };

    stateParams = {
      storeName: 'store-name',
      orderPointName: 'orderpoint-name'
    };

    // initialize MainController
    MainController = $controller('MainController', {
      $scope: scope,
      $stateParams: stateParams,
      SwishAppInit: SwishAppInit
    });
  }));


  it('should be defined', () => {
    expect(MainController).toBeDefined();
  });

  it('should register order point', () => {
    expect(SwishAppInit.registerOrderPoint).toHaveBeenCalledWith('store-name', 'orderpoint-name');
  });
});
