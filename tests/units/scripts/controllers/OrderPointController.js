/**
* OrderPointController tests
*/
describe('customerApp.controllers.OrderPointController', () => {
  let OrderPointController, $scope, $rootScope, OrderPointStore, EVENTS, Alert;

  // load the module
  beforeEach(angular.mock.module('customerApp.controllers'));

  // Initialize the controller and mocks
  beforeEach(inject(($q, $controller, _$rootScope_, _EVENTS_) => {
    EVENTS = _EVENTS_;
    $rootScope = _$rootScope_;
    spyOn($rootScope, '$emit').and.callThrough();

    // create new $scope
    $scope = $rootScope.$new();

    // mock OrderPointStore
    OrderPointStore = {
      _isAvailableDeferred: $q.defer(),
      isAvailable: jasmine.createSpy().and.callFake(() => OrderPointStore._isAvailableDeferred.promise),
    };

    Alert = {
      error: jasmine.createSpy('Alert.error')
    };

    // initialize OrderPointController
    OrderPointController = $controller('OrderPointController', {
      $scope,
      $rootScope,
      OrderPointStore,
      Alert: Alert
    });
  }));


  it('should be defined', () => {
    expect(OrderPointController).toBeDefined();
  });

  it('should show modal if orderpoint is not available', () => {
    expect(OrderPointController.showModal).toBeFalsy();
    expect(OrderPointController.isAvailable).toEqual(true);
    OrderPointStore._isAvailableDeferred.reject({ error: 'ERROR' });
    $rootScope.$apply();

    expect(OrderPointController.showModal).toEqual(true);
    expect(OrderPointController.isAvailable).toEqual(false);
    expect(OrderPointController.details).toEqual({ error: 'ERROR' });
    expect(OrderPointStore.isAvailable).toHaveBeenCalled();
    expect($rootScope.$emit).toHaveBeenCalledWith(EVENTS.ORDERPOINT_AVAILABILITY_CHANGED, false);
  });

  it('shouldnt show modal if orderpoint was already unavailable', () => {
    OrderPointController.details = { error: 'ERROR' };
    OrderPointController.isAvailable = false;
    OrderPointStore._isAvailableDeferred.reject({ error: 'ERROR' });
    $rootScope.$apply();

    expect(OrderPointController.showModal).toBeFalsy();
    expect(OrderPointStore.isAvailable).toHaveBeenCalled();
    expect($rootScope.$emit).not.toHaveBeenCalled();
  });

  it('should show modal if error details changed', () => {
    OrderPointController.details = { error: 'ERROR 2' };
    OrderPointController.isAvailable = false;
    OrderPointStore._isAvailableDeferred.reject({ error: 'ERROR' });
    $rootScope.$apply();

    expect(OrderPointController.showModal).toEqual(true);
    expect(OrderPointController.details).toEqual({ error: 'ERROR' });
    expect(OrderPointStore.isAvailable).toHaveBeenCalled();
    expect($rootScope.$emit).not.toHaveBeenCalled();
  });

  it('should hide modal if orderpoint is available', () => {
    OrderPointController.showModal = true;
    OrderPointController.isAvailable = false;
    OrderPointStore._isAvailableDeferred.resolve();
    $rootScope.$apply();

    expect(OrderPointController.showModal).toEqual(false);
    expect(OrderPointController.isAvailable).toEqual(true);
    expect(OrderPointStore.isAvailable).toHaveBeenCalled();
    expect($rootScope.$emit).toHaveBeenCalledWith(EVENTS.ORDERPOINT_AVAILABILITY_CHANGED, true);
  });

  it('should not emit event if order point was already available', () => {
    OrderPointController.showModal = false;
    OrderPointController.isAvailable = true;
    OrderPointStore._isAvailableDeferred.resolve();
    $rootScope.$apply();

    expect(OrderPointController.showModal).toEqual(false);
    expect(OrderPointController.isAvailable).toEqual(true);
    expect(OrderPointStore.isAvailable).toHaveBeenCalled();
    expect($rootScope.$emit).not.toHaveBeenCalled();
  });
});
