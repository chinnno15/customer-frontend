/**
* ProductController tests
*/
describe('customerApp.controllers.ProductController', () => {
  let ProductController;
  let Cart;
  let Alert;
  let $scope;
  let $rootScope;
  let EVENTS;
  let $controller;

  // load the module
  beforeEach(angular.mock.module('customerApp.controllers'));

  // Initialize the controller and mocks
  beforeEach(inject((_$controller_, $q, _$rootScope_, _EVENTS_) => {
    $rootScope = _$rootScope_;
    EVENTS = _EVENTS_;
    $controller = _$controller_;

    // create $scope mock
    $scope = {
      item: { id: 1, name: 'Soup' }
    };

    // create Cart mock
    Cart = {
      _quantity: 0,
      _optionInObject: {},
      _setQuantityDeferred: $q.defer(),
      setQuantity: jasmine.createSpy('setQuantity').and.callFake(() => {
        return Cart._setQuantityDeferred.promise;
      }),
      getQuantity: jasmine.createSpy('getQuantity').and.callFake(() => Cart._quantity),
      _setOptionDeffered: $q.defer(),
      setOption: jasmine.createSpy('setOption').and.callFake(() => {
        return Cart._setOptionDeffered.promise;
      }),
      getOption: jasmine.createSpy('getOption').and.callFake(() => Cart._optionInObject)
    };

    // create Alert mock
    Alert = {
      error: jasmine.createSpy('error')
    };

    // initialize ProductController
    ProductController = $controller('ProductController', {
      Cart,
      Alert,
      $scope
    });
  }));


  it('should be defined', () => {
    expect(ProductController).toBeDefined();
  });

  it('should initialize controller correctly', () => {
    expect(ProductController.product).toEqual({ id: 1, name: 'Soup' });
    expect(ProductController.quantity).toEqual(0);
    expect(ProductController.quantityCounter).toEqual(false);
    expect(Cart.getQuantity.calls.count()).toEqual(1);
    expect(Cart.getQuantity).toHaveBeenCalledWith(1);
    expect(Cart.getOption).not.toHaveBeenCalled();
  });

  it('should reinitialize on cart update', () => {
    Cart._quantity = 1;
    expect(ProductController.quantity).toEqual(0);

    $rootScope.$emit(EVENTS.CART_UPDATED);
    $rootScope.$apply();

    expect(ProductController.quantity).toEqual(1);
    expect(ProductController.quantityCounter).toEqual(true);
    expect(Cart.getQuantity.calls.count()).toEqual(2);
  });

  describe('incQuantity()', () => {
    it('should increase product quantity', () => {
      expect(ProductController.quantity).toEqual(0);
      ProductController.incQuantity();
      expect(ProductController.quantity).toEqual(1);
      expect(Cart.setQuantity).toHaveBeenCalledWith({ id: 1, name: 'Soup' }, 1);
    });

    it('should display error message when modifying quantity fails', () => {
      ProductController.incQuantity();
      expect(ProductController.quantity).toEqual(1);
      expect(Cart.setQuantity).toHaveBeenCalledWith({ id: 1, name: 'Soup' }, 1);
      Cart._setQuantityDeferred.reject();

      // propagate promises resolutions
      $rootScope.$apply();

      expect(ProductController.quantity).toEqual(0);
      expect(Alert.error).toHaveBeenCalledWith('CART.UPDATE_QUANTITY_FAILED');
    });
  });

  describe('decQuantity()', () => {
    it('should decrease product quantity', () => {
      Cart._quantity = 1;
      $rootScope.$emit(EVENTS.CART_UPDATED);
      $rootScope.$apply();
      Cart._setQuantityDeferred.resolve();
      ProductController.decQuantity();
      expect(ProductController.quantity).toEqual(0);
      expect(Cart.setQuantity.calls.count()).toEqual(1);
      expect(Cart.setQuantity).toHaveBeenCalledWith({ id: 1, name: 'Soup' }, 0);

      // make sure it doesn't go below 0
      ProductController.decQuantity();
      expect(ProductController.quantity).toEqual(0);
      expect(Cart.setQuantity.calls.count()).toEqual(1);
    });

    it('should display error message when modifying quantity fails', () => {
      Cart._quantity = 1;
      $rootScope.$emit(EVENTS.CART_UPDATED);
      $rootScope.$apply();
      ProductController.decQuantity();
      expect(ProductController.quantity).toEqual(0);
      expect(Cart.setQuantity).toHaveBeenCalledWith({ id: 1, name: 'Soup' }, 0);
      Cart._setQuantityDeferred.reject();

      // propagate promises resolutions
      $rootScope.$apply();

      expect(ProductController.quantity).toEqual(1);
      expect(Alert.error).toHaveBeenCalledWith('CART.UPDATE_QUANTITY_FAILED');
    });
  });

  describe('showQuantityCounter', () => {
    it('should show modal when product is unavailable', () => {
      $scope.MenuCtrl = {
        showModal: false
      };

      ProductController.showQuantityCounter({
        id: 789,
        name: 'Ice on fire',
        sorting: 0,
        is_sellable: false,
        schedule: []
      });

      expect($scope.MenuCtrl.showModal).toEqual(true);
      expect($scope.MenuCtrl.unavailableItem).toEqual({
        id: 789,
        name: 'Ice on fire',
        sorting: 0,
        is_sellable: false,
        schedule: []
      });
    });

    it('should show quantity counter when product is available', () => {
      $scope.MenuCtrl = {
        showModal: false
      };

      ProductController.showQuantityCounter({
        id: 789,
        name: 'Ice on fire',
        sorting: 0,
        is_sellable: true,
        schedule: []
      });

      expect($scope.MenuCtrl.showModal).toEqual(false);
      expect(ProductController.quantityCounter).toEqual(true);
    });
  });


  describe('product has option enabled', () => {
    let ProductCtrl;

    beforeEach(() => {
      $scope.item = {
        id: 1,
        name: 'Soup',
        is_option_enabled: true,
        preferences: [
          [
            { id: 1, name: 'group 1 pref 1', price: 1, is_active: true },
            { id: 2, name: 'group 1 pref 2', price: 2, is_active: true },
            { id: 3, name: 'group 1 pref 3', price: 3, is_active: true, is_selected: true },
          ],
          [
            { id: 4, name: 'group 2 pref 1', price: 1, is_active: true },
            { id: 5, name: 'group 2 pref 2', price: 2, is_active: true },
            { id: 6, name: 'group 2 pref 3', price: 3, is_active: false },
          ],
        ],
        extras: [
          { id: 7, name: 'extra 1', price: 1, is_active: true },
          { id: 8, name: 'extra 2', price: 0, is_active: true },
          { id: 9, name: 'extra 1', price: 1, is_active: false }
        ]
      };

      ProductCtrl = $controller('ProductController', {
        Cart,
        Alert,
        $scope
      });
    });

    it('should init product with option', () => {
      expect(ProductCtrl.product.preferences).toEqual([
        [
          { id: 1, name: 'group 1 pref 1', price: 1, is_active: true },
          { id: 2, name: 'group 1 pref 2', price: 2, is_active: true },
          { id: 3, name: 'group 1 pref 3', price: 3, is_active: true, is_selected: true },
        ],
        [
          { id: 4, name: 'group 2 pref 1', price: 1, is_active: true },
          { id: 5, name: 'group 2 pref 2', price: 2, is_active: true },
        ],
      ]);
      expect(ProductCtrl.product.extras).toEqual([
        { id: 7, name: 'extra 1', price: 1, is_active: true },
        { id: 8, name: 'extra 2', price: 0, is_active: true },
      ]);
      expect(ProductCtrl.option).toEqual({
        preferences: [
          { id: 3, name: 'group 1 pref 3', price: 3, is_active: true, is_selected: true },
          { id: 4, name: 'group 2 pref 1', price: 1, is_active: true },
        ],
        extras: [
          { id: 7, name: 'extra 1', price: 1, show_counter: false, quantity: 0 },
          { id: 8, name: 'extra 2', price: 0, show_counter: false, quantity: 0 }
        ]
      });
    });

    it('should reinitialize option when cart update', () => {
      Cart._optionInObject = {
        preferences: [
          { id: 2, name: 'group 1 pref 2', price: 2},
        ],
        extras: [
          { id: 7, name: 'extra 1', price: 1, show_counter: false, quantity: 2 },
        ]
      };

      $rootScope.$emit(EVENTS.CART_UPDATED);
      $rootScope.$apply();

      expect(ProductCtrl.option).toEqual({
        preferences: [
          { id: 2, name: 'group 1 pref 2', is_active: true, price: 2 },
          { id: 4, name: 'group 2 pref 1', is_active: true, price: 1 },
        ],
        extras: [
          { id: 7, name: 'extra 1', price: 1, show_counter: true, quantity: 2 },
          { id: 8, name: 'extra 2', price: 0, show_counter: false, quantity: 0 }
        ]
      });
    });

    it('should update Cart\'s product option', () => {
      ProductCtrl.option = {
        preferences: [
          { id: 2, name: 'group 1 pref 2', is_active: true, price: 2 },
          { id: 4, name: 'group 2 pref 1', is_active: true, price: 1 },
        ],
        extras: [
          { id: 7, name: 'extra 1', price: 1, show_counter: false, quantity: 0 },
          { id: 8, name: 'extra 2', price: 0, show_counter: false, quantity: 0 }
        ]
      };
      ProductCtrl.quantity = 1;
      ProductCtrl.updateCartOptions();

      expect(Cart.setOption).toHaveBeenCalledWith({
        id: 1,
        name: 'Soup',
        is_option_enabled: true,
        preferences: [
          [
            { id: 1, name: 'group 1 pref 1', price: 1, is_active: true },
            { id: 2, name: 'group 1 pref 2', price: 2, is_active: true },
            { id: 3, name: 'group 1 pref 3', price: 3, is_active: true, is_selected: true },
          ],
          [
            { id: 4, name: 'group 2 pref 1', price: 1, is_active: true },
            { id: 5, name: 'group 2 pref 2', price: 2, is_active: true },
          ],
        ],
        extras: [
          { id: 7, name: 'extra 1', price: 1, is_active: true },
          { id: 8, name: 'extra 2', price: 0, is_active: true },
        ]
      }, {
        preferences: [
          { id: 2, name: 'group 1 pref 2', is_active: true, price: 2 },
          { id: 4, name: 'group 2 pref 1', is_active: true, price: 1 },
        ],
        extras: [
          { id: 7, name: 'extra 1', price: 1, show_counter: false, quantity: 0 },
          { id: 8, name: 'extra 2', price: 0, show_counter: false, quantity: 0 }
        ]
      });
    });
  });

  // TODO tests for setQuantity, but need to wait for stable cart flow
});
