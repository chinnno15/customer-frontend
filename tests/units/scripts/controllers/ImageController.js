/**
* ImageController tests
*/
describe('customerApp.controllers.ImageController', () => {
  let $controller, $scope, $attrs;

  // load the module
  beforeEach(angular.mock.module('customerApp.controllers'));

  // Initialize the controller and mocks
  beforeEach(inject((_$controller_) => {
    // create $scope mock
    $scope = {};

    // create $attrs mock
    $attrs = {};

    $controller = _$controller_;
  }));

  it('should initialize controller correctly for no image', () => {
    $scope.item = {
      thumbs: { '540x220': null },
      default_color: null,
    };

    const ImageController = $controller('ImageController', { $scope, $attrs });

    expect(ImageController.defaultImage).toEqual(true);
    expect(ImageController.noImage).toEqual(true);
    expect(ImageController.backgroundStyles).toEqual({});
  });

  it('should initialize controller correctly for default image', () => {
    $scope.item = {
      thumbs: { '540x220': 'http://server/img/default.svg' },
      default_color: '#00ff00',
    };

    const ImageController = $controller('ImageController', { $scope, $attrs });

    expect(ImageController.defaultImage).toEqual(true);
    expect(ImageController.noImage).toEqual(false);
    expect(ImageController.backgroundStyles).toEqual({
      'background-image': 'url(http://server/img/default.svg)',
      'background-color': '#00ff00'
    });
  });

  it('should initialize controller correctly for uploaded image', () => {
    $scope.item = {
      thumbs: { '540x220': 'http://server/img/image.svg' },
      default_color: null,
    };

    const ImageController = $controller('ImageController', { $scope, $attrs });

    expect(ImageController.defaultImage).toEqual(false);
    expect(ImageController.noImage).toEqual(false);
    expect(ImageController.backgroundStyles).toEqual({
      'background-image': 'url(http://server/img/image.svg)'
    });
  });

  it('should initialize controller with small image', () => {
    $scope.item = {
      thumbs: {
        '89x89': 'http://server/img/image_small.svg',
        '540x220': 'http://server/img/image_big.svg',
      },
      default_color: null,
    };

    $attrs.imageSize = '89x89';

    const ImageController = $controller('ImageController', { $scope, $attrs });

    expect(ImageController.backgroundStyles).toEqual({
      'background-image': 'url(http://server/img/image_small.svg)'
    });
  });
});
