/**
* CartProductController tests
* extends from ProductController
*/
describe('customerApp.controllers.CartProductController', () => {
  let CartProductController;
  let Cart;
  let Alert;
  let $scope;
  let $rootScope;
  let EVENTS;

  // load the module
  beforeEach(angular.mock.module('customerApp.controllers'));

  // Initialize the controller and mocks
  beforeEach(inject(($controller, $q, _$rootScope_, _EVENTS_) => {
    $rootScope = _$rootScope_;
    EVENTS = _EVENTS_;

    // create $scope mock
    $scope = {
      item: {
        product: { id: 1, name: 'Soup' },
        quantity: 0
      }
    };

    // create Cart mock
    Cart = {
      _quantity: 0,
      _optionInObject: 0,
      _setQuantityDeferred: $q.defer(),
      setQuantity: jasmine.createSpy('setQuantity').and.callFake(() => {
        return Cart._setQuantityDeferred.promise;
      }),
      getQuantity: jasmine.createSpy('getQuantity').and.callFake(() => Cart._quantity),
      _setOptionDeffered: $q.defer(),
      setOption: jasmine.createSpy('setOption').and.callFake(() => {
        return Cart._setOptionDeffered.promise;
      }),
      getOption: jasmine.createSpy('getOption').and.callFake(() => Cart._optionInObject)
    };

    // create Alert mock
    Alert = {
      error: jasmine.createSpy('error')
    };

    // initialize CartProductController
    CartProductController = $controller('CartProductController', {
      Cart,
      Alert,
      $scope
    });
  }));


  it('should be defined', () => {
    expect(CartProductController).toBeDefined();
  });

  it('should initialize controller correctly', () => {
    expect(CartProductController.product).toEqual({ id: 1, name: 'Soup' });
    expect(CartProductController.quantity).toEqual(0);
    expect(CartProductController.quantityCounter).toEqual(false);
    expect(Cart.getQuantity.calls.count()).toEqual(1);
    expect(Cart.getQuantity).toHaveBeenCalledWith(1);
  });

  it('should reinitialize on cart update', () => {
    Cart._quantity = 1;
    expect(CartProductController.quantity).toEqual(0);

    $rootScope.$emit(EVENTS.CART_UPDATED);
    $rootScope.$apply();

    expect(CartProductController.quantity).toEqual(1);
    expect(CartProductController.quantityCounter).toEqual(true);
    expect(Cart.getQuantity.calls.count()).toEqual(2);
  });

  describe('incQuantity()', () => {
    it('should increase product quantity', () => {
      expect(CartProductController.quantity).toEqual(0);
      CartProductController.incQuantity();
      expect(CartProductController.quantity).toEqual(1);
      expect(Cart.setQuantity).toHaveBeenCalledWith({ id: 1, name: 'Soup' }, 1);
    });

    it('should display error message when modifying quantity fails', () => {
      CartProductController.incQuantity();
      expect(CartProductController.quantity).toEqual(1);
      expect(Cart.setQuantity).toHaveBeenCalledWith({ id: 1, name: 'Soup' }, 1);
      Cart._setQuantityDeferred.reject();

      // propagate promises resolutions
      $rootScope.$apply();

      expect(CartProductController.quantity).toEqual(0);
      expect(Alert.error).toHaveBeenCalledWith('CART.UPDATE_QUANTITY_FAILED');
    });
  });

  describe('decQuantity()', () => {
    it('should decrease product quantity', () => {
      Cart._quantity = 1;
      $rootScope.$emit(EVENTS.CART_UPDATED);
      $rootScope.$apply();
      Cart._setQuantityDeferred.resolve();
      CartProductController.decQuantity();
      expect(CartProductController.quantity).toEqual(0);
      expect(Cart.setQuantity.calls.count()).toEqual(1);
      expect(Cart.setQuantity).toHaveBeenCalledWith({ id: 1, name: 'Soup' }, 0);

      // make sure it doesn't go below 0
      CartProductController.decQuantity();
      expect(CartProductController.quantity).toEqual(0);
      expect(Cart.setQuantity.calls.count()).toEqual(1);
    });

    it('should display error message when modifying quantity fails', () => {
      Cart._quantity = 1;
      $rootScope.$emit(EVENTS.CART_UPDATED);
      $rootScope.$apply();
      CartProductController.decQuantity();
      expect(CartProductController.quantity).toEqual(0);
      expect(Cart.setQuantity).toHaveBeenCalledWith({ id: 1, name: 'Soup' }, 0);
      Cart._setQuantityDeferred.reject();

      // propagate promises resolutions
      $rootScope.$apply();

      expect(CartProductController.quantity).toEqual(1);
      expect(Alert.error).toHaveBeenCalledWith('CART.UPDATE_QUANTITY_FAILED');
    });
  });
});
