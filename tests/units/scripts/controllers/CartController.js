/**
* CartController tests
*/
describe('customerApp.controllers.CartController', () => {
  let CartController;
  let Company;
  let Cart;
  let Checkout;
  let Alert;
  let CartItemsStore;
  let $scope;
  let $rootScope;
  let PAYMENT_PROVIDERS;
  let EVENTS;

  // load the module
  beforeEach(angular.mock.module('customerApp.controllers'));

  // Initialize the controller and mocks
  beforeEach(inject(($controller, _$q_, _$rootScope_, _PAYMENT_PROVIDERS_, _EVENTS_) => {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    PAYMENT_PROVIDERS = _PAYMENT_PROVIDERS_;
    EVENTS = _EVENTS_;

    // create Company mock
    Company = {
      getCompany: jasmine.createSpy('getCompany').and.callFake(() => Company._company),
    };

    // create Cart mock
    Cart = {
      _initializedDeferred: _$q_.defer(),
      __products: [
        { id: 1, name: 'Soup', },
        { id: 2, name: 'Chicken', },
      ],
      getProducts: jasmine.createSpy('getProducts').and.callFake(() => Cart.__products),
      getTotals: jasmine.createSpy('getTotals').and.callFake(() => Cart.__totals),
      waitForInitializedCart: jasmine.createSpy('waitForInitializedCart').and.callFake(() => Cart._initializedDeferred.promise),
    };

    // create Checkout mock
    Checkout = {
      __checkoutDeferred: _$q_.defer(),
      checkout: jasmine.createSpy('checkout').and.callFake(() => Checkout.__checkoutDeferred.promise),
    };

    // create Alert mock
    Alert = {
      error: jasmine.createSpy('error'),
    };

    // create CartItemsStore mock
    CartItemsStore = {
      prioritizeItem: jasmine.createSpy()
    };

    // initialize CartController
    CartController = $controller('CartController', {
      $scope,
      Alert,
      Cart,
      CartItemsStore,
      Checkout,
      Company,
    });
  }));

  // TODO Right now EVENTS.CART_UPDATED will refresh product list too, and we
  // have had no tests for it yet. However, this is not a stable flow, and
  // should be refactored at once.

  it('should be defined and initialize correctly', () => {
    expect(CartController).toBeDefined();
    expect(Cart.getProducts.calls.count()).toEqual(0);
    expect(CartController.isOrderPointAvailable).toEqual(true);
    expect(Cart.waitForInitializedCart).toHaveBeenCalled();
    Cart._initializedDeferred.resolve();
    $rootScope.$apply();
    expect(Cart.getProducts.calls.count()).toEqual(1);
    expect(CartController.products).toEqual([{ id: 1, name: 'Soup' }, { id: 2, name: 'Chicken' }]);
    expect(Company.getCompany).toHaveBeenCalled();
    expect(Alert.error).not.toHaveBeenCalled();
  });

  it('should update isOrderPointAvailable to true', () => {
    CartController.isOrderPointAvailable = false;
    $rootScope.$emit(EVENTS.ORDERPOINT_AVAILABILITY_CHANGED, true);
    expect(CartController.isOrderPointAvailable).toEqual(true);
  });

  it('should update isOrderPointAvailable to false', () => {
    CartController.isOrderPointAvailable = true;
    $rootScope.$emit(EVENTS.ORDERPOINT_AVAILABILITY_CHANGED, false);
    expect(CartController.isOrderPointAvailable).toEqual(false);
  });

  describe('checkout()', () => {
    it('should not perform checkout if cart is empty', () => {
      Company._company = {
        payment_providers: {
          stripe: { available: true, active: true },
          gear: { available: true, active: true },
          bilderlingspay: { available: false, active: false },
        }
      };
      $rootScope.$apply();

      Cart.__totals = { quantity: 0, price: 0 };
      CartController.checkout(CartController.METHODS.CREDIT_CART);
      $rootScope.$apply();
      expect(Checkout.checkout).not.toHaveBeenCalled();
    });

    it('should display info popup if CC payment method is not available', () => {
      Company._company = {
        payment_providers: {
          stripe: { available: false, active: false },
          gear: { available: true, active: true },
          bilderlingspay: { available: false, active: false },
        }
      };
      $rootScope.$apply();

      Cart.__totals = { quantity: 2, price: 32 };
      expect(CartController.showPaymentUnavailableModal).toBeFalsy();
      CartController.checkout(CartController.METHODS.CREDIT_CART);
      $rootScope.$apply();
      expect(Checkout.checkout).not.toHaveBeenCalled();
      expect(CartController.showPaymentUnavailableModal).toEqual(true);
    });

    it('should display info popup if BTC payment method is not available', () => {
      Company._company = {
        payment_providers: {
          stripe: { available: true, active: true },
          gear: { available: false, active: false },
          bilderlingspay: { available: false, active: false },
        }
      };
      $rootScope.$apply();

      Cart.__totals = { quantity: 2, price: 32 };
      expect(CartController.showPaymentUnavailableModal).toBeFalsy();
      CartController.checkout(CartController.METHODS.BITCOIN);
      $rootScope.$apply();
      expect(Checkout.checkout).not.toHaveBeenCalled();
      expect(CartController.showPaymentUnavailableModal).toEqual(true);
    });

    it('should display info popup if no waiters are available', () => {
      Company._company = {
        payment_providers: {
          stripe: { available: true, active: true },
          gear: { available: true, active: true },
          bilderlingspay: { available: false, active: false },
        },
        waiters_working: false,
      };
      $rootScope.$apply();

      Cart.__totals = { quantity: 2, price: 32 };
      expect(CartController.showPaymentUnavailableModal).toBeFalsy();
      expect(CartController.showWaitersUnavailableModal).toBeFalsy();
      CartController.checkout(CartController.METHODS.BITCOIN);
      $rootScope.$apply();
      expect(Checkout.checkout).not.toHaveBeenCalled();
      expect(CartController.showPaymentUnavailableModal).toBeFalsy();
      expect(CartController.showWaitersUnavailableModal).toEqual(true);
    });

    it('should not perform checkout if orderpoint is not available', () => {
      CartController.isOrderPointAvailable = false;
      Company._company = {
        payment_providers: {
          stripe: { available: true, active: true },
          gear: { available: true, active: true },
          bilderlingspay: { available: false, active: false },
        },
        waiters_working: true
      };
      $rootScope.$apply();
      Checkout.__checkoutDeferred.resolve();
      Cart.__totals = { quantity: 2, price: 32 };

      CartController.checkout(CartController.METHODS.CREDIT_CART);
      $rootScope.$apply();

      expect(Checkout.checkout).not.toHaveBeenCalled();
      expect(Alert.error).not.toHaveBeenCalled();
    });

    it('should correctly perform checkout using Stripe payment', () => {
      Company._company = {
        payment_providers: {
          stripe: { available: true, active: true },
          gear: { available: true, active: true },
          bilderlingspay: { available: false, active: false },
        },
        waiters_working: true
      };
      $rootScope.$apply();
      Checkout.__checkoutDeferred.resolve();
      Cart.__totals = { quantity: 2, price: 32 };

      CartController.checkout(CartController.METHODS.CREDIT_CART);
      $rootScope.$apply();

      expect(Checkout.checkout).toHaveBeenCalledWith(PAYMENT_PROVIDERS.STRIPE, undefined, jasmine.any(Function));
      expect(Alert.error).not.toHaveBeenCalled();
    });

    it('should correctly perform checkout with comment', () => {
      Company._company = {
        payment_providers: {
          stripe: { available: true, active: true },
          gear: { available: true, active: true },
          bilderlingspay: { available: false, active: false },
        },
        waiters_working: true
      };
      $rootScope.$apply();
      Checkout.__checkoutDeferred.resolve();
      Cart.__totals = { quantity: 2, price: 32 };

      CartController.comment = 'nice comment';
      CartController.checkout(CartController.METHODS.CREDIT_CART);
      $rootScope.$apply();

      expect(Checkout.checkout).toHaveBeenCalledWith(PAYMENT_PROVIDERS.STRIPE, 'nice comment', jasmine.any(Function));
      expect(Alert.error).not.toHaveBeenCalled();
    });

    it('should display error if checkout failed', () => {
      Company._company = {
        payment_providers: {
          stripe: { available: true, active: true },
          gear: { available: true, active: true },
          bilderlingspay: { available: false, active: false },
        },
        waiters_working: true,
      };
      $rootScope.$apply();

      Checkout.__checkoutDeferred.reject('gear error');
      Cart.__totals = { quantity: 2, price: 32 };

      CartController.checkout(CartController.METHODS.BITCOIN);
      expect(CartController.paymentLoading).toEqual(true);
      $rootScope.$apply();

      expect(Checkout.checkout).toHaveBeenCalledWith(PAYMENT_PROVIDERS.GEAR, undefined, jasmine.any(Function));
      expect(Alert.error).toHaveBeenCalledWith('CART.CHECKOUT_FAILED');
      expect(CartController.paymentLoading).toEqual(false);
    });

    it('should not display error if user cancels payment', () => {
      Company._company = {
        payment_providers: {
          stripe: { available: true, active: true },
          gear: { available: true, active: true },
          bilderlingspay: { available: false, active: false },
        },
        waiters_working: true,
      };
      $rootScope.$apply();

      Checkout.__checkoutDeferred.reject();
      Cart.__totals = { quantity: 2, price: 32 };

      CartController.checkout(CartController.METHODS.CREDIT_CART);
      expect(CartController.paymentLoading).toEqual(true);
      $rootScope.$apply();

      expect(Checkout.checkout).toHaveBeenCalledWith(PAYMENT_PROVIDERS.STRIPE, undefined, jasmine.any(Function));
      expect(Alert.error).not.toHaveBeenCalled();
      expect(CartController.paymentLoading).toEqual(false);
    });

    it('should correctly perform checkout using Gear payment', () => {
      Company._company = {
        payment_providers: {
          stripe: { available: false, active: false },
          gear: { available: true, active: true },
          bilderlingspay: { available: true, active: true },
        },
        waiters_working: true
      };
      $rootScope.$apply();

      Checkout.__checkoutDeferred.resolve();
      Cart.__totals = { quantity: 2, price: 32 };

      CartController.checkout(CartController.METHODS.BITCOIN);
      $rootScope.$apply();

      expect(Checkout.checkout).toHaveBeenCalledWith(PAYMENT_PROVIDERS.GEAR, undefined, jasmine.any(Function));
      expect(Alert.error).not.toHaveBeenCalled();
    });

    it('should correctly perform checkout using Bilderlingpay payment', () => {
      Company._company = {
        payment_providers: {
          stripe: { available: false, active: false },
          gear: { available: false, active: false },
          bilderlingspay: { available: true, active: true },
        },
        waiters_working: true
      };
      $rootScope.$apply();

      Checkout.__checkoutDeferred.resolve();
      Cart.__totals = { quantity: 2, price: 32 };

      CartController.checkout(CartController.METHODS.CREDIT_CART);
      $rootScope.$apply();

      expect(Checkout.checkout).toHaveBeenCalledWith(PAYMENT_PROVIDERS.BILDERLINGSPAY, undefined, jasmine.any(Function));
      expect(Alert.error).not.toHaveBeenCalled();
    });
  });

  describe('sorting', () => {
    // Prioritized list (ie. a list with at list one prioritized item)
    //   should be sortable (an item with priority 0 is non-prioritized)
    it('should set sortable flag to true for prioritized products', () => {
      Cart.__products = [
        { id: 1, name: 'Soup', priority: 1, },
        { id: 2, name: 'Chicken', priority: 2, },
      ];
      Cart._initializedDeferred.resolve();
      $rootScope.$apply();
      expect(CartController.productSortable.sorting).toBeTruthy();
    });

    it('should set sortable flag to false for non-prioritized products', () => {
      Cart.__products = [
        { id: 1, name: 'Soup', priority: 0, },
        { id: 2, name: 'Chicken', priority: 0, },
      ];
      Cart._initializedDeferred.resolve();
      $rootScope.$apply();
      expect(CartController.productSortable.sorting).toBeFalsy();
    });

    describe('updateProductSorting()', () => {
      beforeEach(() => {
        Cart.__products = [
          { id: 1, name: 'Soup', priority: 0, },
          { id: 2, name: 'Chicken', priority: 0, },
        ];

        Cart._initializedDeferred.resolve();
        $rootScope.$apply();
      });

      it('should update product priorities based on their indexes', () => {
        // So higher priority first, priority field should be (length - index)
        CartController.updateProductSorting();
        expect(CartController.products).toEqual([
          { id: 1, name: 'Soup', priority: 2, },
          { id: 2, name: 'Chicken', priority: 1, },
        ]);
        expect(CartController.productSortable.sorting).toBeTruthy();
      });

      it('should invoke CartItemsStore.prioritized to update remotely', () => {
        CartItemsStore.prioritizeItem.calls.reset();
        expect(CartItemsStore.prioritizeItem).not.toHaveBeenCalled();
        CartController.updateProductSorting();
        expect(CartItemsStore.prioritizeItem.calls.count()).toEqual(2); // one call for each item
      });
    });

    describe('cancelProductSorting()', () => {
      beforeEach(() => {
        Cart.__products = [
          { id: 1, name: 'Soup', priority: 1, },
          { id: 2, name: 'Chicken', priority: 2, },
        ];

        Cart._initializedDeferred.resolve();
        $rootScope.$apply();
      });

      it('should set all priorities to 0', () => {
        CartController.cancelProductSorting();
        expect(CartController.products.some(product => product.priority !== 0)).toBeFalsy();
        expect(CartController.productSortable.sorting).toBeFalsy();
      });

      it('should invoke CartItemsStore.prioritized to update remotely', () => {
        CartItemsStore.prioritizeItem.calls.reset();
        expect(CartItemsStore.prioritizeItem).not.toHaveBeenCalled();
        CartController.cancelProductSorting();
        expect(CartItemsStore.prioritizeItem.calls.count()).toEqual(2); // one call for each item
      });
    });
  });

  describe('isCashAvailable()', () => {
    it('should return cash available correctly', () => {
      Company._company = {
        cash_enabled: true
      };
      $rootScope.$apply();

      expect(CartController.isCashAvailable()).toEqual(true);
    });
    it('should return false when cash not available', () => {
      Company._company = { };
      $rootScope.$apply();

      expect(CartController.isCashAvailable()).toEqual(false);
    });
  });
});
