/**
* PriceFilter tests
*/
describe('customerApp.dao.PriceFilter', () => {
  let PriceFilter;
  let Currency;

  // load the module
  beforeEach(angular.mock.module('customerApp.filters', ($provide) => {
    // mock Currency service
    Currency = {
      getCurrency: jasmine.createSpy('getCurrency').and.callFake(() => Currency._currency),
    };

    $provide.value('Currency', Currency);
  }));

  // inject services
  beforeEach(inject(($filter) => {
    // initialize PriceFilter
    PriceFilter = $filter('price');
  }));


  it('should be defined', () => {
    expect(PriceFilter).toBeDefined();
  });

  it('should return empty string when no currency config is unavailable', () => {
    expect(PriceFilter(12.45)).toEqual('');
    expect(PriceFilter('1.12')).toEqual('');
  });

  it('should return empty string when input is empty', () => {
    Currency._currency = {
      sign: '$',
      decimal_fields: 2,
    };

    expect(PriceFilter('')).toEqual('');
    expect(PriceFilter(null)).toEqual('');
    expect(PriceFilter(undefined)).toEqual('');
  });

  it('should return formatted price', () => {
    Currency._currency = {
      sign: '$',
      decimal_fields: 2,
    };

    expect(PriceFilter(12.45)).toEqual('$12.45');
    expect(PriceFilter('1.12')).toEqual('$1.12');
    expect(PriceFilter('1.12345')).toEqual('$1.12');
    expect(PriceFilter('10')).toEqual('$10.00');
    expect(PriceFilter('0')).toEqual('$0.00');
    expect(PriceFilter(0)).toEqual('$0.00');
    expect(PriceFilter(1000)).toEqual('$1,000.00');
  });
});
