/**
* customerApp configuration tests
*/
describe('customerApp', () => {
  let httpProvider, injector;

  // load the module
  beforeEach(angular.mock.module('customerApp', ($provide, $injector, $httpProvider) => {
    httpProvider = $httpProvider;
    injector = $injector;
  }));

  // run injector
  beforeEach(inject());

  it('should have httpProvider configured correctly', () => {
    expect(httpProvider.defaults.withCredentials).toBeTruthy();
    expect(httpProvider.defaults.headers.common['X-Requested-With']).toEqual('XMLHttpRequest');
    expect(httpProvider.interceptors).toContain('SwishApiInterceptor');
  });

  it('should have all 3rd party dependencies specified', () => {
    expect(injector.has('$state')).toBeTruthy();
    expect(injector.has('StripeCheckout')).toBeTruthy();
    expect(injector.has('localStorageService')).toBeTruthy();
    expect(injector.has('Flash')).toBeTruthy();
  });

  it('should have all constants defined', (done) => {
    inject((ENV, STATES, EVENTS, ORDER_ITEM_STATUSES, ORDER_STATUSES, PAYMENT_PROVIDERS, DAOS) => {
      expect(ENV).toBeDefined();
      expect(STATES).toBeDefined();
      expect(EVENTS).toBeDefined();
      expect(ORDER_ITEM_STATUSES).toBeDefined();
      expect(ORDER_STATUSES).toBeDefined();
      expect(PAYMENT_PROVIDERS).toBeDefined();
      expect(DAOS).toBeDefined();
      done();
    });
  });
});
