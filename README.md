Customer frontend

# Development

For smooth dev process you can install these tools:

* [nvm](https://github.com/creationix/nvm) - its like `rvm` but for node or iojs
* [avn](https://github.com/wbyoung/avn) - automatic version switching for node or iojs
* [avn-nvm](https://github.com/wbyoung/avn-nvm) - thats what allows use both above transparenty

# Setup

There is a `config` section in `package.json`:

```
"config": {
  "appName": "Mycelium Swish",
  "https": false,
  "host": "localhost",
  "port": 8082,
  "openInBrowser": false,
  "sourceMap": true,
  "apiRoot": "http://localhost:8000"
}
```

* `openInBrowser` ­ set to `true` if you want to open your app in browser when running `npm start`
* `sourceMap` ­ enable source maps (works only for development env)

# Directory layout

TODO

# npm tasks

to install dependencies:
```
npm install
```

to run development server:
```
npm start
```

to build for production env:
```
npm run build
```

to run tests:
```
npm test
```

# Deploying

Before you can deploy anything, you'll need to install the `swishfab` Python
package. To do that, run:

```
pip install -r requirements.txt
```

To make sure everything's been installed correctly, run:

```
fab -l
```

You should get a list of the available Fabric tasks. If you need to modify the
behavior of some of the Fabric tasks, contact the engine-app team, or post your
proposed changes as a PR in the `github.com/MyceliumSwish/swishfab.git` repo.

Whenever an update is pushed to `swishfab`, you should upgrade your version, so
it's recommended you run `pip install -r requirements.txt` every time you're
about to deploy.

## Deploying Production

To deploy what's currently in `staging` into `production`, merge it and push it
with:

```
git checkout production
git merge staging
git push origin production
```

Then perform the deployment with:

```
fab s:production d
```

### Deploying Staging

To deploy what's currently in `master` into `staging`, merge it and push it
with:

```
git checkout staging
git merge master
git push origin staging
```

Then perform the deployment with:

```
fab s:staging d
```

## Deploying Prototype

Prototype deployment allows for an arbitrary branch to be provided. You can
deploy any branch to prototype with:

```
fab s:prototype d:<branchname>
```

### Automated Merges

If you want Fabric to take care of the merging and pushing, you can do so by
using the fabric task `merge_and_push` (or `mp` for short). You can use this
in tandem with deploy in a single command:

```
fab s:<instance> mp d
```

Notice `mp` is no longer allowed for the `prototype` instance.

Run this task along with `deploy` only if you know what you're doing. Assumes:

* You're not in detached HEAD mode.
* There aren't any outstanding changes in your working directory or the git index.
* There are no merge conflicts between the instance branch (production/staging/prototype) and the current branch.
