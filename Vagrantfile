# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "dhoppe/debian-8.2.0-amd64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  config.vm.network "forwarded_port", guest: 8000, host: 8000

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.255.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network", ip: "192.168.0.11"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "./public", "/usr/share/nginx/html"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Customize the amount of memory on the VM:
    vb.memory = "1024"
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL
    INSTALLATION_PATH=$HOME/www/admin-frontend;
    sudo apt-get update  -y
    sudo apt-get install -y build-essential git screen nginx python-virtualenv virtualenvwrapper libpq-dev python-dev
    if [ ! -f /usr/local/bin/node ];
    then
      wget https://nodejs.org/dist/v4.1.1/node-v4.1.1.tar.gz -O /usr/local/src/node-v4.1.1.tar.gz;
      cd /usr/local/src
      tar -zxvf ./node-v4.1.1.tar.gz
      cd ./node-v4.1.1
      ./configure
      make
      sudo make install
      sudo npm install -g bower gulp yo webpack
    else
      echo "Node.js already installed, skipping...";
    fi

    echo "To run the engine-app app, run the following commands:";
    echo "On the host machine:";
    echo "vagrant ssh";
    echo "On the guest machine:";
    echo "git clone https://github.com/MyceliumSwish/engine-app.git;"
    echo "cd ./engine-app;"
    echo "mkvirtualenv env;";
    echo "workon env;";
    echo "pip install -r requirements.txt;";
    echo "bower install;";
    echo "python manage.py migrate;";
    echo "python manage.py runserver 0:8000;";
  SHELL
end
